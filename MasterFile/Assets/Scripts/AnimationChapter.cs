﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationChapter : MonoBehaviour {

    public float chapterStartTime;

    public GameObject animationObject;

    private Animator animator;

    private MasterController myMasterScript;

    private int myChildIndex;

    private float animationLength = 0;

    // Use this for initialization
    void Start () {
        chapterStartTime = chapterStartTime / 60.0f;
        myMasterScript = GameObject.Find("Master").GetComponent<MasterController>();
        if (animationObject != null)
        {
            animator = animationObject.GetComponent<Animator>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (animator.GetFloat("speedMultiplier") > .1f)
        {
            animationLength = animator.GetCurrentAnimatorStateInfo(0).length;
        }

            //if (animator.GetFloat("speedMultiplier") > 0)
            //{
            //Debug.Log("Norm time = " + animator.GetFloat("normTime") + ". Length = " + animationLength);
            if (transform.GetSiblingIndex() + 1 < transform.parent.childCount)
            {
                if (animator.GetFloat("normTime") * animationLength > transform.parent.GetChild(transform.GetSiblingIndex() + 1).GetComponent<AnimationChapter>().chapterStartTime)
                {
                    transform.GetChild(0).GetComponent<Image>().enabled = true;
                }
                else
                {
                    transform.GetChild(0).GetComponent<Image>().enabled = false;
                }
            }
            else
            {
                if (animator.GetFloat("normTime") >= .999f)
                {
                    transform.GetChild(0).GetComponent<Image>().enabled = true;
                }
                else
                {
                    transform.GetChild(0).GetComponent<Image>().enabled = false;
                }
            }
        //}
    }

    public void setAnimationTime()
    {
        bool wasOne = false;
        if(animator.GetFloat("speedMultiplier") > 0)
        {
            wasOne = true;
        }
        animator.SetFloat("speedMultiplier", 1f);
        animator.SetFloat("normTime", (chapterStartTime / animationLength));
        
    }
}

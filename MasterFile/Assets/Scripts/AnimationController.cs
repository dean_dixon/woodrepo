﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {

    public float alphaValue;

	// Use this for initialization
	void Start () {

        if (transform.GetComponent<Renderer>())
        {
            foreach (Material matt in transform.GetComponent<Renderer>().materials)
            {
                matt.SetInt("_ZWrite", 1);
            }
        }

        Renderer[] rends = GetComponentsInChildren<Renderer>();
        foreach (Renderer rend in rends)
        {
            foreach (Material matt in rend.materials)
            {
                matt.SetInt("_ZWrite", 1);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (transform.GetComponent<Renderer>())
        {
            foreach (Material matt in transform.GetComponent<Renderer>().materials)
            {
                matt.color = new Color(matt.color.r, matt.color.g, matt.color.b, alphaValue);
                if (alphaValue <= .1 && transform.GetComponent<Renderer>().enabled)
                {
                    transform.GetComponent<Renderer>().enabled = false;
                    if(transform.GetComponent<MeshCollider>() != null)
                    {
                        transform.GetComponent<MeshCollider>().enabled = false;
                    }
                }
                else if (alphaValue > .1 && !transform.GetComponent<Renderer>().enabled)
                {
                    transform.GetComponent<Renderer>().enabled = true;
                    if (transform.GetComponent<MeshCollider>() != null)
                    {
                        transform.GetComponent<MeshCollider>().enabled = true;
                    }
                }
            }
        }

        Renderer[] rends = GetComponentsInChildren<Renderer>();
        foreach (Renderer rend in rends)
        {
            if (rend.transform.GetComponent<AnimationController>() == null && (rend.transform.parent.GetComponent<AnimationController>() == null || rend.transform.parent == transform))
            {
                foreach (Material matt in rend.materials)
                {
                    matt.color = new Color(matt.color.r, matt.color.g, matt.color.b, alphaValue);

                    if(alphaValue<= .1 && rend.enabled)
                    {
                        rend.enabled = false;
                        if (rend.transform.GetComponent<MeshCollider>() != null)
                        {
                            rend.transform.GetComponent<MeshCollider>().enabled = false;
                        }
                    }
                    else if(alphaValue > .1 && !rend.enabled)
                    {
                        rend.enabled = true;
                        if (rend.transform.GetComponent<MeshCollider>() != null)
                        {
                            rend.transform.GetComponent<MeshCollider>().enabled = true;
                        }
                    }
                }
            }
        }

	}
}

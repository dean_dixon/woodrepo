﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonController : MonoBehaviour {

    //ROV moveSpeed
    float normalMoveSpeed = 2;
    float rotationX;

    //Reference To PlayerController script, mainly beacuse we want to use the same Camera sensitivity value that script has.
    PlayerController myPlayerScript;

    //Reference to the camera of the ROV
    GameObject myCam;

    //The List of all the different Camera Angles the ROV has.
    //Create as many LOCAL transforms around the ROV to use as camera angles, and put them in this List to cycle through
    public List<Transform> views;

    // index for views List
    int viewIndex = 0, animIndex = 0;

    bool isDpadDown = false, isDpadClicked = false;

	// Use this for initialization
	void Start () {
        myPlayerScript = GameObject.Find("Player_MK").GetComponent<PlayerController>();
        myCam = transform.Find("3rdPersonCam").gameObject;
        rotationX = transform.eulerAngles.y;
    }
	
	// Update is called once per frame
	void Update () {
        //UpdateThirdPersonMode();
	}

    public void UpdateThirdPersonMode()
    {
        // update rotationX. Calculates 'Mouse X' every frame and adds to rotation of transform. Uses 'Xbox Right Stick' horizontal axis.
        // ROV mode cannot rotate up or down, so removed 'Mouse Y' and 'Xbox Right Stick' vertical axis.
        rotationX += Input.GetAxis("Mouse X") * myPlayerScript.cameraSensitivity * Time.deltaTime + Input.GetAxis("Xbox_RS_Horizontal") * myPlayerScript.cameraSensitivity * Time.deltaTime;
        transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);

        // forwardDir (forward Direction) of the ROV. Normalize without Y-axis because we only want it move in horizontal axis. Used for movement below
        Vector3 forwardDir = transform.forward;
        forwardDir.y = 0;
        forwardDir.Normalize();

        //rightDir (right Direction) of the ROV. Used for movement below
        Vector3 rightDir = transform.right;
        rightDir.y = 0;
        rightDir.Normalize();

        // Movement of ROV
        // if holding 'Shift' or 'Xbox Right Trigger' use the fastMoveFactor from the playerScript to move faster
        // Xbox trigger deadzone is .2
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) || (Input.GetAxis("Xbox_RT") > .2f))
        {
            transform.position += forwardDir * (normalMoveSpeed * myPlayerScript.fastMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
            transform.position += rightDir * (normalMoveSpeed * myPlayerScript.fastMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
            transform.position += forwardDir * (normalMoveSpeed * myPlayerScript.fastMoveFactor) * Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime;
            transform.position += rightDir * (normalMoveSpeed * myPlayerScript.fastMoveFactor) * Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime;

            // use 'Q','E' or 'Dpad Up', 'Dpad Down' to move up and down
            transform.position += transform.up * (normalMoveSpeed * myPlayerScript.fastMoveFactor) * .5f * Input.GetAxis("Xbox_Dpad_Vertical") * Time.deltaTime;

            // use 'Q','E' or 'Dpad Up', 'Dpad Down' to move up and down
            if (Input.GetKey(KeyCode.Q)) { transform.position += transform.up * normalMoveSpeed * myPlayerScript.fastMoveFactor * Time.deltaTime; }
            if (Input.GetKey(KeyCode.E)) { transform.position -= transform.up * normalMoveSpeed * myPlayerScript.fastMoveFactor * Time.deltaTime; }
        }
        // if holding 'Ctrl' or 'Xbox Left Trigger' use the slowMoveFactor from the playerScript to move slower
        // Xbox trigger deadzone is .2
        else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl) || (Input.GetAxis("Xbox_LT") > .2f))
        {
            transform.position += forwardDir * (normalMoveSpeed * myPlayerScript.slowMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
            transform.position += rightDir * (normalMoveSpeed * myPlayerScript.slowMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
            transform.position += forwardDir * (normalMoveSpeed * myPlayerScript.slowMoveFactor) * Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime;
            transform.position += rightDir * (normalMoveSpeed * myPlayerScript.slowMoveFactor) * Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime;

            // use 'Q','E' or 'Dpad Up', 'Dpad Down' to move up and down
            transform.position += transform.up * (normalMoveSpeed * myPlayerScript.slowMoveFactor) * .5f * Input.GetAxis("Xbox_Dpad_Vertical") * Time.deltaTime;

            // use 'Q','E' or 'Dpad Up', 'Dpad Down' to move up and down
            if (Input.GetKey(KeyCode.Q)) { transform.position += transform.up * myPlayerScript.slowMoveFactor * normalMoveSpeed * Time.deltaTime; }
            if (Input.GetKey(KeyCode.E)) { transform.position -= transform.up * myPlayerScript.slowMoveFactor * normalMoveSpeed * Time.deltaTime; }
        }
        //move with normal speed factor
        else
        {
            transform.position += forwardDir * normalMoveSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
            transform.position += rightDir * normalMoveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
            transform.position += forwardDir * normalMoveSpeed * Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime;
            transform.position += rightDir * normalMoveSpeed * Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime;

            // use 'Q','E' or 'Dpad Up', 'Dpad Down' to move up and down
            transform.position += transform.up * normalMoveSpeed * .5f * Input.GetAxis("Xbox_Dpad_Vertical") * Time.deltaTime;

            // use 'Q','E' or 'Dpad Up', 'Dpad Down' to move up and down
            if (Input.GetKey(KeyCode.Q)) { transform.position += transform.up * normalMoveSpeed * Time.deltaTime; }
            if (Input.GetKey(KeyCode.E)) { transform.position -= transform.up * normalMoveSpeed * Time.deltaTime; }
        }
        
        //Press 'Escape' or 'Xbox button B' to exit Third Person Mode (ROV Mode)
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown("joystick button 1") || Input.GetMouseButtonDown(1))
        {
            myPlayerScript.ToggleThirdPerson();
        }

        //Press 'Tab' or 'Xbox Button A, X' to cycle through ROV camera positions/angles
        // views is a List of Transforms. viewIndex is the index counter for views.
        if (Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown("joystick button 2") || Input.GetKeyDown("joystick button 0"))
        {
            viewIndex++;
            if(viewIndex >= views.Count)
            {
                viewIndex = 0;
            }

            myCam.transform.position = views[viewIndex].position;
            myCam.transform.rotation = views[viewIndex].rotation;
        }

        updateControls();
    }

    void updateControls()
    {
        #region Animation

        int targetIndex = -1;
        if (Input.GetKeyDown(KeyCode.Alpha1)) { targetIndex = 0; }
        else if (Input.GetKeyDown(KeyCode.Alpha2)) { targetIndex = 1; }
        else if (Input.GetKeyDown(KeyCode.Alpha3)) { targetIndex = 2; }
        else if (Input.GetKeyDown(KeyCode.Alpha4)) { targetIndex = 3; }
        else if (Input.GetKeyDown(KeyCode.Alpha5)) { targetIndex = 4; }

        if (targetIndex >= 0)
        {
            if (GetComponentInChildren<Animation>() != null)
            {
                Animation anim = GetComponentInChildren<Animation>();
                int c = 0;
                foreach (AnimationState state in anim)
                {
                    if (c == targetIndex)
                    {
                        if (anim.IsPlaying(state.clip.name))
                        {
                            if (state.speed > 0)
                            {
                                state.speed = 0;
                            }
                            else
                            {
                                state.speed = 1;
                            }

                        }
                        else
                        {
                            anim.Play(state.clip.name);
                        }
                    }
                    c++;
                }
            }
        }

        #endregion

        #region Animation For Xbox

        if (Input.GetAxis("Xbox_Dpad_Horizontal") != 0)
        {
            if (!isDpadDown)
            {
                isDpadClicked = true;
            }
            else
            {
                isDpadClicked = false;
            }
            isDpadDown = true;
        }
        else
        {
            isDpadDown = false;
            isDpadClicked = false;
        }

        if (isDpadClicked && GetComponentInChildren<Animation>() != null)
        {
            if (Input.GetAxis("Xbox_Dpad_Horizontal") > 0)
            {
                animIndex++;
                Animation anim = GetComponentInChildren<Animation>();
                int c = 0;
                foreach (AnimationState state in anim)
                {
                    c++;
                }

                if (animIndex > (c - 1))
                {
                    animIndex = 0;
                }

                c = 0;
                foreach (AnimationState state in anim)
                {
                    if (c == animIndex)
                    {
                        if (anim.IsPlaying(state.clip.name))
                        {
                            if (state.speed > 0)
                            {
                                state.speed = 0;
                            }
                            else
                            {
                                state.speed = 1;
                            }

                        }
                        else
                        {
                            anim.Play(state.clip.name);
                        }
                    }
                    c++;
                }
            }
            else
            {
                if (GetComponentInChildren<Animation>() != null)
                {
                    Animation anim = GetComponentInChildren<Animation>();
                    int c = 0;
                    foreach (AnimationState state in anim)
                    {
                        if (c == animIndex)
                        {
                            if (anim.IsPlaying(state.clip.name))
                            {
                                if (state.speed > 0)
                                {
                                    state.speed = 0;
                                }
                                else
                                {
                                    state.speed = 1;
                                }

                            }
                            else
                            {
                                anim.Play(state.clip.name);
                            }
                        }
                        c++;
                    }
                }
            }
        }

        #endregion
    }
}

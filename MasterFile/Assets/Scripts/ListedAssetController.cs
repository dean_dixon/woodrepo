﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListedAssetController : MonoBehaviour {

    [HideInInspector]
    public PlayerController myPlayerScript;
    [HideInInspector]
    public VR_Tablet_Script myVRScript;
    [HideInInspector]
    public MasterController myMasterScript;
    [HideInInspector]
    public GameObject myAsset;


	void Start () {
        
    }

	void Update () {

		if(myMasterScript.isVR)
        {
            //Debug.Log("yes we're goin");
            float yDiff = transform.localPosition.y + transform.parent.localPosition.y;

            if(yDiff >= -115 && yDiff <= 115)
            {
                transform.GetComponent<BoxCollider>().enabled = true;
            }
            else
            {
                transform.GetComponent<BoxCollider>().enabled = false;
            }
            /*
            if (myAsset.name.Equals("UMD_164001_DC4"))
            {
                Debug.Log(transform.localPosition.y + transform.parent.localPosition.y);
            }
            */
        }
	}
    

    public void GoToMyAsset()
    {
        if (myMasterScript.isVR)
        {
            if (!myVRScript.hasHitButton)
            {
                myVRScript.transform.parent.parent.position = myAsset.transform.position - (myVRScript.myCamera.transform.forward.normalized) * 5;
                myVRScript.transform.GetComponent<AudioSource>().clip = myVRScript.buttonSound;
                myVRScript.transform.GetComponent<AudioSource>().Play();
                myVRScript.VibrateRight();
            }
            myVRScript.hasHitButton = true;
        }
        else
        {
            myPlayerScript.transform.position = myAsset.transform.position - (myPlayerScript.myCam.transform.forward.normalized) * 5;
			//myPlayerScript.SelectObject(myAsset);
        }
    }
}

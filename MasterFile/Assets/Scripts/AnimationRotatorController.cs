﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationRotatorController : MonoBehaviour {

    public float targetRotationSpeed;

    public bool isX, isY, isZ;


    private float rotateSpeed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        rotateSpeed = Mathf.Lerp(rotateSpeed, targetRotationSpeed, .15f);

        if(isX)
        {
            transform.Rotate(new Vector3(rotateSpeed, 0, 0));
        }
        else if(isY)
        {
            transform.Rotate(new Vector3(0, rotateSpeed, 0));
        }
        else if(isZ)
        {
            transform.Rotate(new Vector3(0, 0, rotateSpeed));
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkPlayerController : Photon.MonoBehaviour {

    public bool isHead;
    public bool isLeft;

    public bool isOtherVr = false;

    public bool amIVr = false;

    private Vector3 correctPlayerPos;
    private Quaternion correctPlayerRot;

    private void Start()
    {

        if (photonView.isMine)
        {
            if (isHead)
            {
                if (amIVr)
                {
                    GameObject Player_VR_Camera = GameObject.Find("Camera (eye)");
                    transform.SetParent(Player_VR_Camera.transform);
                }
                else
                {
                    GameObject Player_VR_Camera = GameObject.Find("Player_MK").transform.Find("Player_Camera").gameObject;
                    transform.SetParent(Player_VR_Camera.transform);
                }
                transform.localEulerAngles = Vector3.zero;
                transform.localPosition = Vector3.zero;
            }
            else if(isLeft)
            {
                GameObject Player_VR_Camera = GameObject.Find("Controller (left)");
                transform.SetParent(Player_VR_Camera.transform);
                transform.localEulerAngles = new Vector3(0,180,0);
                transform.localPosition = Vector3.zero;
            }
            else
            {
                GameObject Player_VR_Camera = GameObject.Find("Controller (right)");
                transform.SetParent(Player_VR_Camera.transform);
                transform.localEulerAngles = new Vector3(0, 180, 0);
                transform.localPosition = Vector3.zero;
            }

            MeshRenderer[] rends = transform.GetComponentsInChildren<MeshRenderer>();
            for(int x =0; x< rends.Length; x++)
            {
                rends[x].enabled = false;
            }

            transform.Find("Avatar_Head").Find("NameText").gameObject.SetActive(false);
        }
        else
        {
            transform.Find("Avatar_Head").Find("NameText").GetComponent<TextMesh>().text = photonView.owner.NickName;
            isOtherVr = transform.Find("Master").GetComponent<MasterController>().isVR;
            if (isOtherVr)
            {
                transform.Find("Avatar_Head").Find("NameText").GetComponent<MakeBillboard>().cam = GameObject.Find("Camera (eye)").GetComponent<Camera>();
            }
            else
            {
                transform.Find("Avatar_Head").Find("NameText").GetComponent<MakeBillboard>().cam = GameObject.Find("Player_MK").transform.Find("Player_Camera").GetComponent<Camera>();
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!photonView.isMine)
        {
            transform.position = Vector3.Lerp(transform.position, this.correctPlayerPos, Time.deltaTime * 5);
            transform.rotation = Quaternion.Lerp(transform.rotation, this.correctPlayerRot, Time.deltaTime * 5);

            if (transform.Find("Avatar_Head").Find("NameText").GetComponent<MakeBillboard>().cam == null)
            {
                if (GameObject.Find("Player") != null)
                {
                    transform.Find("Avatar_Head").Find("NameText").GetComponent<MakeBillboard>().cam = GameObject.Find("Player").transform.Find("[CameraRig]").Find("Camera (eye)").GetComponent<Camera>();
                }
                else
                {
                    transform.Find("Avatar_Head").Find("NameText").GetComponent<MakeBillboard>().cam = GameObject.Find("Player_MK").transform.Find("Player_Camera").GetComponent<Camera>();
                }
            }
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);

        }
        else
        {
            // Network player, receive data
            this.correctPlayerPos = (Vector3)stream.ReceiveNext();
            this.correctPlayerRot = (Quaternion)stream.ReceiveNext();
        }
    }

    [PunRPC]
    public void DisplayLaser()
    {
        if (!isHead && !isLeft)
        {
            transform.Find("Pointer").GetComponent<LineRenderer>().enabled = true;
        }
        else if (isHead && !isLeft)
        {
            transform.Find("Avatar_Head").GetComponent<LineRenderer>().enabled = true;
        }
    }

    [PunRPC]
    public void HideLaser()
    {
        if (!isHead && !isLeft && transform.Find("Pointer"))
        {
            transform.Find("Pointer").GetComponent<LineRenderer>().enabled = false;
        }
        else if (isHead && transform.Find("Avatar_Head"))
        {
            transform.Find("Avatar_Head").GetComponent<LineRenderer>().enabled = false;
        }
    }

}

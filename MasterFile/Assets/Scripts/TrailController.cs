﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailController : MonoBehaviour {

    public GameObject trailObject;
    public List<GameObject> trailPoints;
    public float speed;
    int trailIndex = 0;
    float footprintTimer = 0;

	// Use this for initialization
	void Start () {
        GameObject newObj = Instantiate(trailObject, trailPoints[0].transform.position, Quaternion.identity,transform);
        newObj.GetComponent<Footprint_Controller>().trailPoints = trailPoints;
        //newObj.transform.SetParent(this.transform);
    }
	
	// Update is called once per frame
	void Update () {

        footprintTimer += Time.deltaTime;

        if(footprintTimer >= .75f)
        {
            GameObject newObj = Instantiate(trailObject, trailPoints[0].transform.position, Quaternion.identity, transform);
            newObj.GetComponent<Footprint_Controller>().trailPoints = trailPoints;
            footprintTimer = 0;
        }
             
	}
}

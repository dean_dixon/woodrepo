﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VR_Animation_Controller : MonoBehaviour {

    public VREasy.VRSlider mySliderScript;
    public Animation myAnimation;
    public VR_Tablet_Script tabletScript;

	// Use this for initialization
	void Start () {
        myAnimation.Play();

        foreach (AnimationState state in myAnimation)
        {
            state.speed = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach (AnimationState state in myAnimation)
        {
            if (state.speed == 0)
            {
                state.time = mySliderScript.GetRealValue();
            }
            else
            {
                if(state.time >= 54)
                {
                    state.speed = 0;
                }

                mySliderScript.SetValue(state.time/54f);
            }
        }
    }

    public void PlayAnimation()
    {
        if (!tabletScript.hasHitButton)
        {
            foreach (AnimationState state in myAnimation)
            {
                state.speed = 1;
            }
            tabletScript.VibrateRight();
        }
        tabletScript.hasHitButton = true;
    }

    public void StopAnimation()
    {
        if (!tabletScript.hasHitButton)
        {
            foreach (AnimationState state in myAnimation)
            {
                state.speed = 0;
            }
            tabletScript.VibrateRight();
        }
        tabletScript.hasHitButton = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationRopeSwagController : MonoBehaviour {

    public float sagAmount;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.GetComponent<Cable_Procedural_Simple>().sagAmplitude = sagAmount;
	}
}

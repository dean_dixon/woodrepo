﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointOfInterest_Controller : MonoBehaviour {

    bool movingUp = true;
    float originalYPosition;
	// Use this for initialization
	void Start () {
        originalYPosition = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {

        if(movingUp)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + .005f, transform.position.z);

            if(transform.position.y - originalYPosition > .1f)
            {
                movingUp = false;
            }
        }
        else
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - .005f, transform.position.z);

            if (transform.position.y - originalYPosition < -.1f)
            {
                movingUp = true;
            }
        }

        transform.Rotate(0, 1, 0);
		
	}
}

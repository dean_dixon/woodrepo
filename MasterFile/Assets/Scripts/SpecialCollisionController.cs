﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialCollisionController : MonoBehaviour {

    public GameObject CollisionPointPrefab;

    PlayerController Player_MK_Script;
    VR_Tablet_Script Player_VR_Script;
    MasterController myMasterScript;

    GameObject myCollisionObject;

    Vector3 originalpos;

    // Use this for initialization
    void Start () {
		myMasterScript = GameObject.Find("Master").GetComponent<MasterController>();
        if (GameObject.Find("Player") != null)
        {
            Player_VR_Script = GameObject.Find("Player").transform.Find("[CameraRig]").Find("Controller (left)").Find("TabletParent").GetComponent<VR_Tablet_Script>();
        }

        if(GameObject.Find("Player_MK") != null)
        {
            Player_MK_Script = GameObject.Find("Player_MK").GetComponent<PlayerController>();
        }
        

        originalpos = transform.localPosition;
    }
	
	// Update is called once per frame
	void Update () {
        transform.localPosition = originalpos;
        if (GetComponent<Rigidbody>() != null)
        {
            GetComponent<Rigidbody>().position = transform.position;
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("HERE");
        if (collision.collider.tag.Equals("ROV"))
        {
            //Debug.Log("HIT_ROV");
        }
        else
        {
            if (myMasterScript.isVR)
            {
                if (myCollisionObject == null)
                {
                    myCollisionObject = Instantiate(CollisionPointPrefab, collision.contacts[0].point, Quaternion.identity);
                }
            }
            else
            {
                /*
                if (Player_MK_Script.isMoving)
                {
                    //Player_MK_Script.ClearActions();
                    //Player_MK_Script.stopGrab();
                }
                else if (Player_MK_Script.isThirdPerson)
                {
                    //Player_MK_Script.ToggleThirdPerson();
                }
                */

                if (myCollisionObject == null)
                {
                    myCollisionObject = Instantiate(CollisionPointPrefab, collision.contacts[0].point, Quaternion.identity);
                }
            }

            //Debug.Log("HIT");
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        Destroy(myCollisionObject);
        myCollisionObject = null;
    }

}

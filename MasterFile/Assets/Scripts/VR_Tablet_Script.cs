﻿ using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VR_Tablet_Script : MonoBehaviour {

    #region variables
    public GameObject testValve;

    public bool startAsDay;

    public Vector2 CartesianOffset;

    public float depthOffset;

    public GameObject measuringStick0;
    public GameObject measuringStick1;

    public TextMesh hypotenuse;
    public TextMesh horizontal;
    public TextMesh vertical;

    public Transform[] waypoints;

    public Material newMat;

    public GameObject Headlight, waterSurfaceObj, cartesianRefObj, objXLineRend, objZLineRend, objYLineRend, DayPP_Profile, NightPP_Profile;

    // references our Lights that are on at day to toggle between day and night
    public List<GameObject> Day_Lights_List;

    // references our Lights that are on at night to toggle between day and night
    public List<GameObject> Night_Lights_List;

    public GameObject[] sceneChangers;

    public AudioClip selectSound, buttonSound;

    public GameObject UnderwaterFogVolume;

    [HideInInspector]
    public Transform selectedObject;

    [HideInInspector]
    public VREasy.VRSlider sliderScript;

    [HideInInspector]
    public VREasy.VRGrabbable selectedObjectGrabbableController;

    [HideInInspector]
    public Vector3 selectedObjectOriginalPosition, selectedObjectOriginalRotation;

    [HideInInspector]
    public TextMesh  AnimationButtonText, HideButtonText;

    [HideInInspector]
    public GameObject TabletObject, myCamera, myCameraRig, editPage, dataPage, assetPage, animationPage, multiplayerPage, vpLinkPage, vpLink_AnalogValve_Page,
        vpLink_DigitalValve_Page, vpLink_DigitalPump_Page, leftControllerModel, leftTooltips, rightTooltips, ScreenShotCountdownText, HUD, DepthText, RotationText, 
        NorthingText, EastingText, Compass, MiniMapMesh, rightControllerModel, myMaster;

    [HideInInspector]
    public bool isMenuButtonDown = false, hasHitButton = false, isMakingJumper = false, isMakingJumper2 = false, isTreeJumper = false,
        movingVeritcal = false, canClimb = false;

    [HideInInspector]
    public float pivotSpeed = .5f, ladderTimeout = 0, timeHeldTrigger = 0;

    [HideInInspector]
    public SteamVR_TrackedObject leftControllerScript, rightControllerScript;

    [HideInInspector]
    public VREasy.PointerSelector leftControllerPointerController;

    [HideInInspector]
    public VREasy.ScreenshotMaker myScreenshotScript;

    [HideInInspector]
    public bool isMeasuringFirst = false, isMeasuringSecond = false;

    private int waypointIndex = 0, animIndex = 0;

    private Valve.VR.EVRButtonId aButton = Valve.VR.EVRButtonId.k_EButton_Axis0;
    private Valve.VR.EVRButtonId bButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;
    private Valve.VR.EVRButtonId cButton = Valve.VR.EVRButtonId.k_EButton_Axis2;
    private Valve.VR.EVRButtonId dButton = Valve.VR.EVRButtonId.k_EButton_Axis3;
    private Valve.VR.EVRButtonId menuButton = Valve.VR.EVRButtonId.k_EButton_ApplicationMenu;
    private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
    //Valve.VR.EVRButtonId.k_
    [HideInInspector]
    public SteamVR_Controller.Device leftController, rightController;

    private float currentScale = 1, tempScale = 1, prevScale = 1, gripScale = 0, controllerDist = 0;
    private bool hasStartedScaling = false;

    private float screenshotCountdownTimer = 0;
    private bool isTakingScreenshot = false;
    private float depth = 0;

    private float angle = 0;
    private float prevAngle = 0, vplinkUpdateTimer = 0;

    private Vector3 prevCompPos;

    private bool hasPressedAButton = false;

    private float delayStartTimer = 1, rightMenuButtonHeldTimer = 0, leftMenuButtonHeldTimer = 0;

    private float buttonColorAlpha = 1;

    private bool isAlphaDecreasing = true;

    private Material oldMat;

    private bool hasStarted = false;

    private bool showingHelp = false;

    private bool isAligning = false;

    private bool isAligningFirst = false;

    private bool isAligningSecond = false;

    private Vector3 alignTargetPosition;

    private bool isDay = false;

    GameObject alignObject;

    private VPDatabase vpd;

    private CapsuleCollider myCollider;

    private float leftTouchHeldTimer = 0;

    #endregion

    const int KUnityNameCol = 0;
    const int KFlowCol = 1;
    const int KUpstreamPressureCol = 2;
    const int KDownstreamPressureCol = 3;
    const int KValvePosCol = 4;

    List<GameObject> vpLink_AnalogValve_Values = new List<GameObject>();
    List<GameObject> vpLink_DigitalValve_Values = new List<GameObject>();
    List<GameObject> vpLink_DigitalPump_Values = new List<GameObject>();

    // Use this for initialization
    void Start () {
        myMaster = GameObject.Find("Master").gameObject;

        vpd = FindObjectOfType<VPDatabase>();
        if (vpd == null)
        {
            Debug.LogError("Cannot find VP Link database in the VerMovement object");
        }

        #region Setting Objects

        
        TabletObject = transform.GetChild(0).gameObject;
        editPage = TabletObject.transform.Find("Edit Page").gameObject;
        dataPage = TabletObject.transform.Find("Data Page").gameObject;
        assetPage = TabletObject.transform.Find("Asset Page").gameObject;
        animationPage = TabletObject.transform.Find("Animation Page").gameObject;
        multiplayerPage = TabletObject.transform.Find("Multiplayer Page").gameObject;
        vpLinkPage = TabletObject.transform.Find("VPLink_Page").gameObject;
        vpLink_AnalogValve_Page = vpLinkPage.transform.Find("Canvas").Find("Panel").Find("AnalogValve_Page").gameObject;
        vpLink_DigitalValve_Page = vpLinkPage.transform.Find("Canvas").Find("Panel").Find("DigitalValve_Page").gameObject;
        vpLink_DigitalPump_Page = vpLinkPage.transform.Find("Canvas").Find("Panel").Find("DigitalPump_Page").gameObject;

        sliderScript = editPage.transform.Find("Canvas").Find("Panel").Find("Rotate_Slider").GetComponentInChildren<VREasy.VRSlider>();


        myCameraRig = transform.parent.parent.gameObject;
        myCollider = myCameraRig.GetComponent<CapsuleCollider>();
        myCamera = myCameraRig.transform.Find("Camera (eye)").gameObject;

        leftControllerModel = myCameraRig.transform.Find("Controller (left)").Find("Model").gameObject;
        rightControllerModel = myCameraRig.transform.Find("Controller (right)").Find("Model").gameObject;
        leftTooltips = leftControllerModel.transform.parent.Find("Tooltips").gameObject;
        rightTooltips = rightControllerModel.transform.parent.Find("Tooltips").gameObject;
        leftControllerScript = leftControllerModel.transform.parent.GetComponent<SteamVR_TrackedObject>();
        leftControllerPointerController = leftControllerModel.transform.parent.GetComponent<VREasy.PointerSelector>();
        rightControllerScript = leftControllerModel.transform.parent.parent.Find("Controller (right)").GetComponent<SteamVR_TrackedObject>();
        //rightController = rightControllerScript.transform.GetComponent<SteamVR_Controller.Device>();

        myScreenshotScript = transform.GetComponent<VREasy.ScreenshotMaker>();

        ScreenShotCountdownText = myCamera.transform.Find("ScreenshotText").gameObject;
        HUD = myCamera.transform.Find("HUDPanel").gameObject;
        DepthText = myCamera.transform.Find("WDText").gameObject;
        RotationText = myCamera.transform.Find("HDGText").gameObject;
        NorthingText = myCamera.transform.Find("NText").gameObject;
        EastingText = myCamera.transform.Find("EText").gameObject;
        Compass = myCamera.transform.Find("Compass").gameObject;
        MiniMapMesh = myCamera.transform.Find("MiniMapMesh").gameObject;

        

        #endregion

        //Compass.transform.parent = null;

        //QualitySettings.SetQualityLevel(5, true);
        sliderScript.targetComponent = null;
        sliderScript.targetProperty = "Added_Y_Rotation";      
        
        #region File Reading

        #region isValve

        string path = "Assets/Resources/VPValves.txt";

        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(path);
        string line;
        while ((line = reader.ReadLine()) != null)
        {
            //line = line.Replace("\t", string.Empty);
            string[] fields = line.Split('\t');
            char[] chars = fields[0].ToCharArray();
            if (!chars[0].Equals('#'))
            {
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();

                    objScript.isValve = true;
                    objScript.manifoldInfo.name = fields[0];
                    objScript.manifoldInfo.type = fields[1];
                    objScript.manifoldInfo.depth = fields[2];
                    objScript.manifoldInfo.foundation = fields[3];
                    objScript.manifoldInfo.weight = fields[4];
                    objScript.manifoldInfo.dPressure = fields[5];
                    objScript.manifoldInfo.tempRating = fields[6];
                    objScript.manifoldInfo.headers = fields[7];
                    objScript.manifoldInfo.hubs = fields[8];
                    objScript.manifoldInfo.sensors = fields[9];
                }
            }
        }
        reader.Close();

        #endregion

        #region isPump

        path = "Assets/Resources/PLEM & PLET.txt";

        //Read the text from directly from the test.txt file
        reader = new StreamReader(path);
        while ((line = reader.ReadLine()) != null)
        {
            //line = line.Replace("\t", string.Empty);
            string[] fields = line.Split('\t');
            if (GameObject.Find(fields[0]))
            {
                GameObject obj = GameObject.Find(fields[0]);
                VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                objScript.isPump = true;
                objScript.plemInfo.name = fields[0];
                objScript.plemInfo.type = fields[1];
                objScript.plemInfo.depth = fields[2];
                objScript.plemInfo.weight = fields[3];
                objScript.plemInfo.dPressure = fields[4];
                objScript.plemInfo.tempRating = fields[5];
                objScript.plemInfo.hubs = fields[6];

            }
        }
        reader.Close();

        #endregion

        #endregion

        GetComponent<LineRenderer>().enabled = true;

        isDay = !startAsDay;
        ToggleDayNight();

        if(myMaster.GetComponent<MasterController>().isVPLink)
        {
            editPage.SetActive(false);
            TabletObject.transform.Find("EditButton").gameObject.SetActive(false);
            TabletObject.transform.Find("DataButton").gameObject.SetActive(false);
            TabletObject.transform.Find("AssetButton").gameObject.SetActive(false);
            TabletObject.transform.Find("AnimationButton").gameObject.SetActive(false);
            
            vpLink_AnalogValve_Values.Add(vpLink_AnalogValve_Page.transform.Find("Texts").Find("VPLink_ObjectName_Text").gameObject);
            vpLink_AnalogValve_Values.Add(vpLink_AnalogValve_Page.transform.Find("Texts").Find("VPLink_ValueText_0").gameObject);
            vpLink_AnalogValve_Values.Add(vpLink_AnalogValve_Page.transform.Find("Texts").Find("VPLink_ValueText_1").gameObject);
            vpLink_AnalogValve_Values.Add(vpLink_AnalogValve_Page.transform.Find("Texts").Find("VPLink_ValueText_2").gameObject);
            vpLink_AnalogValve_Values.Add(vpLink_AnalogValve_Page.transform.Find("Texts").Find("VPLink_ValueText_3").gameObject);

            vpLink_DigitalValve_Values.Add(vpLink_DigitalValve_Page.transform.Find("Texts").Find("VPLink_ObjectName_Text").gameObject);
            vpLink_DigitalValve_Values.Add(vpLink_DigitalValve_Page.transform.Find("Texts").Find("VPLink_ValueText_0").gameObject);
            vpLink_DigitalValve_Values.Add(vpLink_DigitalValve_Page.transform.Find("Texts").Find("VPLink_ValueText_1").gameObject);
            vpLink_DigitalValve_Values.Add(vpLink_DigitalValve_Page.transform.Find("Texts").Find("VPLink_ValueText_2").gameObject);
            vpLink_DigitalValve_Values.Add(vpLink_DigitalValve_Page.transform.Find("Texts").Find("VPLink_ValueText_3").gameObject);

            vpLink_DigitalPump_Values.Add(vpLink_DigitalPump_Page.transform.Find("Texts").Find("VPLink_ObjectName_Text").gameObject);
            vpLink_DigitalPump_Values.Add(vpLink_DigitalPump_Page.transform.Find("Texts").Find("VPLink_ValueText_0").gameObject);
            vpLink_DigitalPump_Values.Add(vpLink_DigitalPump_Page.transform.Find("Texts").Find("VPLink_ValueText_1").gameObject);
            vpLink_DigitalPump_Values.Add(vpLink_DigitalPump_Page.transform.Find("Texts").Find("VPLink_ValueText_2").gameObject);
            vpLink_DigitalPump_Values.Add(vpLink_DigitalPump_Page.transform.Find("Texts").Find("VPLink_ValueText_3").gameObject);
        }
        else
        {
            vpLinkPage.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update () {

        #region Vplink Position Control

        vplinkUpdateTimer += Time.deltaTime;

        //Debug.Log("X = " + myCamera.transform.position.x);
        //Debug.Log("Z = " + myCamera.transform.position.z);
        if (vplinkUpdateTimer >= 1)
        {
            if(vpd != null)
            {
                string player = "Player1";
                if (vpd.TagType("VRLocation_" + player + "_X").Length > 0)
                {
                    //Debug.Log("yo");
                    vpd.setPVE("VRLocation_" + player + "_X", myCamera.transform.position.x);
                    vpd.setPVE("VRLocation_" + player + "_Y", myCamera.transform.position.y);
                    vpd.setPVE("VRLocation_" + player + "_Z", myCamera.transform.position.z);
                    vpd.setPVE("VRLocation_" + player + "_Heading", myCamera.transform.eulerAngles.y);
                }
            }
        }

        #endregion

        #region Update Valve/Pump Info

        if (myMaster.GetComponent<MasterController>().isVPLink)
        {
            if (selectedObject != null)
            {
                
                if (selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings.Length >= 4)
                {
                    Transform vpLinkPanel = vpLinkPage.transform.Find("Canvas").Find("Panel");
                    if (selectedObject.GetComponent<VREasy.VRGrabbable>().isValve)
                    {
                        

                        if (selectedObject.GetComponent<VREasy.TransformController>().myValve.GetComponent<ValveLerper>().isLever)
                        {
                            if(!vpLink_DigitalValve_Page.activeSelf)
                            {
                                vpLink_DigitalValve_Page.SetActive(true);
                                vpLink_AnalogValve_Page.SetActive(false);
                                vpLink_DigitalPump_Page.SetActive(false);
                            }
                            vpLink_DigitalValve_Values[0].GetComponent<Text>().text = "ID: " + selectedObject.transform.name;
                            vpLink_DigitalValve_Values[1].GetComponent<Text>().text = "Flow Through Valve: " + vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KFlowCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KFlowCol] + " ??");
                            vpLink_DigitalValve_Values[2].GetComponent<Text>().text = "Upstream Pressure: " + vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KUpstreamPressureCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KUpstreamPressureCol] + " ??");
                            vpLink_DigitalValve_Values[3].GetComponent<Text>().text = "Downstream Pressure: " + vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KDownstreamPressureCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KDownstreamPressureCol] + " ??");
                            vpLink_DigitalValve_Values[4].GetComponent<Text>().text = "Valve Status:       " + vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KValvePosCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KValvePosCol] + " ??");

                            if(vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KValvePosCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KValvePosCol] + " ??").Equals("Open"))
                            {
                                vpLink_DigitalValve_Page.transform.Find("Valve_Light_Status").GetComponent<Renderer>().material = vpLink_DigitalValve_Page.transform.Find("Valve_Light_Status").GetComponent<VPLight>().onMaterial;
                            }
                            else
                            {
                                vpLink_DigitalValve_Page.transform.Find("Valve_Light_Status").GetComponent<Renderer>().material = vpLink_DigitalValve_Page.transform.Find("Valve_Light_Status").GetComponent<VPLight>().offMaterial;
                            }

                        }
                        else
                        {
                            if (!vpLink_AnalogValve_Page.activeSelf)
                            {
                                vpLink_DigitalValve_Page.SetActive(false);
                                vpLink_AnalogValve_Page.SetActive(true);
                                vpLink_DigitalPump_Page.SetActive(false);
                            }
                            vpLink_AnalogValve_Values[0].GetComponent<Text>().text = "ID: " + selectedObject.transform.name;
                            vpLink_AnalogValve_Values[1].GetComponent<Text>().text = "Flow Through Valve: " + vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KFlowCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KFlowCol] + " ??");
                            vpLink_AnalogValve_Values[2].GetComponent<Text>().text = "Upstream Pressure: " + vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KUpstreamPressureCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KUpstreamPressureCol] + " ??");
                            vpLink_AnalogValve_Values[3].GetComponent<Text>().text = "Downstream Pressure: " + vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KDownstreamPressureCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KDownstreamPressureCol] + " ??");
                            vpLink_AnalogValve_Values[4].GetComponent<Text>().text = "Percent Open: " + vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KValvePosCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KValvePosCol] + " ??");

                            

                        }
                    }
                    else
                    {
                        if (!vpLink_DigitalPump_Page.activeSelf)
                        {
                            vpLink_DigitalValve_Page.SetActive(false);
                            vpLink_AnalogValve_Page.SetActive(false);
                            vpLink_DigitalPump_Page.SetActive(true);
                        }
                        vpLink_DigitalPump_Values[0].GetComponent<Text>().text = "ID: " + selectedObject.transform.name;
                        vpLink_DigitalPump_Values[1].GetComponent<Text>().text = "Flow Through Pump: " + vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KFlowCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KFlowCol] + " ??");
                        vpLink_DigitalPump_Values[2].GetComponent<Text>().text = "Inlet Pressure: " + vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KUpstreamPressureCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KUpstreamPressureCol] + " ??");
                        vpLink_DigitalPump_Values[3].GetComponent<Text>().text = "Discharge Pressure: " + vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KDownstreamPressureCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KDownstreamPressureCol] + " ??");
                        vpLink_DigitalPump_Values[4].GetComponent<Text>().text = "Pump Status:      " + vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KValvePosCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KValvePosCol] + " ??");

                        if (vpd.PVEStr(selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KValvePosCol], "?? " + selectedObject.GetComponent<VREasy.VRGrabbable>().tagStrings[KValvePosCol] + " ??").Equals("On"))
                        {
                            vpLink_DigitalPump_Page.transform.Find("Pump_Light_Status").GetComponent<Renderer>().material = vpLink_DigitalPump_Page.transform.Find("Pump_Light_Status").GetComponent<VPLight>().onMaterial;
                        }
                        else
                        {
                            vpLink_DigitalPump_Page.transform.Find("Pump_Light_Status").GetComponent<Renderer>().material = vpLink_DigitalPump_Page.transform.Find("Pump_Light_Status").GetComponent<VPLight>().offMaterial;
                        }

                    }
                }
            }
        }

        #endregion

        #region Controller Graphics
        /*
        if(isAlphaDecreasing)
        {
            buttonColorAlpha -= .033333333f;

            if(buttonColorAlpha <= .1f)
            {
                isAlphaDecreasing = false;
            }
        }
        else
        {
            buttonColorAlpha += .033333333f;

            if (buttonColorAlpha >= 1)
            {
                isAlphaDecreasing = true;
            }
        }

        delayStartTimer -= Time.deltaTime;
        if(delayStartTimer < 0  && !hasStarted)
        {
            DelayedStart();
            hasStarted = true;
        }

        if(hasStarted)
        {
            rightControllerModel.transform.Find("trigger").GetComponent<MeshRenderer>().material.SetFloat("_Inside", buttonColorAlpha);
            rightControllerModel.transform.Find("trigger").GetComponent<MeshRenderer>().material.SetFloat("_Rim", buttonColorAlpha);
        }
        */
        #endregion

        // Set the device as the Left Controller
        leftController = SteamVR_Controller.Input((int)leftControllerScript.index);
        rightController = SteamVR_Controller.Input((int)rightControllerScript.index);

        #region Measuring Control

        measuringStick0.transform.Rotate(2, 0, 0);
        measuringStick1.transform.Rotate(2, 0, 0);
        hypotenuse.characterSize = 12 * (Vector3.Distance(hypotenuse.transform.position, myCamera.transform.position) / 20);

        if(hypotenuse.characterSize > 2.5f)
        {
            hypotenuse.characterSize = 2.5f;
        }

        vertical.characterSize = hypotenuse.characterSize;
        horizontal.characterSize = hypotenuse.characterSize;

        if (Vector3.Distance(hypotenuse.transform.position, horizontal.transform.position) < hypotenuse.characterSize / 5.0f)
        {
            horizontal.transform.GetComponent<MeshRenderer>().enabled = false;
        }
        else
        {
            horizontal.transform.GetComponent<MeshRenderer>().enabled = true;
        }

        if (Vector3.Distance(hypotenuse.transform.position, vertical.transform.position) < hypotenuse.characterSize / 5.0f)
        {
            vertical.transform.GetComponent<MeshRenderer>().enabled = false;
        }
        else
        {
            vertical.transform.GetComponent<MeshRenderer>().enabled = true;
        }

        if (isMeasuringFirst)
        {
            LineRenderer line = transform.GetComponent<LineRenderer>();
            line.enabled = true;

            Vector3 fwd = rightControllerModel.transform.forward;
            RaycastHit hit;
            Ray ray = new Ray(rightControllerModel.transform.position, rightControllerModel.transform.forward);

            int lay = LayerMask.NameToLayer("Detail Object");
            //Debug.Log("Layer = " + lay);
            int lay2 = 4;
            int lay3 = LayerMask.NameToLayer("Detail Object ROV");
            int lay4 = 0;
            int layermask1 = (1 << lay) | (1 << lay2) | (1 << lay3) | (1 << lay4);

            if (Physics.Raycast(ray, out hit,2500.0f, layermask1))
            {
                measuringStick0.transform.position = hit.point;
                measuringStick0.transform.LookAt(transform.position + hit.normal * 5);
                measuringStick0.transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);
                
                line.SetPosition(0, rightControllerModel.transform.position);
                line.SetPosition(1, rightControllerModel.transform.position + rightControllerModel.transform.forward * hit.distance);
            }
            else
            {
                line.SetPosition(0, rightControllerModel.transform.position);
                line.SetPosition(1, rightControllerModel.transform.position + rightControllerModel.transform.forward * 100);
            }

            measuringStick0.transform.Rotate(2, 0, 0);
            measuringStick1.transform.Rotate(2, 0, 0);
        }
        else if(isMeasuringSecond)
        {
            LineRenderer line = transform.GetComponent<LineRenderer>();
            line.enabled = true;
            Vector3 fwd = rightControllerModel.transform.forward;
            RaycastHit hit;
            Ray ray = new Ray(rightControllerModel.transform.position, rightControllerModel.transform.forward);

            int lay = LayerMask.NameToLayer("Detail Object");
            //Debug.Log("Layer = " + lay);
            int lay2 = 4;
            int lay3 = LayerMask.NameToLayer("Detail Object ROV");
            int lay4 = 0;
            int layermask1 = (1 << lay) | (1 << lay2) | (1 << lay3) | (1 << lay4);

            if (Physics.Raycast(ray, out hit, 2500.0f, layermask1))
            {
                measuringStick1.transform.position = hit.point;
                measuringStick1.transform.LookAt(transform.position + hit.normal * 5);
                measuringStick1.transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);

                line.SetPosition(0, rightControllerModel.transform.position);
                line.SetPosition(1, rightControllerModel.transform.position + rightControllerModel.transform.forward * hit.distance);
            }
            else
            {
                line.SetPosition(0, rightControllerModel.transform.position);
                line.SetPosition(1, rightControllerModel.transform.position + rightControllerModel.transform.forward * 100);
            }

            LineRenderer hypLine = hypotenuse.GetComponent<LineRenderer>();
            LineRenderer horLine = horizontal.GetComponent<LineRenderer>();
            LineRenderer verLine = vertical.GetComponent<LineRenderer>();

            hypLine.SetPosition(0, measuringStick0.transform.position);
            hypLine.SetPosition(1, measuringStick1.transform.position);
            hypotenuse.transform.position = (hypLine.GetPosition(0) + hypLine.GetPosition(1)) / 2;

            float dist = Vector3.Distance(measuringStick0.transform.position, measuringStick1.transform.position)*3.28084f;

            hypotenuse.text = "" + dist.ToString("F2");

            verLine.SetPosition(0, measuringStick0.transform.position);
            verLine.SetPosition(1, new Vector3(measuringStick0.transform.position.x, measuringStick0.transform.position.y + (measuringStick1.transform.position.y - measuringStick0.transform.position.y), measuringStick0.transform.position.z));
            vertical.transform.position = (verLine.GetPosition(0) + verLine.GetPosition(1)) / 2;

            float dist2 = Vector3.Distance(verLine.GetPosition(0), verLine.GetPosition(1)) * 3.28084f;
            vertical.text = "" + dist2.ToString("F2");

            horLine.SetPosition(0, new Vector3(measuringStick0.transform.position.x, measuringStick0.transform.position.y + (measuringStick1.transform.position.y - measuringStick0.transform.position.y), measuringStick0.transform.position.z));
            horLine.SetPosition(1, measuringStick1.transform.position);
            horizontal.transform.position = (horLine.GetPosition(0) + horLine.GetPosition(1)) / 2;

            float dist3 = Vector3.Distance(horLine.GetPosition(0), horLine.GetPosition(1)) * 3.28084f;
            horizontal.text = "" + dist3.ToString("F2");

            measuringStick0.transform.Rotate(2, 0, 0);
            measuringStick1.transform.Rotate(2, 0, 0);
        }

        #endregion

        #region Align Control

        if(isAligning)
        {
            alignObject.transform.position = Vector3.Lerp(alignObject.transform.position, alignTargetPosition, .1f);

            if(Vector3.Distance(alignObject.transform.position,alignTargetPosition) < .01f)
            {
                alignObject.transform.position = alignTargetPosition;

                selectedObject.transform.parent = null;

                alignObject = null;

                isAligning = false;
                
            }
        }

        if (isAligningFirst)
        {
            LineRenderer line = transform.GetComponent<LineRenderer>();

            Vector3 fwd = rightControllerModel.transform.forward;
            RaycastHit hit;
            Ray ray = new Ray(rightControllerModel.transform.position, rightControllerModel.transform.forward);

            int lay = LayerMask.NameToLayer("Detail Object");
            int layermask1 = (1 << lay);

            if (Physics.Raycast(ray, out hit, layermask1))
            {
                measuringStick0.transform.position = hit.point;
                measuringStick0.transform.LookAt(transform.position + hit.normal * 5);
                measuringStick0.transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);

                line.SetPosition(0, rightControllerModel.transform.position);
                line.SetPosition(1, rightControllerModel.transform.position + rightControllerModel.transform.forward * hit.distance);
            }
            else
            {
                line.SetPosition(0, rightControllerModel.transform.position);
                line.SetPosition(1, rightControllerModel.transform.position + rightControllerModel.transform.forward * 100);
            }

            measuringStick0.transform.Rotate(2, 0, 0);
            measuringStick1.transform.Rotate(2, 0, 0);
        }
        else if (isAligningSecond)
        {
            LineRenderer line = transform.GetComponent<LineRenderer>();

            Vector3 fwd = rightControllerModel.transform.forward;
            RaycastHit hit;
            Ray ray = new Ray(rightControllerModel.transform.position, rightControllerModel.transform.forward);

            int lay = LayerMask.NameToLayer("Detail Object");
            int layermask1 = (1 << lay);

            if (Physics.Raycast(ray, out hit, layermask1))
            {
                measuringStick1.transform.position = hit.point;
                measuringStick1.transform.LookAt(transform.position + hit.normal * 5);
                measuringStick1.transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);

                line.SetPosition(0, rightControllerModel.transform.position);
                line.SetPosition(1, rightControllerModel.transform.position + rightControllerModel.transform.forward * hit.distance);
            }
            else
            {
                line.SetPosition(0, rightControllerModel.transform.position);
                line.SetPosition(1, rightControllerModel.transform.position + rightControllerModel.transform.forward * 100);
            }
        }

        #endregion

        #region Update Tablet's Texts
        // Controlling the Button's Text on the Tablet
        if (selectedObject != null && selectedObject.GetComponentInChildren<Animation>() != null)
        {
            Animation objAnim = selectedObject.GetComponentInChildren<Animation>();

            if (!objAnim.isPlaying)
            {
                //AnimationButtonText.text = "Play:";
            }

        }
        #endregion

        #region Vive Controller Input Control

        if (rightController.GetPressDown(menuButton))
        {
            showingHelp = !showingHelp;
            leftTooltips.SetActive(showingHelp);
            rightTooltips.SetActive(showingHelp);
            //Debug.Log("Presssed BUTTON");
            if (myMaster.GetComponent<MasterController>().isOnline)
            {
                //Debug.Log("Online");
                //myMaster.GetComponent<MasterController>().photonView.RPC("DoSomething", PhotonTargets.All);
            }
        }

        if (rightController.GetPress(menuButton))
        {
            rightMenuButtonHeldTimer += Time.deltaTime;

            if (rightMenuButtonHeldTimer > .5f)
            {
                tempScale = myCameraRig.transform.localScale.x;
                tempScale += .5f;

                if(tempScale >= 100.0f)
                {
                    tempScale = 100.0f;
                }

                myCameraRig.transform.localScale = new Vector3(tempScale, tempScale, tempScale);
                prevScale = tempScale;

                leftControllerScript.transform.GetComponent<NavigationBasicThrust>().ThrustForce = 20 + (tempScale - 1.0f) * 1.5f;
                //rightControllerScript.transform.GetComponent<NavigationBasicThrust>().ThrustForce = 20 + (tempScale - 1.0f) * 1.5f;
                Headlight.GetComponent<Light>().range = 25 * tempScale;
                RenderSettings.fogDensity = .03f / tempScale;
            }
        }
        else
        {
            rightMenuButtonHeldTimer = 0;
        }

        if (leftController.GetPress(menuButton))
        {
            leftMenuButtonHeldTimer += Time.deltaTime;

            if (leftMenuButtonHeldTimer > .5f)
            {
                tempScale = myCameraRig.transform.localScale.x;
                tempScale -= .5f;

                if (tempScale <= 1.0f)
                {
                    tempScale = 1.0f;
                }

                myCameraRig.transform.localScale = new Vector3(tempScale, tempScale, tempScale);
                prevScale = tempScale;

                leftControllerScript.transform.GetComponent<NavigationBasicThrust>().ThrustForce = 20 + (tempScale - 1.0f) * 1.5f;
                //rightControllerScript.transform.GetComponent<NavigationBasicThrust>().ThrustForce = 20 + (tempScale - 1.0f)*1.5f;
                Headlight.GetComponent<Light>().range = 25 * tempScale;
                RenderSettings.fogDensity = .03f / tempScale;
            }
        }
        else
        {
            leftMenuButtonHeldTimer = 0;
        }

        if (rightController.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
        {
            
            if(myMaster.GetComponent<MasterController>().isOnline && timeHeldTrigger == 0.0f)
            {
                //myMaster.GetComponent<MasterController>().photonView.RPC("DisplayLaser", PhotonTargets.All);
                myMaster.GetComponent<MasterController>().DisplayLaser();
            }
            timeHeldTrigger += Time.deltaTime;

            if (!hasHitButton && !hasPressedAButton)
            {
                if (isMeasuringFirst)
                {
                    isMeasuringFirst = false;
                    isMeasuringSecond = true;
                    hasHitButton = true;
                    transform.GetComponent<AudioSource>().clip = buttonSound;
                    transform.GetComponent<AudioSource>().Play();
                    VibrateRight();
                }
                else if (isMeasuringSecond)
                {
                    LineRenderer line = transform.GetComponent<LineRenderer>();

                    line.SetPosition(0, rightControllerModel.transform.position);
                    line.SetPosition(1, rightControllerModel.transform.position);

                    line.enabled = false;

                    isMeasuringSecond = false;
                    rightControllerModel.transform.parent.GetComponent<VREasy.SteamControllerGrab>().isMeasuring = false;
                    //leftControllerModel.transform.parent.GetComponent<VREasy.SteamControllerGrab>().isMeasuring = false;
                    hasHitButton = true;
                    transform.GetComponent<AudioSource>().clip = buttonSound;
                    transform.GetComponent<AudioSource>().Play();
                    VibrateRight();
                }
            }
            
            hasPressedAButton = true;
        }
        else
        {
            
            if (myMaster.GetComponent<MasterController>().isOnline && timeHeldTrigger > 0.0f)
            {
                //myMaster.GetComponent<MasterController>().photonView.RPC("HideLaser", PhotonTargets.All);
                myMaster.GetComponent<MasterController>().HideLaser();
            }

            hasPressedAButton = false;
            hasHitButton = false;
            timeHeldTrigger = 0;
        }

        /*
        if(rightController.GetPressUp(aButton))
        {
            hasPressedAButton = false;
            hasHitButton = false;
        }
        */

        if (leftController.GetPress(bButton))
        {
            if(leftTouchHeldTimer == 0)
            {
                Vector2 touchpad = leftController.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);

                //Debug.Log("X: " + touchpad.x + ". Y: " + touchpad.y);

                if (Mathf.Abs(touchpad.x) < .7 && Mathf.Abs(touchpad.y) > .3)
                {
                    if (touchpad.y > .3)
                    {
                        //up
                        //Debug.Log("up");
                    }
                    else if (touchpad.y < -.3)
                    {
                        //down
                        //Debug.Log("down");
                    }
                }
                else
                {
                    if (touchpad.x > .5)
                    {
                        /*
                        waypointIndex++;
                        if(waypointIndex >= waypoints.Length)
                        {
                            waypointIndex = 0;
                        }
                        //right
                        //Debug.Log("right");
                        myCameraRig.transform.position = waypoints[waypointIndex].position;
                        */
                        //Vector3 myPos = myCamera.transform.position;
                        //myCameraRig.transform.Rotate(0, 45, 0);
                        myCameraRig.transform.RotateAround(myCamera.transform.position, Vector3.up, 45);
                        //myCamera.transform.position = myPos;
                    }
                    else if (touchpad.x < -.5)
                    {
                        /*
                        waypointIndex--;
                        if (waypointIndex < 0)
                        {
                            waypointIndex = waypoints.Length - 1;
                        }
                        myCameraRig.transform.position = waypoints[waypointIndex].position;
                        //left
                        //Debug.Log("left");
                        */
                        //Vector3 myPos = myCamera.transform.position;
                        //myCameraRig.transform.Rotate(0, 45, 0);
                        myCameraRig.transform.RotateAround(myCamera.transform.position, Vector3.up, -45);
                        //myCamera.transform.position = myPos;
                    }
                    else
                    {
                        //Debug.Log("middle");
                        //middle
                    }
                }
            }

            leftTouchHeldTimer += Time.deltaTime;
            if (leftTouchHeldTimer >= 4)
            {
                if (SceneManager.GetActiveScene().name.Equals("BFN_Scene"))
                {
                    SceneManager.LoadScene("PC_Scene");
                }
                else if (SceneManager.GetActiveScene().name.Equals("PC_Scene"))
                {
                    SceneManager.LoadScene("LIQ_Scene");
                }
                else if (SceneManager.GetActiveScene().name.Equals("LIQ_Scene"))
                {
                    SceneManager.LoadScene("BFN_Scene");
                }
            }
            
        }
        else
        {
            leftTouchHeldTimer = 0;
        }

        if (rightController.GetPress(gripButton))
        {
            if (testValve != null)
            {
                //rightControllerModel.transform.parent.GetComponent<HingeJoint>().connectedBody = testValve.GetComponent<Rigidbody>();
                //rightControllerModel.transform.parent.GetComponent<HingeJoint>().connectedAnchor = testValve.transform.position;
                //rightControllerModel.transform.parent.GetComponent<HingeJoint>().anchor = transform.position;
                //testValve.GetComponent<ConfigurableJoint>().connectedBody = rightControllerModel.transform.parent.GetComponent<Rigidbody>();
            }
        }
        else
        {
            //rightControllerModel.transform.parent.GetComponent<HingeJoint>().connectedBody = null;
            //testValve.GetComponent<ConfigurableJoint>().connectedBody = null;
        }

        if(selectedObject != null && rightController.GetPressDown(gripButton))
        {
            
            animIndex++;
            Animation anim = selectedObject.GetComponentInChildren<Animation>();
            int c = 0;
            foreach (AnimationState state in anim)
            {
                c++;
            }

            if (animIndex > (c - 1))
            {
                animIndex = 0;
            }

            c = 0;
            foreach (AnimationState state in anim)
            {
                if (c == animIndex)
                {
                    if (anim.IsPlaying(state.clip.name))
                    {
                        if (state.speed > 0)
                        {
                            state.speed = 0;
                        }
                        else
                        {
                            state.speed = 1;
                        }

                    }
                    else
                    {
                        anim.Play(state.clip.name);
                    }
                }
                c++;
            }

            /*
            if (!hasStartedScaling)
            {
                hasStartedScaling = true;
                controllerDist = Vector3.Distance(leftControllerModel.transform.position, rightControllerModel.transform.position);
                currentScale = myCameraRig.transform.localScale.x;
                tempScale = currentScale;
                gripScale = 1;
            }
            else
            {
                tempScale = currentScale;
                gripScale = controllerDist/(Vector3.Distance(leftControllerModel.transform.position, rightControllerModel.transform.position));


                tempScale *= gripScale;

                tempScale = Mathf.Clamp(tempScale, 1, 100);

                GradientAlphaKey[] gak;
                gak = new GradientAlphaKey[2];
                gak[0].alpha = 0.0F;
                gak[0].time = 0.0F;
                gak[1].alpha = (1.0f/tempScale);
                gak[1].time = 1.0F;

                ValveFog.GetComponent<ValveFog>().gradient.SetKeys(ValveFog.GetComponent<ValveFog>().gradient.colorKeys, gak);

                ValveFog.GetComponent<ValveFog>().GenerateTexture();

                leftControllerScript.transform.GetComponent<NavigationBasicThrust>().ThrustForce = 20 * tempScale;
                rightControllerScript.transform.GetComponent<NavigationBasicThrust>().ThrustForce = 20 * tempScale;
                Headlight.GetComponent<Light>().range = 25 * tempScale;

                tempScale = Mathf.Lerp(prevScale, tempScale, .1f);
                myCameraRig.transform.localScale = new Vector3(tempScale, tempScale, tempScale);
                prevScale = tempScale;
            }
            */
        }
        else
        {
            //currentScale = tempScale;
            //hasStartedScaling = false;
        }

        if (selectedObject != null && leftController.GetPressDown(gripButton))
        {
            Animation anim = selectedObject.GetComponentInChildren<Animation>();
            int c = 0;
            foreach (AnimationState state in anim)
            {
                if (c == animIndex)
                {
                    if (anim.IsPlaying(state.clip.name))
                    {
                        if (state.speed > 0)
                        {
                            state.speed = 0;
                        }
                        else
                        {
                            state.speed = 1;
                        }

                    }
                    else
                    {
                        anim.Play(state.clip.name);
                    }
                }
                c++;
            }
        }

        

        #endregion

        #region Hide/Show Tablet
        // Controlling the Tablet being hidden and activated again
        if (leftController.GetPressDown(menuButton))
        {
            if (TabletObject.active)
            {
                TabletObject.active = false;
                leftControllerPointerController.enabled = true;
                //leftControllerModel.active = true;
            }
            else
            {
                TabletObject.active = true;
                leftControllerPointerController.enabled = false;
                //leftControllerModel.active = false;
            }
        }
        #endregion

        #region Update Northing, Easting, Water Depth

        float n = CartesianOffset.y - (myCamera.transform.position.z- cartesianRefObj.transform.position.z)*3.28f;
        float e = CartesianOffset.x - (myCamera.transform.position.x - cartesianRefObj.transform.position.x)*3.28f;

        //NorthingText.GetComponent<TextMesh>().text = "N: " + n.ToString("N0");
        //EastingText.GetComponent<TextMesh>().text = "E: " + e.ToString("N0");

        #region Update Depth

        // the depth is just the difference between the y positions of our water surface object (set in inspector), and the players y position
        depth = waterSurfaceObj.transform.position.y - myCamera.transform.position.y;
        depth *= 3.28084f;

        depth += depthOffset;
        if (depth <= 0)
        {
            depth = depth * -1;
            string dString = depth.ToString("0");
            //depthText.text = "D = (+)" + dString + " ft";
            if(UnderwaterFogVolume != null)
            {
                if(UnderwaterFogVolume.activeSelf)
                {
                    UnderwaterFogVolume.SetActive(false);
                }
            }
        }
        else
        {
            string dString = depth.ToString("0");
            //depthText.text = "D = (-)" + dString + " ft";
            if (UnderwaterFogVolume != null)
            {
                if (!UnderwaterFogVolume.activeSelf)
                {
                    UnderwaterFogVolume.SetActive(true);
                }
            }
        }

        #endregion

        #endregion

        #region Update Compass

        // Controlling the Compass Bar
        Vector3 dir = myCamera.transform.forward;
        dir.y = 0;

        dir.Normalize();

        angle = Mathf.Atan2(dir.x, dir.z) * 57.2958f;

        float why = 1;

        //angle = Mathf.SmoothDampAngle(prevAngle, angle, ref why, 0);

        if(angle < 0)
        {
            angle += 360;
        }

        //RotationText.GetComponent<TextMesh>().text = "HDG: " + angle.ToString("N0") + "°";

        //Compass.transform.eulerAngles = new Vector3(0, 0, angle);

        //Compass.transform.localEulerAngles = new Vector3(0, -46.819f, angle);

        prevAngle = angle;
        #endregion

        #region Control Screenshot

        // Controlling the screenshot countdown
        if (isTakingScreenshot)
        {
            if(screenshotCountdownTimer < 0)
            {
                // tell my Screenshot script to take a screenshot
                myScreenshotScript.isTakingScreenshot = true;

                isTakingScreenshot = false;
                ScreenShotCountdownText.active = false;
            }
            else
            {
                screenshotCountdownTimer -= Time.deltaTime;
                ScreenShotCountdownText.GetComponent<TextMesh>().text = "Get ready\n" + screenshotCountdownTimer.ToString("F1");
            }
        }

        #endregion


        //Vector3 gazeRay = myCamera.transform.forward.normalized;

        //Debug.DrawLine(transform.position, transform.position + gazeRay * 5);

        myCollider.center = new Vector3(myCamera.transform.localPosition.x, .5f, myCamera.transform.localPosition.z);

        UpdateAxisLineRends();

        ladderTimeout += Time.deltaTime;

    }

    private void FixedUpdate()
    {
        /*
        Vector3 gazeRay = myCamera.transform.forward.normalized;
        RaycastHit hit;

        var layerMask = (1 << 8);
        layerMask = ~layerMask;

        if (Physics.Raycast(transform.position, gazeRay, out hit, 10,layerMask))
        {
            
        }
        */
    }

    private void DelayedStart()
    {
        oldMat = rightControllerModel.transform.Find("trigger").GetComponent<MeshRenderer>().material;

        rightControllerModel.transform.Find("body").GetComponent<MeshRenderer>().material = newMat;
        rightControllerModel.transform.Find("body").GetComponent<MeshRenderer>().material.color = new Color(1,1,1);
        rightControllerModel.transform.Find("body").GetComponent<MeshRenderer>().material.SetFloat("_Inside", .1f);
        rightControllerModel.transform.Find("body").GetComponent<MeshRenderer>().material.SetFloat("_Rim", 0);

        rightControllerModel.transform.Find("lgrip").GetComponent<MeshRenderer>().material = newMat;
        rightControllerModel.transform.Find("lgrip").GetComponent<MeshRenderer>().material.color = new Color(1, 1, 1);
        rightControllerModel.transform.Find("lgrip").GetComponent<MeshRenderer>().material.SetFloat("_Inside", .1f);
        rightControllerModel.transform.Find("lgrip").GetComponent<MeshRenderer>().material.SetFloat("_Rim", 0);

        rightControllerModel.transform.Find("rgrip").GetComponent<MeshRenderer>().material = newMat;
        rightControllerModel.transform.Find("rgrip").GetComponent<MeshRenderer>().material.color = new Color(1, 1, 1);
        rightControllerModel.transform.Find("rgrip").GetComponent<MeshRenderer>().material.SetFloat("_Inside", .1f);
        rightControllerModel.transform.Find("rgrip").GetComponent<MeshRenderer>().material.SetFloat("_Rim", 0);

        rightControllerModel.transform.Find("trackpad").GetComponent<MeshRenderer>().material = newMat;
        rightControllerModel.transform.Find("trackpad").GetComponent<MeshRenderer>().material.color = new Color(1, 1, 1);
        rightControllerModel.transform.Find("trackpad").GetComponent<MeshRenderer>().material.SetFloat("_Inside", .1f);
        rightControllerModel.transform.Find("trackpad").GetComponent<MeshRenderer>().material.SetFloat("_Rim", 0);

        rightControllerModel.transform.Find("sys_button").GetComponent<MeshRenderer>().material = newMat;
        rightControllerModel.transform.Find("sys_button").GetComponent<MeshRenderer>().material.color = new Color(1, 1, 1);
        rightControllerModel.transform.Find("sys_button").GetComponent<MeshRenderer>().material.SetFloat("_Inside", .1f);
        rightControllerModel.transform.Find("sys_button").GetComponent<MeshRenderer>().material.SetFloat("_Rim", 0);

        rightControllerModel.transform.Find("button").GetComponent<MeshRenderer>().material = newMat;
        rightControllerModel.transform.Find("button").GetComponent<MeshRenderer>().material.color = new Color(1, 1, 1);
        rightControllerModel.transform.Find("button").GetComponent<MeshRenderer>().material.SetFloat("_Inside", .1f);
        rightControllerModel.transform.Find("button").GetComponent<MeshRenderer>().material.SetFloat("_Rim", 0);

        rightControllerModel.transform.Find("trigger").GetComponent<MeshRenderer>().material = newMat;
        rightControllerModel.transform.Find("trigger").GetComponent<MeshRenderer>().material.color = Color.yellow;

        hasStarted = true;
    }

    public void resetSelectedObject()
    {
        if (selectedObject != null && !hasHitButton)
        {
            selectedObject.GetComponent<VREasy.VRGrabbable>().isResetting = true;
            //selectedObject.position = selectedObjectOriginalPosition;
            //selectedObject.eulerAngles = selectedObjectOriginalRotation;
            VibrateRight();
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
        }
        hasHitButton = true;
    }

    public void spawnMeasuringSticks()
    {
        if (!hasHitButton)
        {
            /*
            Vector3 realCameraForward = myCamera.transform.forward;
            realCameraForward.y = 0;
            realCameraForward.Normalize();

            measuringStick0.transform.position = myCamera.transform.position + myCamera.transform.forward * .5f;
            measuringStick1.transform.position = myCamera.transform.position + myCamera.transform.forward * .5f + myCamera.transform.right * .5f;
            */

            isMeasuringFirst = true;
            rightControllerModel.transform.parent.GetComponent<VREasy.SteamControllerGrab>().isMeasuring = true;
            //leftControllerModel.transform.parent.GetComponent<VREasy.SteamControllerGrab>().isMeasuring = true;

            LineRenderer line = transform.GetComponent<LineRenderer>();
            line.enabled = true;
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }

        hasHitButton = true;
    }

    public void changeToVertical()
    {
        if (!hasHitButton)
        {
            if (selectedObject != null)
            {
                selectedObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
            movingVeritcal = true;
        }
        hasHitButton = true;
    }

    public void changeToHorizontal()
    {
        if (!hasHitButton)
        {
            if (selectedObject != null)
            {
                selectedObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
            }
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
            movingVeritcal = false;
        }
        hasHitButton = true;
    }

    public void hideSelectedObject()
    {
        
        bool doHide = true;

        if (!hasHitButton)
        {
            Renderer[] rends = selectedObject.GetComponentsInChildren<Renderer>();
            for (int ii = 0; ii < rends.Length; ii++)
            {
                if (ii == 0)
                {
                    if (rends[ii].material.GetFloat("_Mode") > 1)
                    {
                        doHide = false;
                    }
                }

                if (doHide)
                {
                    HideButtonText.text = "Show:";
                    rends[ii].material.SetFloat("_Mode", 2);
                    rends[ii].material.color = new Color(1, 1, 1, .1f);
                    rends[ii].material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                    rends[ii].material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    rends[ii].material.SetInt("_ZWrite", 0);
                    rends[ii].material.DisableKeyword("_ALPHATEST_ON");
                    rends[ii].material.EnableKeyword("_ALPHABLEND_ON");
                    rends[ii].material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    rends[ii].material.renderQueue = 3000;
                    selectedObject.GetComponent<VREasy.VRGrabbable>().isHidden = true;
                }
                else
                {
                    HideButtonText.text = "Hide:";
                    rends[ii].material.SetFloat("_Mode", 1);
                    selectedObject.GetComponent<VREasy.VRGrabbable>().paintColours(true);
                    rends[ii].material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    rends[ii].material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    rends[ii].material.SetInt("_ZWrite", 1);
                    rends[ii].material.DisableKeyword("_ALPHATEST_ON");
                    rends[ii].material.DisableKeyword("_ALPHABLEND_ON");
                    rends[ii].material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    rends[ii].material.renderQueue = -1;
                    selectedObject.GetComponent<VREasy.VRGrabbable>().isHidden = false;
                }
            }
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }

        hasHitButton = true;
    }

    public void playObjectAnimation()
    {
        if (!hasHitButton)
        {
            if (selectedObject != null)
            {
                Animation objAnim = selectedObject.GetComponentInChildren<Animation>();

                if (objAnim.isPlaying)
                {
                    foreach (AnimationState state in objAnim)
                    {
                        if (state.speed > 0)
                        {
                            state.speed = 0;
                            AnimationButtonText.text = "Play:";
                        }
                        else
                        {
                            state.speed = 1;
                            AnimationButtonText.text = "Stop:";
                        }
                    }
                    transform.GetComponent<AudioSource>().clip = buttonSound;
                    transform.GetComponent<AudioSource>().Play();
                    VibrateRight();
                }
                else
                {
                    foreach (AnimationState state in objAnim)
                    {
                        state.speed = 1;
                        AnimationButtonText.text = "Stop:";
                    }
                    transform.GetComponent<AudioSource>().clip = buttonSound;
                    transform.GetComponent<AudioSource>().Play();
                    VibrateRight();
                    objAnim.Play();
                }

                
            }
        }

        hasHitButton = true;
    }

    public void resetAnimation()
    {
        if (selectedObject != null && !hasHitButton)
        {
            Animation objAnim = selectedObject.GetComponentInChildren<Animation>();
            foreach (AnimationState state in objAnim)
            {
                state.speed = 0;
            }

            objAnim.Rewind();
            AnimationButtonText.text = "Play:";

            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }

        hasHitButton = true;
    }

    public void takeScreenShot()
    {
        if (!hasHitButton)
        {
            isTakingScreenshot = true;
            screenshotCountdownTimer = 3;
            ScreenShotCountdownText.active = true;
            ScreenShotCountdownText.GetComponent<TextMesh>().text = screenshotCountdownTimer.ToString("F1");
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void loadSceneChangers()
    {
        if (!hasHitButton)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            /*
            if (!sceneChangers[0].active)
            {
                for (int x = 0; x < sceneChangers.Length; x++)
                {
                    sceneChangers[x].SetActive(true);
                }
            }
            else
            {
                for (int x = 0; x < sceneChangers.Length; x++)
                {
                    sceneChangers[x].SetActive(false);
                }
            }
            */
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }

        hasHitButton = true;
    }

    public void ActivateEditPage()
    {
        if (!hasHitButton)
        {
            editPage.SetActive(true);
            dataPage.SetActive(false);
            assetPage.SetActive(false);
            multiplayerPage.SetActive(false);
            animationPage.SetActive(false);
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void ActivateDataPage()
    {
        if (!hasHitButton)
        {
            editPage.SetActive(false);
            dataPage.SetActive(true);
            assetPage.SetActive(false);
            multiplayerPage.SetActive(false);
            animationPage.SetActive(false);
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void ActivateAssetPage()
    {
        if (!hasHitButton)
        {
            editPage.SetActive(false);
            dataPage.SetActive(false);
            assetPage.SetActive(true);
            multiplayerPage.SetActive(false);
            animationPage.SetActive(false);
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void ActivateAnimationPage()
    {
        if (!hasHitButton)
        {
            editPage.SetActive(false);
            dataPage.SetActive(false);
            assetPage.SetActive(false);
            multiplayerPage.SetActive(false);
            animationPage.SetActive(true);
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void ActivateMultiplayerPage()
    {
        if (!hasHitButton && myMaster.GetComponent<MasterController>().isOnline)
        {
            editPage.SetActive(false);
            dataPage.SetActive(false);
            assetPage.SetActive(false);
            multiplayerPage.SetActive(true);
            animationPage.SetActive(false);
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void VibrateLeft()
    {
        if (leftController != null)
        {
            leftController.TriggerHapticPulse(2000);
        }
    }

    public void VibrateRight()
    {
        if (rightController != null)
        {
            rightController.TriggerHapticPulse(2000);
        }
    }

    public void VibrateRightSmall()
    {
        rightController.TriggerHapticPulse(500);
    }

    public void ToggleDayNight()
    {
        if (!hasHitButton)
        {
            isDay = !isDay;

            if (isDay)
            {
                RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Skybox;
                RenderSettings.ambientIntensity = 1f;
                RenderSettings.reflectionIntensity = .5f;
                RenderSettings.fogDensity = .01f;
                RenderSettings.fog = false;
                RenderSettings.skybox = (Material)Resources.Load("WispySkyboxMat");
                myCamera.GetComponent<Camera>().clearFlags = CameraClearFlags.Skybox;

                if (myCamera.transform.Find("PlanktonParticles"))
                {
                    myCamera.transform.Find("PlanktonParticles").gameObject.SetActive(false);
                }

                if (DayPP_Profile != null)
                {
                    DayPP_Profile.SetActive(true);
                }
                if (NightPP_Profile != null)
                {
                    NightPP_Profile.SetActive(false);
                }

                foreach (GameObject day_light in Day_Lights_List)
                {
                    if (day_light != null)
                    {
                        day_light.SetActive(true);
                    }
                    else
                    {
                        Debug.LogError("Day Light is null.");
                    }
                }

                foreach (GameObject night_light in Night_Lights_List)
                {

                    if (night_light != null)
                    {
                        night_light.SetActive(false);
                    }
                    else
                    {
                        Debug.LogError("Night Light is null.");
                    }
                }
            }
            else
            {
                if (myMaster.GetComponent<MasterController>().isUnderwaterScene)
                {
                    RenderSettings.ambientIntensity = 0f;
                    RenderSettings.reflectionIntensity = 0f;


                    myCamera.GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
                    RenderSettings.fogDensity = .3f;
                    //RenderSettings.fog = true;
                    RenderSettings.fog = false;
                    RenderSettings.fogColor = Color.black;
                    myCamera.GetComponent<Camera>().backgroundColor = Color.black;
                    if (myCamera.transform.Find("PlanktonParticles"))
                    {
                        myCamera.transform.Find("PlanktonParticles").gameObject.SetActive(true);
                    }
                }
                else
                {
                    RenderSettings.ambientIntensity = .3f;
                    RenderSettings.reflectionIntensity = 0f;
                    RenderSettings.skybox = (Material)Resources.Load("MilkyWay");

                    myCamera.GetComponent<Camera>().clearFlags = CameraClearFlags.Skybox;
                    //RenderSettings.fogDensity = .03f;
                    RenderSettings.fog = false;
                }

                if (DayPP_Profile != null)
                {
                    DayPP_Profile.SetActive(false);
                }
                if (NightPP_Profile != null)
                {
                    NightPP_Profile.SetActive(true);
                }

                foreach (GameObject day_light in Day_Lights_List)
                {
                    if (day_light != null)
                    {
                        day_light.SetActive(false);
                    }
                    else
                    {
                        Debug.LogError("Day Light is null.");
                    }
                }

                foreach (GameObject night_light in Night_Lights_List)
                {
                    if (night_light != null)
                    {
                        night_light.SetActive(true);
                    }
                    else
                    {
                        Debug.LogError("Night Light is null.");
                    }
                }
            }

            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void ToggleHUD()
    {
        if (!hasHitButton)
        {
            if (HUD.activeSelf)
            {
                HUD.SetActive(false);
                NorthingText.SetActive(false);
                EastingText.SetActive(false);
                DepthText.SetActive(false);
                RotationText.SetActive(false);
                MiniMapMesh.SetActive(false);
                Compass.SetActive(false);
            }
            else
            {
                HUD.SetActive(true);
                NorthingText.SetActive(true);
                EastingText.SetActive(true);
                DepthText.SetActive(true);
                RotationText.SetActive(true);
                MiniMapMesh.SetActive(true);
                Compass.SetActive(true);
            }

            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void DuplicateObject()
    {
        if (!hasHitButton)
        {
            if (myMaster.GetComponent<MasterController>().isOnline)
            {
                if (selectedObject.GetComponent<VREasy.TransformController>().myPrefab != null)
                {
                    myMaster.GetComponent<MasterController>().DuplicateObjectOnline(selectedObject.GetComponent<VREasy.TransformController>().myPrefab, selectedObject.transform.position + Vector3.right * 7, selectedObject.transform.rotation);
                    transform.GetComponent<AudioSource>().clip = buttonSound;
                    transform.GetComponent<AudioSource>().Play();
                    VibrateRight();
                }
            }
            else
            {
                if (selectedObject.GetComponent<VREasy.TransformController>().myPrefab != null)
                {
                    GameObject bro = Instantiate(selectedObject.GetComponent<VREasy.TransformController>().myPrefab, selectedObject.transform.position + Vector3.right * 7, selectedObject.transform.rotation);
                    transform.GetComponent<AudioSource>().clip = buttonSound;
                    transform.GetComponent<AudioSource>().Play();
                    VibrateRight();
                }
            }
        }
        hasHitButton = true;
    }

    public void DeleteObject()
    {
        if (!hasHitButton)
        {
            Destroy(selectedObject.gameObject);
            selectedObject = null;
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void AddRotation()
    {
        if (!hasHitButton)
        {
            bool xAxis = selectedObject.transform.GetComponent<VREasy.TransformController>().xAxis;
            bool yAxis = selectedObject.transform.GetComponent<VREasy.TransformController>().yAxis;
            bool zAxis = selectedObject.transform.GetComponent<VREasy.TransformController>().zAxis;

            if (xAxis)
            {
                selectedObject.transform.Rotate(45, 0, 0);
            }
            else if (yAxis)
            {
                selectedObject.transform.Rotate(0, 45, 0);
            }
            else if (zAxis)
            {
                selectedObject.transform.Rotate(0, 0, 45);
            }

            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void SubRotation()
    {
        if (!hasHitButton)
        {
            bool xAxis = selectedObject.transform.GetComponent<VREasy.TransformController>().xAxis;
            bool yAxis = selectedObject.transform.GetComponent<VREasy.TransformController>().yAxis;
            bool zAxis = selectedObject.transform.GetComponent<VREasy.TransformController>().zAxis;

            if (xAxis)
            {
                selectedObject.transform.Rotate(-45, 0, 0);
            }
            else if (yAxis)
            {
                selectedObject.transform.Rotate(0, -45, 0);
            }
            else if (zAxis)
            {
                selectedObject.transform.Rotate(0, 0, -45);
            }
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void MakeJumper()
    {
        if (!hasHitButton)
        {
            isMakingJumper = true;
            isMakingJumper2 = false;
            //treeHub = null;
            //otherHub = null;
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void Deselect()
    {
        if (!hasHitButton)
        {
            Renderer[] renders = selectedObject.transform.GetComponentsInChildren<Renderer>();
            for (int ii = 0; ii < renders.Length; ii++)
            {
                List<Material> materialList = new List<Material>();
                materialList.Add(renders[ii].materials[0]);
                renders[ii].materials = materialList.ToArray();
            }

            selectedObject.transform.GetComponent<VREasy.VRGrabbable>().paintColours(true);
            selectedObject = null;
            transform.GetComponent<AudioSource>().clip = buttonSound;
            transform.GetComponent<AudioSource>().Play();
            VibrateRight();
        }
        hasHitButton = true;
    }

    public void InsertTabletInfo()
    {
        foreach (Transform child in dataPage.transform.Find("Canvas").Find("ObjectDataBoxVR").Find("ScrollRect").Find("Panel"))
        {
            Destroy(child.gameObject);
        }
        foreach (string infoValue in selectedObject.GetComponent<VREasy.VRGrabbable>().infoValues)
        {
            GameObject newText = Instantiate(Resources.Load("InfoTextVR", typeof(GameObject))) as GameObject;
            newText.GetComponent<Text>().text = infoValue;
            newText.transform.SetParent(dataPage.transform.Find("Canvas").Find("ObjectDataBoxVR").Find("ScrollRect").Find("Panel").transform);
            newText.transform.localScale = new Vector3(1, 1, 1);
            newText.transform.localPosition = new Vector3(newText.transform.localPosition.x, newText.transform.localPosition.y, 0);
            newText.transform.localRotation = Quaternion.identity;

            //GameObject seperator = Instantiate(Resources.Load("SeperatorImage", typeof(GameObject))) as GameObject;
            //seperator.transform.SetParent(ObjectDataBox.transform.Find("ScrollRect").Find("Panel").transform);
        }
    }

    public void UpdateAxisLineRends()
    {
        if (selectedObject != null)
        {
            if (selectedObject.GetComponent<VREasy.TransformController>() != null)
            {
                RigidbodyConstraints objConstraints = selectedObject.GetComponent<Rigidbody>().constraints;
                bool isXFrozenGood = (objConstraints & RigidbodyConstraints.FreezePositionX) != RigidbodyConstraints.None;
                bool isYFrozenGood = (objConstraints & RigidbodyConstraints.FreezePositionY) != RigidbodyConstraints.None;
                bool isZFrozenGood = (objConstraints & RigidbodyConstraints.FreezePositionZ) != RigidbodyConstraints.None;
                // if currently moving an object in horizontal plane
                if (!isXFrozenGood)
                {
                    // set our horizontal line positions
                    objXLineRend.GetComponent<LineRenderer>().SetPosition(0, selectedObject.transform.position + Vector3.right * 3);
                    objXLineRend.GetComponent<LineRenderer>().SetPosition(1, selectedObject.transform.position + Vector3.right * -3);

                    objZLineRend.GetComponent<LineRenderer>().SetPosition(0, selectedObject.transform.position + Vector3.forward * 3);
                    objZLineRend.GetComponent<LineRenderer>().SetPosition(1, selectedObject.transform.position + Vector3.forward * -3);

                    // activate our horizontal axis lines
                    objXLineRend.SetActive(true);
                    objZLineRend.SetActive(true);
                }
                else
                {
                    // activate our horizontal axis lines
                    objXLineRend.SetActive(false);
                    objZLineRend.SetActive(false);
                }

                if (!isYFrozenGood)
                {
                    // set our vertical line positions
                    objYLineRend.GetComponent<LineRenderer>().SetPosition(0, selectedObject.transform.position + Vector3.up * 3);
                    objYLineRend.GetComponent<LineRenderer>().SetPosition(1, selectedObject.transform.position + Vector3.up * -3);

                    // activate our vertical axis line
                    objYLineRend.SetActive(true);
                }
                else
                {
                    // activate our vertical axis line
                    objYLineRend.SetActive(false);
                }
            }
            else
            {
                objXLineRend.SetActive(false);
                objZLineRend.SetActive(false);
                objYLineRend.SetActive(false);
            }
        }
        else
        {
            objXLineRend.SetActive(false);
            objZLineRend.SetActive(false);
            objYLineRend.SetActive(false);
        }
    }

    #region VPLink Manipulation Methods

    public void CloseDec()
    {
        if (selectedObject != null)
        {
            if (selectedObject.GetComponent<VREasy.TransformController>().isValve)
            {
                selectedObject.GetComponent<VREasy.VRGrabbable>().CloseDec();
                transform.GetComponent<AudioSource>().Play();
            }
        }
    }

    public void CloseFully()
    {
        if (selectedObject != null)
        {
            transform.GetComponent<AudioSource>().Play();
            selectedObject.GetComponent<VREasy.VRGrabbable>().CloseFully();
        }
    }

    public void OpenInc()
    {
        if (selectedObject != null)
        {
            if (selectedObject.GetComponent<VREasy.TransformController>().isValve)
            {
                transform.GetComponent<AudioSource>().Play();
                selectedObject.GetComponent<VREasy.VRGrabbable>().OpenInc();
            }
        }
    }

    public void OpenFully()
    {
        if (selectedObject != null)
        {
            transform.GetComponent<AudioSource>().Play();
            selectedObject.GetComponent<VREasy.VRGrabbable>().OpenFully();
        }
    }

    public void OpenAmount(float amount)
    {
        if (selectedObject != null)
        {
            transform.GetComponent<AudioSource>().Play();
            selectedObject.GetComponent<VREasy.VRGrabbable>().OpenAmount(amount);
        }
    }

    public void CloseAmount(float amount)
    {
        if (selectedObject != null)
        {
            transform.GetComponent<AudioSource>().Play();
            selectedObject.GetComponent<VREasy.VRGrabbable>().CloseAmount(amount);
        }
    }

    public void Open1Percent()
    {
        OpenAmount(1);
    }

    public void Open2Percent()
    {
        OpenAmount(2);
    }

    public void Open5Percent()
    {
        OpenAmount(5);
    }

    public void Open10Percent()
    {
        OpenAmount(10);
    }

    public void Open50Percent()
    {
        OpenAmount(50);
    }

    public void Close1Percent()
    {
        CloseAmount(1);
    }

    public void Close2Percent()
    {
        CloseAmount(2);
    }

    public void Close5Percent()
    {
        CloseAmount(5);
    }

    public void Close10Percent()
    {
        CloseAmount(10);
    }

    public void Close50Percent()
    {
        CloseAmount(50);
    }

    #endregion

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class ListedAnimatorController : MonoBehaviour
{

    public GameObject animationObject;
    public string clipName;

    public float animSpeed;
    public float animNormTime;

    private Animator animator;

    private MasterController myMasterScript;

    // Use this for initialization
    void Start()
    {
        myMasterScript = GameObject.Find("Master").GetComponent<MasterController>();
        if (animationObject != null)
        {
            animator = animationObject.GetComponent<Animator>();
        }

        animator.SetFloat("speedMultiplier", 1f);
    }

    // Update is called once per frame
    void Update()
    {
        if(animator.GetFloat("speedMultiplier") > 0)
        {
            animator.SetFloat("normTime", animator.GetFloat("normTime") + (Time.deltaTime / animator.GetCurrentAnimatorStateInfo(0).length));
        }
    }

    public void playAnimator()
    {
        //if(animator.playbackTime)
        animator.SetFloat("speedMultiplier", 1.0f);

        /*
        foreach ( state in animator.GetCurrentAnimatorStateInfo(0))
        {
            if (state.clip.name.Equals(clipName))
            {
                if (animation.IsPlaying(state.clip.name))
                {
                    state.speed = 1;
                }
                else
                {
                    animation.Play(state.clip.name);
                    state.speed = 1;
                }
            }
        }
       */
    }

    public void pauseAnimator()
    {
        animator.SetFloat("speedMultiplier", 0.0000001f);
        /*
        foreach (AnimationState state in animation)
        {
            if (state.clip.name.Equals(clipName))
            {
                if (animation.IsPlaying(state.clip.name))
                {
                    state.speed = 0;
                }
                else
                {
                    animation.Play(state.clip.name);
                    state.speed = 0;
                }
            }
        }
        */
    }

    public void setMasterAnimation()
    {
        if (!myMasterScript.tabletScript.hasHitButton)
        {
            myMasterScript.selectedAnimatorController = transform.GetComponent<ListedAnimatorController>();
            myMasterScript.tabletScript.transform.GetComponent<AudioSource>().clip = myMasterScript.tabletScript.buttonSound;
            myMasterScript.tabletScript.transform.GetComponent<AudioSource>().Play();
            myMasterScript.tabletScript.VibrateRight();
        }
        myMasterScript.tabletScript.hasHitButton = true;
    }
}

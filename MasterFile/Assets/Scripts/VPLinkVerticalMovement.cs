﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VPLinkVerticalMovement : MonoBehaviour {

    private float _newPosition;
    public string vpTagname;
	private VPDatabase vpd;

	// Use this for initialization
    void Start()
    {
        vpd = FindObjectOfType<VPDatabase>();
        if (vpd == null)
        {
            Debug.LogError("Cannot find VP Link databasein the VerMovement object");
        }
        else
        {
            if (vpTagname.Length > 0)
            {
                _newPosition = vpd.PVE(vpTagname, transform.position.y);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
		if (vpd != null) {
			if (vpTagname.Length > 0) {
				_newPosition = vpd.PVE (vpTagname, transform.position.y);
				transform.position = new Vector3 (transform.position.x, _newPosition, transform.position.z);
			}
		}
    }
}

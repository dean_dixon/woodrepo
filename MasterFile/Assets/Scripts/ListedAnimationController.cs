﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListedAnimationController : MonoBehaviour {

    public GameObject animationObject;
    public string clipName;
    private Animation animation;

    private MasterController myMasterScript;

	// Use this for initialization
	void Start () {
        if (animationObject != null)
        {
            animation = animationObject.GetComponent<Animation>();
        }
        myMasterScript = GameObject.Find("Master").GetComponent<MasterController>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void playAnimation()
    {
        foreach (AnimationState state in animation)
        {
            if (state.clip.name.Equals(clipName))
            {
                if (animation.IsPlaying(state.clip.name))
                {
                    state.speed = 1;
                }
                else
                {
                    animation.Play(state.clip.name);
                    state.speed = 1;
                }
            }
        }
    }

    public void pauseAnimation()
    {
        foreach (AnimationState state in animation)
        {
            if (state.clip.name.Equals(clipName))
            {
                if (animation.IsPlaying(state.clip.name))
                {
                    state.speed = 0;
                }
                else
                {
                    animation.Play(state.clip.name);
                    state.speed = 0;
                }
            }
        }
    }

    public void setMasterAnimation()
    {
        if (!myMasterScript.tabletScript.hasHitButton)
        {
            myMasterScript.selectedAnimationController = transform.GetComponent<ListedAnimationController>();
            myMasterScript.tabletScript.transform.GetComponent<AudioSource>().clip = myMasterScript.tabletScript.buttonSound;
            myMasterScript.tabletScript.transform.GetComponent<AudioSource>().Play();
            myMasterScript.tabletScript.VibrateRight();
        }
        myMasterScript.tabletScript.hasHitButton = true;
    }
}

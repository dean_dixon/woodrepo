﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPointerController : MonoBehaviour {

    VR_Tablet_Script myTabletScript;
    public GameObject endPoint;
    bool isOnSomething = false;
    string objName = "";
    float hoverTimer = 0;
    float onlineHeldTimer = 0;
    float triggerHeldTimer = 0;
    MasterController myMasterScript;
    GameObject otherPlayer;

    bool hasTransferred = false;
    // Use this for initialization
    void Start () {

        GameObject Master = GameObject.Find("Master");
        if (Master != null)
        {
            myMasterScript = Master.transform.GetComponent<MasterController>();
        }

        GameObject PlayerTablet = GameObject.Find("Player/[CameraRig]/Controller (left)/TabletParent");
        if (PlayerTablet != null)
        {
            myTabletScript = PlayerTablet.transform.GetComponent<VR_Tablet_Script>();
        }
        //endPoint = GameObject.Find("EndPoint").gameObject;
        //endPoint.GetComponentInChildren<Renderer>().material.color = new Color(0, 1, 1);
    }
	
	// Update is called once per frame
	void Update () {

        if (myTabletScript != null)
        {
            if (myTabletScript.rightController != null)
            {
                if (myTabletScript.rightController.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
                {
                    triggerHeldTimer += Time.deltaTime;
                }
                else
                {
                    triggerHeldTimer = 0;
                }
            }
        }

        hoverTimer += Time.deltaTime;
        if(hoverTimer > .5f)
        {
            objName = "";
        }

        LineRenderer line = transform.GetComponent<LineRenderer>();

        line.SetPosition(0, transform.position - transform.forward*.15f);

        Vector3 fwd = transform.forward;
        RaycastHit hit;
        Ray ray = new Ray(transform.position - transform.forward * .15f, transform.forward);

        int lay = LayerMask.NameToLayer("Selectable");
        int lay2 = LayerMask.NameToLayer("VRButton");
        int layermask1 = (1 << lay) | (1<< lay2);

        if (Physics.Raycast(ray, out hit, 1000, layermask1))
        {
            line.SetColors(new Color(0, 0, 1), new Color(0, 1, 1));
            line.SetPosition(1, transform.position + transform.forward * hit.distance);
 
            transform.Find("PointerText").GetComponent<TextMesh>().gameObject.SetActive(true);
            transform.Find("PointerText").GetComponent<TextMesh>().text = hit.transform.name;
            hoverTimer = 0;

            objName = hit.transform.name;

            if (hit.transform.tag.Equals("OnlinePlayer"))
            {
                transform.GetComponent<MeshRenderer>().material.color = new Color(1, 0, 1, .9f);
                endPoint.transform.position = line.GetPosition(1);
                onlineHeldTimer += Time.deltaTime;
                otherPlayer = hit.transform.gameObject;
            }
            else if (hit.transform.tag.Equals("Button"))
            {
                transform.GetComponent<MeshRenderer>().material.color = new Color(0, 1, 1, .9f);
                endPoint.transform.position = new Vector3(0, 0, 0);
                onlineHeldTimer = 0;
                otherPlayer = null;
                hasTransferred = false;
            }
            else if(hit.transform.tag.Equals("Selectable") || hit.transform.tag.Equals("ROV"))
            {
                transform.GetComponent<MeshRenderer>().material.color = new Color(1, 0, 0, .9f);
                endPoint.transform.position = line.GetPosition(1);
                onlineHeldTimer = 0;
                otherPlayer = null;
                hasTransferred = false;
                //Debug.Log(objName);
                if (hit.transform.name.Equals("ProxyCube"))
                {
                    if (myTabletScript.rightController.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
                    {
                        //myTabletScript.rightControllerScript.transform.GetComponent<VREasy.VRSelector>().ActivateGrabbable(hit.transform.parent.GetComponent<VREasy.VRGrabbable>());
                        //hit.transform.parent.GetComponent<VREasy.VRGrabbable>().StartGrab(myTabletScript.rightControllerScript.transform.GetComponent<VREasy.PointerSelector>());
                    }
                }
            }
            else
            {
                transform.GetComponent<MeshRenderer>().material.color = new Color(1, 0, 0, .9f);
                endPoint.transform.position = new Vector3(9999, 9999, 9999);
                onlineHeldTimer = 0;
                otherPlayer = null;
                hasTransferred = false;
            }
        }
        else
        {
            transform.Find("PointerText").GetComponent<TextMesh>().gameObject.SetActive(false);
            line.SetColors(new Color(1, 1, 1), new Color(1, 1, 1));
            line.SetPosition(1, transform.position + transform.forward * 100);
            endPoint.transform.position = new Vector3(9999, 9999, 9999);
            transform.GetComponent<MeshRenderer>().material.color = new Color(1, 0, 0, .9f);
            onlineHeldTimer = 0;
        }

        if (myTabletScript.isMeasuringFirst || myTabletScript.isMeasuringSecond)
        {
            onlineHeldTimer = 0;
            endPoint.transform.position = new Vector3(0, 0, 0);
            triggerHeldTimer = 0;
        }

        if(triggerHeldTimer > 3.0f && onlineHeldTimer > 3.0f && otherPlayer != null && !hasTransferred)
        {
            if (PhotonNetwork.isMasterClient)
            {
                myMasterScript.TranferAllOwnership(otherPlayer.GetComponent<PhotonView>());
                hasTransferred = true;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationStepTextController : MonoBehaviour {

    public Text textToChange;
    public string[] stepTexts;
    public int currentStep;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (currentStep <= stepTexts.Length - 1)
        {
            if (currentStep <= 11)
            {
                textToChange.text = "Step " + (currentStep - 2) + ": " + stepTexts[currentStep];
            }
            else
            {
                textToChange.text = "Step " + (currentStep - 3) + ": " + stepTexts[currentStep];
            }
        }
        else
        {
            textToChange.text = "Error.";
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Net;
using System.IO;
using System;
using System.Text.RegularExpressions;
using System.Text;
using SimpleJSON;

public class GIS_Test : MonoBehaviour {

    public string restURL = "https://www.arcgis.com/sharing/rest/oauth2/token";
    public string url = "https://wp-app-arcgis-5.woodgroup.com:6443/arcgis/rest/services/GoM/GC903_MapService/MapServer/0/query?where=TagNumber+LIKE+%27%25HB%25%27&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=&returnGeometry=true&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentsOnly=false&datumTransformation=&parameterValues=&rangeValues=&f=pjson";
    public string tokenURL = "http://wp-app-arcgis-5.woodgroup.com:6443/arcgis/tokens/";
    public string tokenURL2 = "https://wp-app-arcgis-5.woodgroup.com:6443/arcgis/tokens/generateToken";
    public string mapServerURL = "https://wp-app-arcgis-5.woodgroup.com:6443/arcgis/rest/services/GoM/GC903_MapService/MapServer/0";
    string cookie = null;

    private MasterController myMasterScript;

    private void Awake()
    {
        if (GameObject.Find("Master"))
        {
            myMasterScript = GameObject.Find("Master").transform.GetComponent<MasterController>();
        }

        StartCoroutine(Upload());

        //StartCoroutine(Load());
    }

    void Start()
    {
        /*
        if(GameObject.Find("Master"))
        {
            myMasterScript = GameObject.Find("Master").transform.GetComponent<MasterController>();
        }

        StartCoroutine(Upload());

        //StartCoroutine(Load());
        */
    }


    IEnumerator Upload()
    {
        WWWForm form = new WWWForm();
        form.AddField("username", "Wood_VR_Team");
        form.AddField("password", "WoodVRTeam");
        form.AddField("slbxClient", "requestip");
        form.AddField("slbxClientUi", "requestip");

        using ( UnityWebRequest www = UnityWebRequest.Post(tokenURL, form))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }

        WWWForm form2 = new WWWForm();
        form2.AddField("token", "dW2o6oAyI8TsqFBu7YCPtx_4AOk--MXibGEdQxsxQsaoqnrZ39eIZf_KtxDWW9j0");

        using (UnityWebRequest www2 = UnityWebRequest.Post("https://wp-app-arcgis-5.woodgroup.com:6443/arcgis/rest/services/GoM/GC903_MapService/MapServer/0/query?where=TagNumber+LIKE+%27%25HB%25%27&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=true&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentsOnly=false&datumTransformation=&parameterValues=&rangeValues=&f=pjson", form2))
        {
            yield return www2.SendWebRequest();
            if (www2.isNetworkError || www2.isHttpError)
            {
                Debug.Log(www2.error);
            }
            else
            {
                //Debug.Log("Form upload complete 3!");
                //Debug.Log(www2.downloadHandler.text);
                string input = www2.downloadHandler.text;
                string splitter = "attributes";
                char[] split = splitter.ToCharArray();
                string[] strings = input.Split(split, StringSplitOptions.None);

                string[] stringSeparators = new string[] { "attributes" };
                var result = input.Split(stringSeparators, StringSplitOptions.None);

                int x = 0;
                foreach (string s in result)
                {
                    if (x>0)
                    {
                        String regex = "\\s+(?=((\\\\[\\\\\"]|[^\\\\\"])*\"(\\\\[\\\\\"]|[^\\\\\"])*\")*(\\\\[\\\\\"]|[^\\\\\"])*$)";

                        string s22 = s;
                        s22 = s22.Replace("\n", String.Empty);
                        s22 = s22.Replace("\r", String.Empty);
                        s22 = s22.Replace("\t", String.Empty);
                        s22 = s22.Remove(0, 3);
                        s22 = s22.Remove(s22.Length - 4, 4);
                        s22 = Regex.Replace(s22, regex, "");
                        s22 = s22.Replace("{", "");
                        s22 = s22.Replace("}", "");
                        s22 = s22.Replace("\"", "");
                        s22 = s22.Replace("geometry:", "");
                        Debug.Log(s22);

                        string[] stringSeparators2 = new string[] { "," };
                        var result2 = s22.Split(stringSeparators2, StringSplitOptions.None);

                        foreach (string s3 in result2)
                        {
                            //Debug.Log(s3);
                            string[] stringSeparators3 = new string[] { ":" };
                            var result3 = s3.Split(stringSeparators3, StringSplitOptions.None);
                            if(result3[0].Equals("Name"))
                            {
                                Debug.Log("Found name column");
                                Debug.Log(result3[1]);
                                if (GameObject.Find(result3[1]))
                                {
                                    Debug.Log("Found object " + result3[1]);
                                    GameObject obj = GameObject.Find(result3[1]);
                                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                                    if (objScript != null)
                                    {
                                        if(!myMasterScript.allAssets.Contains(obj))
                                        {
                                            myMasterScript.allAssets.Add(obj);
                                        }

                                        foreach (string s4 in result2)
                                        {
                                            string[] stringSeparators4 = new string[] { ":" };
                                            var result4 = s4.Split(stringSeparators3, StringSplitOptions.None);
                                            objScript.infoValues.Add(result4[0] + " : " + result4[1]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    x++;
                }

                using (StringReader reader = new StringReader(www2.downloadHandler.text))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if(line.Contains("attributes"))
                        {
                            //Debug.Log(line);
                        }
                    }
                }
            }
        }
        using (UnityWebRequest www3 = UnityWebRequest.Post("https://wp-app-arcgis-5.woodgroup.com:6443/arcgis/rest/services/GoM/GC903_MapService/MapServer/0/1",form))
        {
            yield return www3.SendWebRequest();
            if (www3.isNetworkError || www3.isHttpError)
            {
                Debug.Log(www3.error);
            }
            else
            {
                //Debug.Log("Form upload complete 4!");
                Debug.Log(www3.downloadHandler.text);
            }
        }
    }
}

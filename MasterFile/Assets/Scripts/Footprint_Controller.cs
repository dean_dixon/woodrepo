﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footprint_Controller : MonoBehaviour {

    Vector3 prevPosition;
    int trailIndex = 0;

    public List<GameObject> trailPoints =  new List<GameObject>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (trailPoints.Count > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, trailPoints[trailIndex].transform.position, 1.75f * Time.deltaTime);

            if (Vector3.Distance(transform.position, trailPoints[trailIndex].transform.position) < .05f)
            {
                trailIndex++;
                if (trailIndex >= trailPoints.Count)
                {
                    trailIndex = 0;
                    Destroy(transform.gameObject);
                }
            }


            Vector3 movement = transform.position - prevPosition;
            transform.rotation = Quaternion.LookRotation(movement);
            prevPosition = transform.position;
        }
    }
}

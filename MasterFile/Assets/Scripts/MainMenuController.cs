﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {
    
    public WorldMasterController worldMasterScript;
    public GameObject MainMenu;
    public GameObject NameMenu;
    public InputField nameField;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ActivateNameMenu()
    {
        MainMenu.SetActive(false);
        NameMenu.SetActive(true);
    }

    public void LoadLoadingScene()
    {
        SceneManager.LoadScene(1);
    }

    public void SetName()
    {
        worldMasterScript.isOnline = true;
        worldMasterScript.playerName = nameField.text;
        LoadLoadingScene();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform cameraOrbit;
    public Transform target;

    public float rotateSpeed = 8f;

    void Start()
    {
        cameraOrbit.position = target.position;
    }

    void Update()
    {
        if (Input.GetMouseButton(2) && Input.GetKey(KeyCode.LeftAlt))
        {
            float h = rotateSpeed * Input.GetAxis("Mouse X");
            float v = rotateSpeed * Input.GetAxis("Mouse Y");

            if (cameraOrbit.transform.eulerAngles.z + v <= 0.1f || cameraOrbit.transform.eulerAngles.z + v >= 179.9f)
                v = 0;

            cameraOrbit.transform.eulerAngles = new Vector3(cameraOrbit.transform.eulerAngles.x, cameraOrbit.transform.eulerAngles.y + h, cameraOrbit.transform.eulerAngles.z + v);
        }
        else if (Input.GetMouseButton(2))
        {
            float h = Input.GetAxis("Mouse X");
            float v = Input.GetAxis("Mouse Y");

            Vector3 forwardDir = transform.up;
            //forwardDir.y = 0;
            forwardDir.Normalize();

            Vector3 rightDir = transform.right;
            rightDir.y = 0;
            rightDir.Normalize();

            Vector3 rightMove = rightDir * h;
            Vector3 upMove = forwardDir * v;


            cameraOrbit.transform.parent.position -= Input.GetAxis("Mouse X") * rightDir;
            cameraOrbit.transform.parent.position -= Input.GetAxis("Mouse Y") * forwardDir;
        }

        float scrollFactor = Input.GetAxis("Mouse ScrollWheel");
        //Vector3 moveFactor 

        if (scrollFactor != 0)
        {
            transform.GetComponent<Camera>().orthographicSize -= scrollFactor * 5;
            //cameraOrbit.transform.localScale = cameraOrbit.transform.localScale * (1f - scrollFactor);
        }


        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);

        transform.LookAt(target.position);
    }
}

﻿/*
 * ****************************************************************
 * MasterController is an object designed to control the scene. The script is usually placed in a GameObject named 'Master'
 * It keeps track of if the game is online, using VPLink, the player's name, or any other values that might be static throughout the scene's lifetime.
 * It controls saving and loading
 * It loads data from .text files that can be used to place information on objects
 * It controls the multiplayer side of things (connecting to Photon NameServer, joining lobbies and rooms)
 ********************************************************************
*/


using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VR;
using Photon;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Formatters.Binary;

public class MasterController : Photon.PunBehaviour
{
    public bool isVPLink;
    public bool isUnderwaterScene;

    // Reference to the Virtual Reality Player GameObject
    public GameObject Player_VR;
    // Reference to the Mouse and Keyboard Player GameObject
    public GameObject Player_MK;
    // Reference to the Mouse and Keyboard Player's UI GameObject
    public GameObject Player_MK_UI;

    // Reference to the Mouse and Keyboard Player's UI GameObject
    public GameObject DesktopAnimPlayButton;

    // Reference to the Mouse and Keyboard Player's UI GameObject
    public GameObject DesktopAnimPauseButton;

    // Reference to the Mouse and Keyboard Player's UI GameObject
    public GameObject VRAnimPlayButton;

    // Reference to the Mouse and Keyboard Player's UI GameObject
    public GameObject VRAnimPauseButton;

    public Scrollbar desktopAnimationScrollbar;

    public VREasy.VRSlider VRAnimationScrollbar;

    // Reference to the Mouse and Keyboard Player's UI GameObject
    public GameObject[] ROV_Collider_Objects;

    // Reference to the Ultimate(World) Master Controller
    [HideInInspector]
    public WorldMasterController worldMasterScript;

    // Reference to Player_VR VR_Tablet_Script
    [HideInInspector]
    public VR_Tablet_Script tabletScript;

    // static values inherited from WorldMasterController
    [HideInInspector]
    public bool isVR = false, isOnline = false;

    // The Player's name to display if the session is Online.
    [HideInInspector]
    public string playerName = "Player";

    // Keeps track of it this player made his own room. If the player made a room, he is the host!
    private bool isHost = false;

    // List<GameObject> allAssets keep a list of every GameObject with a <VREasy.VRGrabbable> script attatched to it.
    [HideInInspector]
    public List<GameObject> allAssets = new List<GameObject>();

    // List<GameObject> saveObjects is a List of <SaveObject> used to save every GameObject's (with a <VREasy.VRGrabbable> script attatched to it) position and rotation.
    [HideInInspector]
    public List<SaveObject> saveObjects = new List<SaveObject>();

    float disableTimer = 0;
    bool hasDisabled = false;

    [HideInInspector]
    public GameObject myOnlineRightController;

    [HideInInspector]
    public GameObject myOnlineDesktopAvatar;


    public ListedAnimationController selectedAnimationController;
    public ListedAnimatorController selectedAnimatorController;

    public struct OnlinePlayer
    {
        public int playerID;
        public string playerName;
    }

    public struct PlayerAction
    {
        public int type; //0-move, 1-rotate, 2-delete, 3-duplicate
        public GameObject changedObject;
        public GameObject deletedObjPrefab;
        public GameObject duplicatedObject;
        public Quaternion prevRotation;
        public Vector3 prevPosition;
    }

    [HideInInspector]
    public List<OnlinePlayer> onlinePlayerList = new List<OnlinePlayer>();

    [HideInInspector]
    public Stack<PlayerAction> prevPlayerActions = new Stack<PlayerAction>();

    [HideInInspector]
    public PlayerAction currentPlayerAction;

    private void Awake()
    {
        PhotonNetwork.automaticallySyncScene = true;
    }

    // Use this for initialization
    void Start()
    {
        currentPlayerAction = new PlayerAction();

        if (GameObject.Find("WorldMasterController") != null)
        {
            worldMasterScript = GameObject.Find("WorldMasterController").GetComponent<WorldMasterController>();
            isOnline = worldMasterScript.isOnline;
            playerName = worldMasterScript.playerName;
        }

        #region File Reading

        #region Jumper Data

        TextAsset allLines = Resources.Load("K2_Data") as TextAsset;
        string fs = allLines.text;
        string[] fLines = Regex.Split(fs, "\\r?\\n");
        string[] descriptors = fLines[0].Split('\t');

        int count = 0;
        foreach (string line in fLines)
        {
            if (count != 0)
            {
                string[] fields = line.Split('\t');
                //Debug.Log("count = " + count + ". " + fields[0]);
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                    if (objScript != null)
                    {
                        //objScript.infoValues.Clear();
                        int c = 0;
                        allAssets.Add(obj);
                        foreach (string str in fields)
                        {
							if (!str.Equals(""))
                            {
                                objScript.infoValues.Add(descriptors[c] + " : " + str);
                            }
                            //Debug.Log(descriptors[c] + " : " + str);
                            c++;
                        }
                    }
                }
            }

            count++;
        }
        #endregion

        #region Manifold Data

        allLines = Resources.Load("Manifold") as TextAsset;
        fs = allLines.text;
        fLines = Regex.Split(fs, "\\r?\\n");
        descriptors = fLines[0].Split('\t');

        count = 0;
        foreach (string line in fLines)
        {
            if (count != 0)
            {
                string[] fields = line.Split('\t');
                //Debug.Log("count = " + count + ". " + fields[0]);
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                    if (objScript != null)
                    {
                        //objScript.infoValues.Clear();
                        int c = 0;
                        allAssets.Add(obj);
                        foreach (string str in fields)
                        {
                            if (!str.Equals(""))
                            {
                                objScript.infoValues.Add(descriptors[c] + " : " + str);
                            }
                            //Debug.Log(descriptors[c] + " : " + str);
                            c++;
                        }
                    }
                }
            }

            count++;
        }
        #endregion

        #region Plem_Plet Data

        allLines = Resources.Load("PLEM & PLET") as TextAsset;
        fs = allLines.text;
        fLines = Regex.Split(fs, "\\r?\\n");
        descriptors = fLines[0].Split('\t');

        count = 0;
        foreach (string line in fLines)
        {
            if (count != 0)
            {
                string[] fields = line.Split('\t');
                //Debug.Log("count = " + count + ". " + fields[0]);
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                    if (objScript != null)
                    {
                        //objScript.infoValues.Clear();
                        int c = 0;
                        allAssets.Add(obj);
                        foreach (string str in fields)
                        {
                            if (!str.Equals(""))
                            {
                                objScript.infoValues.Add(descriptors[c] + " : " + str);
                            }
                            //Debug.Log(descriptors[c] + " : " + str);
                            c++;
                        }
                    }
                }
            }

            count++;
        }
        #endregion

        #region XT Data

        allLines = Resources.Load("XT") as TextAsset;
        fs = allLines.text;
        fLines = Regex.Split(fs, "\\r?\\n");
        descriptors = fLines[0].Split('\t');

        count = 0;
        foreach (string line in fLines)
        {
            if (count != 0)
            {
                string[] fields = line.Split('\t');
                //Debug.Log("count = " + count + ". " + fields[0]);
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                    if (objScript != null)
                    {
                        //objScript.infoValues.Clear();
                        int c = 0;
                        allAssets.Add(obj);
                        foreach (string str in fields)
                        {
                            if (!str.Equals(""))
                            {
                                objScript.infoValues.Add(descriptors[c] + " : " + str);
                            }
                            //Debug.Log(descriptors[c] + " : " + str);
                            c++;
                        }
                    }
                }
            }

            count++;
        }
        #endregion

        #region Other Data

        allLines = Resources.Load("Other") as TextAsset;
        fs = allLines.text;
        fLines = Regex.Split(fs, "\\r?\\n");
        descriptors = fLines[0].Split('\t');

        count = 0;
        foreach (string line in fLines)
        {
            if (count != 0)
            {
                string[] fields = line.Split('\t');
                //Debug.Log("count = " + count + ". " + fields[0]);
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                    if (objScript != null)
                    {
                        //objScript.infoValues.Clear();
                        int c = 0;
                        allAssets.Add(obj);
                        foreach (string str in fields)
                        {
                            objScript.infoValues.Add(descriptors[c] + " : " + str);
                            //Debug.Log(descriptors[c] + " : " + str);
                            c++;
                        }
                    }
                }
            }

            count++;
        }
        #endregion

        #region ROV Data

        allLines = Resources.Load("ROV_Clear") as TextAsset;
        fs = allLines.text;
        fLines = Regex.Split(fs, "\\r?\\n");
        descriptors = fLines[0].Split('\t');

        count = 0;
        foreach (string line in fLines)
        {
            if (count != 0)
            {
                string[] fields = line.Split('\t');
                //Debug.Log("count = " + count + ". " + fields[0]);
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                    if (objScript != null)
                    {
                        //objScript.infoValues.Clear();
                        int c = 0;
                        allAssets.Add(obj);
                        foreach (string str in fields)
                        {
                            objScript.infoValues.Add(descriptors[c] + " : " + str);
                            //Debug.Log(descriptors[c] + " : " + str);
                            c++;
                        }
                    }
                }
            }

            count++;
        }
        #endregion

        #endregion

        #region Setup Desktop Asset List Box

        if (GameObject.Find("AssetListBox"))
        {
            GameObject panel = GameObject.Find("AssetListBox").transform.Find("ScrollRect").transform.Find("Panel").gameObject;

            foreach (GameObject asset in allAssets)
            {
                GameObject newText = Instantiate(Resources.Load("AssetText", typeof(GameObject))) as GameObject;
                newText.GetComponent<ListedAssetController>().myAsset = asset;
                //newText.GetComponent<ListedAssetController>().myVRScript = Player_VR.transform.Find("[CameraRig]").Find("Controller (left)").Find("TabletParent").GetComponent<VR_Tablet_Script>();
                newText.GetComponent<ListedAssetController>().myPlayerScript = Player_MK.GetComponent<PlayerController>();
                newText.GetComponent<ListedAssetController>().myMasterScript = transform.GetComponent<MasterController>();
                newText.GetComponent<Text>().text = asset.name;
                newText.transform.SetParent(panel.transform);
                newText.transform.localScale = new Vector3(1, 1, 1);

                GameObject seperator = Instantiate(Resources.Load("SeperatorImage", typeof(GameObject))) as GameObject;
                seperator.transform.SetParent(panel.transform);
            }
        }

        #endregion

        #region Setup VR Asset List Box

        if (Player_VR.transform.Find("[CameraRig]").Find("Controller (left)").Find("TabletParent").Find("Tablet").Find("Asset Page").Find("Canvas").Find("AssetListBoxVR"))
        {
            GameObject panel = Player_VR.transform.Find("[CameraRig]").Find("Controller (left)").Find("TabletParent").Find("Tablet").Find("Asset Page").Find("Canvas").Find("AssetListBoxVR").Find("ScrollRect").Find("Panel").gameObject;

            foreach (GameObject asset in allAssets)
            {
                GameObject newText = Instantiate(Resources.Load("AssetTextVR", typeof(GameObject))) as GameObject;
                newText.GetComponent<ListedAssetController>().myAsset = asset;
                newText.GetComponent<ListedAssetController>().myVRScript = Player_VR.transform.Find("[CameraRig]").Find("Controller (left)").Find("TabletParent").GetComponent<VR_Tablet_Script>();
                //newText.GetComponent<ListedAssetController>().myPlayerScript = Player_MK.GetComponent<PlayerController>();
                newText.GetComponent<ListedAssetController>().myMasterScript = transform.GetComponent<MasterController>();
                newText.GetComponent<Text>().text = asset.name;
                newText.transform.SetParent(panel.transform);
                newText.transform.localScale = new Vector3(1, 1, 1);
                newText.transform.localPosition = new Vector3(newText.transform.localPosition.x, newText.transform.localPosition.y, 0);
                newText.transform.localRotation = Quaternion.identity;

                //GameObject seperator = Instantiate(Resources.Load("SeperatorImage", typeof(GameObject))) as GameObject;
                //seperator.transform.SetParent(panel.transform);
            }
        }

        #endregion

        tabletScript = Player_VR.transform.Find("[CameraRig]").Find("Controller (left)").Find("TabletParent").GetComponent<VR_Tablet_Script>();
        if (UnityEngine.XR.XRDevice.isPresent)
        {
            isVR = true;
            UnityEngine.XR.XRSettings.enabled = true;
            Player_MK.SetActive(false);
            Player_MK_UI.SetActive(false);
        }
        else
        {
            isVR = false;

            UnityEngine.XR.XRSettings.enabled = false;
            Player_VR.SetActive(false);
            Player_MK.SetActive(true);
            Player_MK_UI.SetActive(true);

            GameObject.Find("HypotenuseText").GetComponent<MakeBillboard>().cam = Player_MK.transform.Find("Player_Camera").GetComponent<Camera>();
            GameObject.Find("HorizontalText").GetComponent<MakeBillboard>().cam = Player_MK.transform.Find("Player_Camera").GetComponent<Camera>();
            GameObject.Find("VerticalText").GetComponent<MakeBillboard>().cam = Player_MK.transform.Find("Player_Camera").GetComponent<Camera>();

        }

        if (isOnline)
        {
            PhotonNetwork.playerName = playerName;
            PhotonNetwork.ConnectUsingSettings("1");

            PhotonNetwork.sendRate = 20;
            PhotonNetwork.sendRateOnSerialize = 10;
        }


    }

    // Update is called once per frame
    void Update()
    {
        disableTimer += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.V))
        {
            reloadVPLink();
        }

        if (Input.GetKeyDown(KeyCode.PageUp))
        {
            SaveScene();
        }

        if (Input.GetKeyDown(KeyCode.PageDown))
        {
            DeleteFile();
        }

        if (Input.GetKeyDown(KeyCode.Home))
        {
            LoadScene();
        }

        if (disableTimer > 1 && !hasDisabled)
        {
            /*
            if (isVR)
            {
                UnityEngine.XR.XRSettings.enabled = true;
                Player_MK.SetActive(false);
                Player_MK_UI.SetActive(false);
            }
            else
            {
                UnityEngine.XR.XRSettings.enabled = false;
                Player_VR.SetActive(false);
                Player_MK.SetActive(true);
                Player_MK_UI.SetActive(true);

                GameObject.Find("HypotenuseText").GetComponent<MakeBillboard>().cam = Player_MK.transform.Find("Player_Camera").GetComponent<Camera>();
                GameObject.Find("HorizontalText").GetComponent<MakeBillboard>().cam = Player_MK.transform.Find("Player_Camera").GetComponent<Camera>();
                GameObject.Find("VerticalText").GetComponent<MakeBillboard>().cam = Player_MK.transform.Find("Player_Camera").GetComponent<Camera>();
            }
            */
            hasDisabled = true;
        }

        if (!isVR)
        {
            if (selectedAnimationController != null)
            {
                if (selectedAnimationController.animationObject != null)
                {
                    if (selectedAnimationController.animationObject.GetComponent<Animation>().IsPlaying(selectedAnimationController.clipName))
                    {

                        foreach (AnimationState state in selectedAnimationController.animationObject.GetComponent<Animation>())
                        {
                            if (state.clip.name.Equals(selectedAnimationController.clipName))
                            {
                                if (state.speed > 0)
                                {
                                    DesktopAnimPlayButton.SetActive(false);
                                    DesktopAnimPauseButton.SetActive(true);
                                    desktopAnimationScrollbar.value = (state.time / state.length) / 1.0f;
                                }
                                else
                                {
                                    DesktopAnimPlayButton.SetActive(true);
                                    DesktopAnimPauseButton.SetActive(false);
                                    state.time = desktopAnimationScrollbar.value * state.length;
                                }
                            }
                        }
                    }
                    else
                    {
                        DesktopAnimPlayButton.SetActive(true);
                        DesktopAnimPauseButton.SetActive(false);
                        desktopAnimationScrollbar.value = 0;
                    }
                }
            }

            if (selectedAnimatorController != null)
            {
                if ((selectedAnimatorController.animationObject.GetComponent<Animator>().GetFloat("speedMultiplier")) > 0.5f)
                {
                    DesktopAnimPlayButton.SetActive(false);
                    DesktopAnimPauseButton.SetActive(true);
                    desktopAnimationScrollbar.value = selectedAnimatorController.animationObject.GetComponent<Animator>().GetFloat("normTime");
                }
                else
                {
                    DesktopAnimPlayButton.SetActive(true);
                    DesktopAnimPauseButton.SetActive(false);
                    selectedAnimatorController.animationObject.GetComponent<Animator>().SetFloat("normTime", desktopAnimationScrollbar.value);
                }
            }
        }
        else
        {
            if (selectedAnimationController != null)
            {
                if (selectedAnimationController.animationObject != null)
                {
                    if (selectedAnimationController.animationObject.GetComponent<Animation>().IsPlaying(selectedAnimationController.clipName))
                    {

                        foreach (AnimationState state in selectedAnimationController.animationObject.GetComponent<Animation>())
                        {
                            if (state.clip.name.Equals(selectedAnimationController.clipName))
                            {
                                if (state.speed > 0)
                                {
                                    VRAnimPlayButton.SetActive(false);
                                    VRAnimPauseButton.SetActive(true);
                                    //VRAnimationScrollbar.value = (state.time / state.length) / 1.0f;
                                    VRAnimationScrollbar.SetValue((state.time / state.length) / 1.0f);
                                }
                                else
                                {
                                    VRAnimPlayButton.SetActive(true);
                                    VRAnimPauseButton.SetActive(false);
                                    state.time = VRAnimationScrollbar.value * state.length;
                                }
                            }
                        }
                    }
                    else
                    {
                        VRAnimPlayButton.SetActive(true);
                        VRAnimPauseButton.SetActive(false);
                        VRAnimationScrollbar.value = 0;
                    }
                }
            }

            if (selectedAnimatorController != null)
            {
                if ((selectedAnimatorController.animationObject.GetComponent<Animator>().GetFloat("speedMultiplier")) > 0.5f)
                {
                    VRAnimPlayButton.SetActive(false);
                    VRAnimPauseButton.SetActive(true);
                    VRAnimationScrollbar.value = selectedAnimatorController.animationObject.GetComponent<Animator>().GetFloat("normTime");
                }
                else
                {
                    VRAnimPlayButton.SetActive(true);
                    VRAnimPauseButton.SetActive(false);
                    selectedAnimatorController.animationObject.GetComponent<Animator>().SetFloat("normTime", VRAnimationScrollbar.value);
                }
            }
        }
    }

    //Photon Built-In Method. Invoked when the player successfully joins a lobby
    public override void OnJoinedLobby()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    //Photon Built-In Method. Invoked when the playerfails to join a lobby
    void OnPhotonRandomJoinFailed()
    {
        Debug.Log("Can't join random room, so I'm creating my own.");
        isHost = true;
        PhotonNetwork.CreateRoom(null);
    }

    //Photon Built-In Method. Invoked when the player joins a room
    public override void OnJoinedRoom()
    {
        if (isHost)
        {
            Debug.Log("I've connected to my own room!!!");
        }
        else
        {
            Debug.Log("I've connected to an existing room with another player!!!");
        }

        if (isVR)
        {
            GameObject myAvatar = PhotonNetwork.Instantiate("VR_Online_Player", Vector3.zero, Quaternion.identity, 0);
            GameObject leftCont = PhotonNetwork.Instantiate("Online_Vive_Controller_L", Vector3.zero, Quaternion.identity, 0);
            GameObject rightCont = PhotonNetwork.Instantiate("Online_Vive_Controller_R", Vector3.zero, Quaternion.identity, 0);

            myAvatar.GetComponent<NetworkPlayerController>().amIVr = isVR;
            leftCont.GetComponent<NetworkPlayerController>().amIVr = isVR;
            rightCont.GetComponent<NetworkPlayerController>().amIVr = isVR;
            myOnlineRightController = rightCont;

            myAvatar.transform.Find("Avatar_Head").Find("NameText").GetComponent<TextMesh>().text = playerName;
            //myAvatar.transform.GetComponent<NetworkPlayerController>().photonView
            //myAvatar.transform.Find("Avatar_Head").Find("NameText").GetComponent<MakeBillboard>().cam = GameObject.Find("Camera (eye)").GetComponent<Camera>();
        }
        else
        {
            GameObject myAvatar = PhotonNetwork.Instantiate("VR_Online_Player", Vector3.zero, Quaternion.identity, 0);
            myAvatar.GetComponent<NetworkPlayerController>().amIVr = isVR;
            myAvatar.transform.Find("Avatar_Head").Find("NameText").GetComponent<TextMesh>().text = playerName;
            myOnlineDesktopAvatar = myAvatar;
            //myAvatar.transform.Find("Avatar_Head").Find("NameText").GetComponent<MakeBillboard>().cam = GameObject.Find("Player_Camera").GetComponent<Camera>();
        }

        GameObject myVoiceObject = PhotonNetwork.Instantiate("OnlineVoiceObject", Vector3.zero, Quaternion.identity, 0);
    }


    public override void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        OnlinePlayer newPlayer = new OnlinePlayer();
        newPlayer.playerName = player.NickName;
        newPlayer.playerID = player.ID;
        onlinePlayerList.Add(newPlayer);
    }
    // Method for saving the scene
    public void SaveScene()
    {
        //Clear our list of saved objects so that we can update it
        saveObjects.Clear();

        //Get all Objects of type GameObject and store in local GameObject array "allObjects"
        GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();

        //iterate through newly created allObjects array
        foreach (GameObject go in allObjects)
        {
            // if the object has a VREasy.VRGrabbable> script attached
            if (go.GetComponent<VREasy.VRGrabbable>() != null)
            {
                // create a new SaveObject object
                SaveObject save = new SaveObject();

                //put all object's data into the newly created SaveObject object
                save.objName = go.name;
                save.xPos = go.transform.position.x;
                save.xRot = go.transform.rotation.x;
                save.yPos = go.transform.position.y;
                save.yRot = go.transform.rotation.y;
                save.zPos = go.transform.position.z;
                save.zRot = go.transform.rotation.z;
                save.wRot = go.transform.rotation.w;

                //add the new SaveObject to the saveObjects List
                saveObjects.Add(save);
            }
        }

        // Save the file
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd");
        bf.Serialize(file, saveObjects);
        file.Close();
    }

    // Method for loading a save
    public void LoadScene()
    {
        //Clear our list of saved objects so that we can update it
        saveObjects.Clear();

        // check to see if a save file even exists
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            // load the save file
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);

            //Grab all saveObjects in the save file and put them in the saveObjects List
            saveObjects = (List<SaveObject>)bf.Deserialize(file);
            file.Close();
        }

        // iterate through each SaveObject loaded from the file
        foreach (SaveObject save in saveObjects)
        {
            // Find the object needed to be updated by looking for it's name
            GameObject obj = GameObject.Find(save.objName);
            if (obj != null)
            {
                // set the objects position and rotation to the save file's values
                obj.transform.position = new Vector3(save.xPos, save.yPos, save.zPos);
                obj.transform.rotation = new Quaternion(save.xRot, save.yRot, save.zRot, save.wRot);
            }
        }
    }

    // Method for deleting the save
    public void DeleteFile()
    {
        string filePath = Application.persistentDataPath + "/savedGames.gd";

        // check if file exists
        if (!File.Exists(filePath))
        {
        }
        else
        {
            //delete file
            File.Delete(filePath);
        }
    }

    //Photon Remote Procedural Call Test
    [PunRPC]
    public void DoSomething()
    {

    }

    //Photon Remote Procedural Call Test
    [PunRPC]
    public void DoSomething2()
    {
        Player_MK.GetComponent<PlayerController>().ToggleDayNight();
    }


    public void DisplayLaser()
    {
        if (myOnlineRightController != null)
        {
            myOnlineRightController.GetComponent<NetworkPlayerController>().photonView.RPC("DisplayLaser", PhotonTargets.All);
        }
        else
        {
            if (myOnlineDesktopAvatar != null)
            {
                myOnlineDesktopAvatar.GetComponent<NetworkPlayerController>().photonView.RPC("DisplayLaser", PhotonTargets.All);
            }
            else
            {
                Debug.Log("Desktop Avatar is null");
            }
        }
    }


    public void HideLaser()
    {
        if (myOnlineRightController != null)
        {
            //myOnlineRightController.GetComponent<NetworkPlayerController>().HideLaser();
            myOnlineRightController.GetComponent<NetworkPlayerController>().photonView.RPC("HideLaser", PhotonTargets.All);
        }
        else
        {
            if (myOnlineDesktopAvatar != null)
            {
                myOnlineDesktopAvatar.GetComponent<NetworkPlayerController>().photonView.RPC("HideLaser", PhotonTargets.All);
            }
            else
            {
                Debug.Log("Desktop Avatar is null");
            }
        }
    }

    public void ToggleROVCollision()
    {
        if (ROV_Collider_Objects[0].activeSelf)
        {
            ROV_Collider_Objects[0].SetActive(false);
            ROV_Collider_Objects[1].SetActive(true);
        }
        else
        {
            ROV_Collider_Objects[0].SetActive(true);
            ROV_Collider_Objects[1].SetActive(false);
        }

        if (isVR)
        {
            VR_Tablet_Script myTabletScript = Player_VR.transform.Find("[CameraRig]").Find("Controller (left)").Find("TabletParent").GetComponent<VR_Tablet_Script>();
            AudioSource tabletAudio = Player_VR.transform.Find("[CameraRig]").Find("Controller (left)").Find("TabletParent").GetComponent<AudioSource>();

            tabletAudio.clip = myTabletScript.buttonSound;
            tabletAudio.Play();
        }
    }

    public void DuplicateObjectOnline(GameObject prefab, Vector3 pos, Quaternion rot)
    {
        if (photonView.isMine)
        {
            GameObject newObject = PhotonNetwork.Instantiate(prefab.name, pos, rot, 0);
        }
    }

    public void TranferAllOwnership(PhotonView newOwnerView)
    {
        if (photonView.isMine)
        {
            foreach (GameObject obj in allAssets)
            {
                obj.GetComponent<PhotonView>().TransferOwnership(newOwnerView.viewID);
                Debug.Log("Transfered Ownership");
            }
        }
    }

    public void PlaySceneAnimation()
    {
        if (isVR)
        {
            if (!tabletScript.hasHitButton)
            {
                if (selectedAnimationController != null)
                {
                    selectedAnimationController.playAnimation();
                }
                else if(selectedAnimatorController != null)
                {
                    selectedAnimatorController.playAnimator();
                }
                tabletScript.transform.GetComponent<AudioSource>().clip = tabletScript.buttonSound;
                tabletScript.transform.GetComponent<AudioSource>().Play();
                tabletScript.VibrateRight();
            }
            tabletScript.hasHitButton = true;
        }
        else
        {
            if (selectedAnimationController != null)
            {
                selectedAnimationController.playAnimation();
            }
            else if(selectedAnimatorController != null)
            {
                selectedAnimatorController.playAnimator();
            }
        }
    }

    public void PauseSceneAnimation()
    {
        if (isVR)
        {
            if (!tabletScript.hasHitButton)
            {
                if (selectedAnimationController != null)
                {
                    selectedAnimationController.pauseAnimation();
                }
                else if (selectedAnimatorController != null)
                {
                    selectedAnimatorController.pauseAnimator();
                }
                tabletScript.transform.GetComponent<AudioSource>().clip = tabletScript.buttonSound;
                tabletScript.transform.GetComponent<AudioSource>().Play();
                tabletScript.VibrateRight();
            }
            tabletScript.hasHitButton = true;
        }
        else
        {
            if (selectedAnimationController != null)
            {
                selectedAnimationController.pauseAnimation();
            }
            else if (selectedAnimatorController != null)
            {
                selectedAnimatorController.pauseAnimator();
            }
        }
    }

    public void Undo()
    {
        
        PlayerAction action = prevPlayerActions.Peek();

        if(action.type == 0)
        {
            action.changedObject.transform.position = action.prevPosition;
        }
        else if(action.type == 1)
        {
            action.changedObject.transform.rotation = action.prevRotation;
        }
        else if (action.type == 2)
        {
            Instantiate(action.deletedObjPrefab, action.prevPosition, action.prevRotation);
        }

        //Debug.Log("Type = " + action.type);

        prevPlayerActions.Pop();
        
    }

    public void reloadVPLink()
    {
        //Get all Objects of type GameObject and store in local GameObject array "allObjects"
        GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();

        //iterate through newly created allObjects array
        foreach (GameObject go in allObjects)
        {
            // if the object has a VREasy.VRGrabbable> script attached
            if (go.GetComponent<VPLinkEquipment>() != null)
            {
                go.GetComponent<VPLinkEquipment>().reloadConfig();
                if (go.GetComponent<VREasy.VRGrabbable>())
                {
                    go.GetComponent<VREasy.VRGrabbable>().reloadVPLink();
                }
            }
            else if (go.GetComponent<VPLinkTextValue>() != null)
            {
                go.GetComponent<VPLinkTextValue>().reloadConfig();
            }
            else if (go.GetComponent<VPScaler>() != null)
            {
                go.GetComponent<VPScaler>().reloadConfig();
            }
            else if (go.GetComponent<VPButton>() != null)
            {
                go.GetComponent<VPButton>().reloadConfig();
            }
            else if (go.GetComponent<VPColorChanger>() != null)
            {
                go.GetComponent<VPColorChanger>().reloadConfig();
            }
            else if (go.GetComponent<VPLinkRotator>() != null)
            {
                go.GetComponent<VPLinkRotator>().reloadConfig();
            }
            else if (go.GetComponent<VPLight>() != null)
            {
                go.GetComponent<VPLight>().reloadConfig();
            }
        }
    }
}

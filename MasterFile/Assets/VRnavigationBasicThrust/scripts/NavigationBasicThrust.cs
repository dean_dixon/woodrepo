﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class NavigationBasicThrust : MonoBehaviour
{
    public Rigidbody NaviBase;
    public Vector3 ThrustDirection;
    public float ThrustForce, initialThrustForce;
    public bool ShowTrustMockup = true;
    public bool canFly;
    public GameObject ThrustMockup;


    SteamVR_TrackedObject trackedObj;
    FixedJoint joint;
    GameObject attachedObject;
    Vector3 tempVector;

    VR_Tablet_Script playerVRScript;

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    private void Start()
    {
        playerVRScript = transform.Find("TabletParent").GetComponent<VR_Tablet_Script>();
        if (canFly)
        {
            transform.parent.GetComponent<Rigidbody>().useGravity = false;
        }
    }

    void FixedUpdate()
    {
        var device = SteamVR_Controller.Input((int)trackedObj.index);

        // add force
        if (device.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
        {

            if (playerVRScript.canClimb)
            {
                tempVector = Quaternion.Euler(ThrustDirection) * Vector3.forward;
                tempVector = Quaternion.Euler(ThrustDirection) * Vector3.forward;
                tempVector = transform.rotation * tempVector * ThrustForce;
                tempVector.z = 0;
                tempVector.x = 0;
                NaviBase.AddForce(tempVector * device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x);
                NaviBase.maxAngularVelocity = 2f;

                CapsuleCollider col = NaviBase.transform.GetComponent<CapsuleCollider>();
                col.material.dynamicFriction = 0;
                col.material.staticFriction = 0;
            }
            else
            {
                if (!canFly)
                {
                    tempVector = Quaternion.Euler(ThrustDirection) * Vector3.forward;
                    tempVector = transform.rotation * tempVector * ThrustForce;
                    tempVector.y = 0;

                    NaviBase.AddForce(tempVector * device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x);
                    NaviBase.maxAngularVelocity = 2f;
                    CapsuleCollider col = NaviBase.transform.GetComponent<CapsuleCollider>();
                    col.material.dynamicFriction = 0;
                    col.material.staticFriction = 0;
                }
                else
                {
                    tempVector = Quaternion.Euler(ThrustDirection) * Vector3.forward;
                    NaviBase.AddForce(transform.rotation * tempVector * ThrustForce * device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x);
                    NaviBase.maxAngularVelocity = 2f;
                    CapsuleCollider col = NaviBase.transform.GetComponent<CapsuleCollider>();
                    col.material.dynamicFriction = 0;
                    col.material.staticFriction = 0;
                }
            }
        }
        else
        {
            CapsuleCollider col = NaviBase.transform.GetComponent<CapsuleCollider>();
            col.material.dynamicFriction = 100;
            col.material.staticFriction = 100;
        }

        // show trust mockup
        if (ShowTrustMockup && ThrustMockup != null)
        {
            if (attachedObject == null && device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
            {
                attachedObject = (GameObject)GameObject.Instantiate(ThrustMockup, Vector3.zero, Quaternion.identity);
                attachedObject.transform.SetParent(this.transform, false);
                attachedObject.transform.Rotate(ThrustDirection);
            }
            else if (attachedObject != null && device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
            {
                Destroy(attachedObject);
            }
        }
    }
}

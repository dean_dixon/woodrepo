using UnityEngine;
using System.Collections;

public struct PeekHeaderData
{
	public bool readSuccess;
	public long linesRead;
	public double x;
	public double y;
	public double z;
}

// use this in meshes with DX11 mode (because DX11 doesnt support point size)
// original shader by "smb02dunnal" http://forum.unity3d.com/threads/billboard-geometry-shader.169415/
// modified by unitycoder.com, this version supports transform position & scale

Shader "UnityCoder/PointMeshSizeDX11TriBillOffset" 
{
	Properties 
	{
	    _Color ("ColorTint", Color) = (1,1,1,1)
		_Size ("Size", Range(0.001, 1)) = 0.05
	}

	SubShader 
	{
		Pass
		{
			Tags { "RenderType"="Opaque"}
			LOD 200
		
			CGPROGRAM
			#pragma vertex VS_Main
			#pragma fragment FS_Main
			#pragma geometry GS_Main
			
			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
			};
		
			struct GS_INPUT
			{
				float4	pos		: POSITION;
				fixed4 color 	: COLOR;
			};

			struct FS_INPUT
			{
				float4	pos		: POSITION;
				fixed4 color 	: COLOR;
			};

			float _Size;
	        fixed4 _Color;

			GS_INPUT VS_Main(appdata v)
			{
				GS_INPUT o = (GS_INPUT)0;
				o.pos = mul (unity_ObjectToWorld, v.vertex);
				o.color = v.color*_Color;
				return o;
			}

			[maxvertexcount(3)]
			void GS_Main(point GS_INPUT p[1], inout TriangleStream<FS_INPUT> triStream)
			{
				float3 cameraUp = UNITY_MATRIX_IT_MV[1].xyz;
				float3 cameraForward = _WorldSpaceCameraPos - p[0].pos;
				float3 right = normalize(cross(cameraUp, cameraForward));

				float4 v[3];
				v[0] = float4(p[0].pos + _Size * right - _Size * cameraUp, 1.0f);
				v[1] = float4(p[0].pos + (_Size*0.25f) * right + _Size * cameraUp, 1.0f);
				v[2] = float4(p[0].pos - _Size * right - _Size * cameraUp, 1.0f);		

				FS_INPUT newVert;
				newVert.pos = UnityObjectToClipPos(v[0]);
				newVert.color = p[0].color;
				triStream.Append(newVert);
				newVert.pos =  UnityObjectToClipPos(v[1]);
				newVert.color = p[0].color;
				triStream.Append(newVert);
				newVert.pos =  UnityObjectToClipPos(v[2]);
				newVert.color = p[0].color;
				triStream.Append(newVert);
			}

			fixed4 FS_Main(FS_INPUT input) : COLOR
			{
				return input.color;
			}
			ENDCG
		}
	} 
}
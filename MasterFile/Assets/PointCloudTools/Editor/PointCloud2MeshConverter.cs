// Point Cloud to Unity Mesh Converter
// Converts pointcloud data into multiple mesh assets
// http://unitycoder.com

using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEngine.Rendering;

#pragma warning disable 0219 // disable unused var warnings


namespace unitycodercom_PointCloud2MeshConverter
{

    public class PointCloud2MeshConverter : EditorWindow
    {
        private static string appName = "PointCloud2Mesh";
        private Object sourceFile;

        private bool readRGB = false;
        private bool readIntensity = false;
        private bool readNormals = false;
        private bool useUnitScale = true; // scaling enabled
        private float unitScale = 0.001f;
        private bool flipYZ = true;
        private bool autoOffsetNearZero = true; // takes first point value as offset
        private bool enableManualOffset = false;
        private Vector3 manualOffset = Vector3.zero;

        // advanced settings
        private bool addMeshesToScene = true;
        private bool optimizePoints = false; // sort points
        private bool createLODS = false;
        private int lodLevels = 3; // including full mesh (so 2 are generated)
        private int minLodVertexCount = 1000; // last LOD mesh has this many verts
        private bool decimatePoints = false;
        private int removeEveryNth = 5;
        //		private bool forceRecalculateNormals = false;

        // mesh generation stuff
        private int MaxVertexCountPerMesh = 65000;
        private Material meshMaterial;
        private List<Mesh> cloudList = new List<Mesh>();
        private int meshCounter = 1;
        private GameObject folder;
        private long masterPointCount = 0;
        private string savePath;
        private int pcCounter = 0;
        //private bool addToScene = true;

        // create menu item and window
        [MenuItem("Window/PointCloudTools/Convert Point Cloud To Unity Meshes", false, 2)]
        static void Init()
        {
            PointCloud2MeshConverter window = (PointCloud2MeshConverter)EditorWindow.GetWindow(typeof(PointCloud2MeshConverter));
            window.titleContent = new GUIContent(appName);
            window.minSize = new Vector2(340, 570);
            window.maxSize = new Vector2(340, 574);
        }

        // main loop
        void OnGUI()
        {
            // source file
            GUILayout.Label("Point Cloud source file", EditorStyles.boldLabel);
            sourceFile = EditorGUILayout.ObjectField(sourceFile, typeof(Object), false);

            // TODO: only get fileinfo once, no need to request again
            GUILayout.Label(sourceFile != null ? "file:" + GetSelectedFileInfo() : "", EditorStyles.miniLabel);

            EditorGUILayout.Space();

            // start import settings
            GUILayout.Label("Import settings", EditorStyles.boldLabel);

            EditorGUILayout.BeginHorizontal();
            readRGB = EditorGUILayout.ToggleLeft(new GUIContent("Read RGB values", null, "Read R G B values"), readRGB, GUILayout.Width(160));
            readIntensity = EditorGUILayout.ToggleLeft(new GUIContent("Read Intensity value", null, "Read intensity value"), readIntensity);
            readRGB = readIntensity ? false : readRGB;
            EditorGUILayout.EndHorizontal();


            readNormals = EditorGUILayout.ToggleLeft(new GUIContent("Read Normal values (PLY)", null, "Only for .PLY files"), readNormals);

            // extra options
            EditorGUILayout.Space();
            useUnitScale = EditorGUILayout.BeginToggleGroup(new GUIContent("Scale values", null, "Enable scaling"), useUnitScale);
            unitScale = EditorGUILayout.FloatField(new GUIContent(" Scaling multiplier", null, "To scale millimeters to unity meters, use 0.001"), unitScale);
            EditorGUILayout.EndToggleGroup();
            EditorGUILayout.Space();
            flipYZ = EditorGUILayout.ToggleLeft(new GUIContent("Flip Y & Z values", null, "Flip YZ values because Unity Y is up"), flipYZ);
            EditorGUILayout.Space();
            autoOffsetNearZero = EditorGUILayout.ToggleLeft(new GUIContent("Auto-offset near 0,0,0", null, "Takes first line from xyz data as offset"), autoOffsetNearZero);
            enableManualOffset = EditorGUILayout.BeginToggleGroup(new GUIContent("Add manual offset", null, "Add this offset to XYZ values"), enableManualOffset);
            autoOffsetNearZero = enableManualOffset ? false : autoOffsetNearZero;

            manualOffset = EditorGUILayout.Vector3Field(new GUIContent("Offset" + (flipYZ ? " (added after YZ values flip)" : ""), null, ""), manualOffset);
            EditorGUILayout.EndToggleGroup();
            EditorGUILayout.Space();

            // advanced settings
            EditorGUILayout.Space();
            GUILayout.Label("Advanced Settings", EditorStyles.boldLabel);
            EditorGUILayout.Space();
            addMeshesToScene = EditorGUILayout.ToggleLeft(new GUIContent("Add Meshes to current scene", null, "Creates mesh objects and adds them to this scene"), addMeshesToScene);
            optimizePoints = EditorGUILayout.ToggleLeft(new GUIContent("Sort points in X axis", null, "Sorts points on X axis"), optimizePoints);
            //			forceRecalculateNormals = EditorGUILayout.ToggleLeft(new GUIContent("Force RecalculateNormals()",null,"Note: Uses builtin RecalculateNormals(), it wont give correct normals"), forceRecalculateNormals);
            createLODS = EditorGUILayout.ToggleLeft(new GUIContent("Create LODS", null, ""), createLODS);
            GUI.enabled = createLODS;
            lodLevels = EditorGUILayout.IntSlider(new GUIContent("LOD levels:", null, "Including LOD0 (main) mesh level"), lodLevels, 2, 4);
            //minLodVertexCount = EditorGUILayout.IntSlider(new GUIContent("Minimum LOD point count:",null,"How many points in the last (furthest) LOD mesh"), minLodVertexCount, 1, Mathf.Clamp(vertCount-1,1,65000));
            GUI.enabled = true;

            decimatePoints = EditorGUILayout.ToggleLeft(new GUIContent("Decimate points", null, "Skip rows from data"), decimatePoints);
            GUI.enabled = decimatePoints;
            removeEveryNth = EditorGUILayout.IntField(new GUIContent("Remove every Nth point", null, ""), Mathf.Clamp(removeEveryNth, 0, 999999));
            GUI.enabled = true;


            // mesh settings
            EditorGUILayout.Space();
            GUILayout.Label("Mesh Output settings", EditorStyles.boldLabel);
            EditorGUILayout.Space();
            MaxVertexCountPerMesh = Mathf.Clamp(EditorGUILayout.IntField(new GUIContent("Vertices per mesh", null, "How many verts per mesh (max 65k). !Warning: Low values will create millions of files!"), MaxVertexCountPerMesh), 1, 65000);
            meshMaterial = (Material)EditorGUILayout.ObjectField(new GUIContent("Mesh material", null, "Material & Shader for the meshes"), meshMaterial, typeof(Material), true);

            // TODO:
            //addToScene = EditorGUILayout.ToggleLeft(new GUIContent("Add meshes to current scene", null, ""), addToScene);

            EditorGUILayout.Space();
            GUI.enabled = sourceFile == null ? false : true; // disabled if no source selected
            if (GUILayout.Button(new GUIContent("Convert to Meshes", "Convert source to meshes"), GUILayout.Height(40)))
            {
                pcCounter = 0;
                savePath = null;

                // check if need to process whole folder
                string fileToRead = AssetDatabase.GetAssetPath(sourceFile);
                if (Directory.Exists(fileToRead))
                {
                    // get all suitable files in this folder
                    string[] filters = new[] { "*.xyz", "*.asc", "*.las", "*.pts", "*.cgo", "*.ply", "*.xyzrgb", ".pcd" };
                    string[] filePaths = filters.SelectMany(f => Directory.GetFiles(fileToRead, f)).ToArray();

                    // convert files one by one
                    for (int i = 0; i < filePaths.Length; i++)
                    {
                        folder = new GameObject();
                        pcCounter++;
                        folder.name = "PointClouds" + pcCounter;
                        Convert2Mesh(filePaths[i]);
                    }

                } else
                { // single point cloud
                    var stopwatch = new System.Diagnostics.Stopwatch();
                    stopwatch.Start();
                    folder = new GameObject("PointClouds-" + sourceFile.name);
                    Convert2Mesh(fileToRead);
                    stopwatch.Stop();
                    Debug.Log("Timer: " + stopwatch.ElapsedMilliseconds + "ms");
                    stopwatch.Reset();
                }

            }
            GUI.enabled = true;
        }


        void Convert2Mesh(string fileToRead)
        {
            cloudList.Clear();
            meshCounter = 1;

            if (savePath == null)
            {
                savePath = EditorUtility.SaveFilePanelInProject("Mesh assets output folder & basename", "PointChunk", "asset", "Set base filename");
            }


            // check path
            if (savePath.Length == 0)
            {
                Debug.LogWarning(appName + "> Cancelled..");
                return;
            }

            if (fileToRead.Length != 0)
            {
                //Debug.Log ("readfile: "+path);
            } else
            {
                Debug.LogWarning(appName + "> Cannot find file (" + fileToRead + ")");
                return;
            }

            if (File.Exists(fileToRead) == false)
            {
                Debug.LogWarning(appName + "> Cannot find file (" + fileToRead + ")");
                return;
            }

            EditorUtility.DisplayProgressBar(appName, "Checking file..", 0.25f);

            string fileExtension = Path.GetExtension(fileToRead).ToLower();

            if (fileExtension != ".ply")
            {
                if (readNormals == true)
                {
                    Debug.LogWarning(appName + "> Importing normals is only supported for .PLY files");
                    readNormals = false;
                }
            }

            // TEMPORARY: Custom reader for Brekel binary data
            if (fileExtension == ".bin")
            {
                Debug.Log(fileExtension);
                EditorUtility.ClearProgressBar();
                BrekelDataConvert(fileToRead);
                return;
            }

            // TEMPORARY: Custom reader for LAS binary data
            if (fileExtension == ".las")
            {
                EditorUtility.ClearProgressBar();
                LASConverter(fileToRead);
                return;
            }

            masterPointCount = 0;
            long lines = 0;
            int dataCount = 0;

            Vector3[] vertexArray;
            Color[] colorArray = null;
            int[] indexArray;
            Vector3[] normalArray = null;

            double minCornerX = Mathf.Infinity;
            double minCornerY = Mathf.Infinity;
            double minCornerZ = Mathf.Infinity;
            double maxCornerX = Mathf.NegativeInfinity;
            double maxCornerY = Mathf.NegativeInfinity;
            double maxCornerZ = Mathf.NegativeInfinity;

            // reading whole file
            using (StreamReader reader = new StreamReader(File.OpenRead(fileToRead)))
            {
                double x = 0, y = 0, z = 0, nx = 0, ny = 0, nz = 0;
                float r = 0, g = 0, b = 0;
                int indexR = 3, indexG = 4, indexB = 5;
                int indexNX = 3, indexNY = 4, indexNZ = 5;
                int indexI = 3;

                string rawLine = null;
                string origRawLine = null;
                string[] lineSplitted = null;
                int commentsLength = 0;
                int commentLines = 0;

                // formats
                bool replaceCommas = false; // for cgo, catia asc (depends on pc regional settings)

                // find first line of point data
                bool comments = true;
                bool headerData = true;
                bool hasNormals = false;
                bool plyHeaderFinished = false;
                bool plyNormalsBeforeRGB = false; // default from MeshLab XYZ NNN RGB

                // parse header
                while (comments == true && reader.EndOfStream == false)
                {
                    origRawLine = reader.ReadLine();
                    // early exit if certain file type
                    if (fileExtension == ".ply" && headerData == true)
                    {
                        // RGB data before normals
                        if (hasNormals == true && (origRawLine.ToLower().Contains("property float red") || origRawLine.ToLower().Contains("property uchar red")))
                        {
                            plyNormalsBeforeRGB = true;
                        }

                        if (origRawLine.ToLower().Contains("property float nx"))
                        {
                            hasNormals = true;
                        }

                        if (origRawLine.ToLower().Contains("format binary"))
                        {
                            Debug.LogError("Only .PLY ASCII format is supported, your file is PLY Binary");
                            EditorUtility.ClearProgressBar();
                            headerData = false;
                            return;
                        }

                        if (origRawLine.ToLower().Contains("element vertex"))
                        {
                            // get point count
                            int tempParse = 0;
                            if (int.TryParse(origRawLine.Split(' ')[2], out tempParse))
                            {
                                masterPointCount = tempParse;
                            } else
                            {
                                Debug.LogError("PLY Header parsing failed, point count not founded");
                                EditorUtility.ClearProgressBar();
                                return;
                            }
                        }

                        if (origRawLine.ToLower().Contains("end_header"))
                        {
                            headerData = false;
                            plyHeaderFinished = true;
                        }

                    } else if ((fileExtension == ".pts" || fileExtension == ".cgo") && masterPointCount == 0)
                    {
                        //Debug.Log(origRawLine);
                        // get point count
                        int tempParse = 0;
                        rawLine = Regex.Replace(origRawLine, "[^.0-9 ]+[^e\\-\\d]", "").Trim(); // cleanup non numeric

                        if (int.TryParse(rawLine, out tempParse))
                        {
                            masterPointCount = tempParse;
                            commentLines = 1;
                            headerData = false;
                        } else
                        {
                            Debug.LogError(fileExtension.ToUpper() + " pts/cgo header parsing failed, point count not founded");
                            EditorUtility.ClearProgressBar();
                            return;

                        }
                    } else if (fileExtension == ".pcd" && headerData == true)
                    {
                        if (origRawLine.Contains("POINTS "))
                        {
                            rawLine = origRawLine.Replace("POINTS ", "").Trim();
                            long.TryParse(rawLine, out masterPointCount);
                        }

                        if (origRawLine.ToLower().Contains("data binary"))
                        {
                            Debug.LogError("Only .pcd ASCII format is supported, your file is PCD Binary");
                            EditorUtility.ClearProgressBar();
                            headerData = false;
                            return;
                        }

                        if (origRawLine.ToLower().Contains("data ascii"))
                        {
                            headerData = false;
                        }

                    } else // other formats
                    {
                        // no custom data
                        headerData = false;
                    }


                    //					rawLine = origRawLine.Replace(",","."); // for cgo/catia asc
                    rawLine = Regex.Replace(origRawLine, "[^.0-9 ]+[^e\\-\\d]", ""); // cleanup non numeric
                    rawLine = rawLine.Replace("   ", " ").Replace("  ", " ").Trim();

                    lineSplitted = rawLine.Split(' ');

                    // its still comments or invalid data
                    //Debug.Log(lineSplitted.Length + " , " + ValidateColumns(lineSplitted.Length) + " : " + origRawLine);
                    //Debug.Log("splitlen:" + CheckIfDataIsComments(lineSplitted.Length) + " " + lineSplitted.Length + " hea:" + headerData);
                    //                    Debug.Break();
                    if (headerData == true || rawLine.StartsWith("#") || rawLine.StartsWith("!") || rawLine.StartsWith("*") || rawLine.ToLower().Contains("comment") || CheckIfDataIsComments(lineSplitted.Length) == false)
                    {
                        commentsLength += origRawLine.Length + 1; // +1 is end of line?
                        commentLines++;
                    } else // actual data, get first row
                    {
                        if (replaceCommas == false && rawLine.Contains(",")) replaceCommas = true;
                        if (replaceCommas == true) rawLine = rawLine.Replace(",", "."); // for cgo/catia asc

                        lineSplitted = rawLine.Split(' ');

                        if (readRGB && lineSplitted.Length < 6) { Debug.LogError("No RGB data founded after XYZ, disabling readRGB"); readRGB = false; }

                        if (readIntensity == true && (lineSplitted.Length != 4 && lineSplitted.Length != 7)) { Debug.LogError("No Intensity data founded after XYZ, disabling readIntensity"); readIntensity = false; }

                        if (readNormals == true)
                        {
                            //if (lineSplitted.Length!=6 && lineSplitted.Length != 7 && lineSplitted.Length != 9 && lineSplitted.Length != 10) {
                            if (hasNormals == false)
                            {
                                Debug.LogError("No normals data founded, disabling readNormals. [" + lineSplitted.Length + " values founded]");
                                readNormals = false;

                            } else
                            { // we have normals
                                if (plyNormalsBeforeRGB == true)
                                {
                                    indexR += 3;
                                    indexG += 3;
                                    indexB += 3;
                                } else // RGB before normals
                                {

                                }
                            }
                        } else // dont read normals
                        { // check if normals are there, but we dont use them
                            //Debug.Log("hasnormals" + hasNormals);
                            if (hasNormals == true)
                            {
                                if (plyNormalsBeforeRGB == true)
                                {
                                    indexR += 3;
                                    indexG += 3;
                                    indexB += 3;
                                } else // RGB before normals
                                {

                                }
                            }

                            // do we have intensity  for .pts? then skip intensity
                            if (fileExtension == ".pts" && readIntensity == false && lineSplitted.Length == 7)
                            {
                                indexR += 1;
                                indexG += 1;
                                indexB += 1;
                            }

                        }

                        dataCount = lineSplitted.Length;
                        comments = false;
                        lines++;
                    }
                } // while (parsing header)

                bool skipRow = false;
                int skippedRows = 0;

                // get first actual data row
                if (!double.TryParse(lineSplitted[0], out x)) skipRow = true;
                if (!double.TryParse(lineSplitted[1], out y)) skipRow = true;
                if (!double.TryParse(lineSplitted[2], out z)) skipRow = true;


                if (skipRow == true)
                {
                    skippedRows++;
                    Debug.LogWarning("First point data row was skipped, conversion will most likely fail (rawline:" + rawLine + ")");
                }


                if (enableManualOffset == true || autoOffsetNearZero == true)
                {
                    manualOffset = flipYZ ? new Vector3((float)x, (float)z, (float)y) : new Vector3((float)x, (float)y, (float)z);
                }

                // scaling enabled, scale offset too
                if (useUnitScale == true) manualOffset *= unitScale;

                // jump back to start of first line
                EditorUtility.ClearProgressBar();


                // use header count value from ply, cgo, pts..
                if (fileExtension == ".ply" || fileExtension == ".pts" || fileExtension == ".cgo" || fileExtension == ".pcd")
                {
                    lines = masterPointCount;
                } else // other formats
                {
                    // calculate rest of data lines
                    while (reader.EndOfStream == false)
                    {
                        origRawLine = reader.ReadLine();
                        if (replaceCommas == true) rawLine = origRawLine.Replace(",", ".");
                        rawLine = Regex.Replace(rawLine, "[^.0-9 ]+[^e\\-\\d]", ""); // cleanup non numeric
                        rawLine = rawLine.Replace("   ", " ").Replace("  ", " ").Trim();

                        if (lines % 256000 == 1)
                        {
                            if (EditorUtility.DisplayCancelableProgressBar(appName, "Counting lines..", lines / 20000000.0f))
                            {
                                Debug.Log("Cancelled at: " + lines);
                                EditorUtility.ClearProgressBar();
                                return;
                            }
                        }

                        // check if data is valid
                        if (!rawLine.StartsWith("!"))
                        {
                            if (!rawLine.StartsWith("*"))
                            {
                                if (!rawLine.StartsWith("#"))
                                {
                                    lineSplitted = rawLine.Split(' ');

                                    if (lineSplitted.Length == dataCount)
                                    {
                                        lines++;
                                    }
                                }
                            }
                        }
                    } // count points

                    EditorUtility.ClearProgressBar();
                }

                // reset back to start
                reader.DiscardBufferedData();
                reader.BaseStream.Seek(0, SeekOrigin.Begin);
                reader.BaseStream.Position = 0;

                if (commentLines > 0)
                {
                    for (int i = 0; i < commentLines; i++)
                    {
                        var tmp = reader.ReadLine();
                    }
                }

                masterPointCount = lines;

                vertexArray = new Vector3[masterPointCount];
                indexArray = new int[masterPointCount];

                if (readRGB == true || readIntensity == true) colorArray = new Color[masterPointCount];
                if (readNormals == true) normalArray = new Vector3[masterPointCount];

                long rowCount = 0;
                bool readMore = true;
                double tempVal = 0;

                //read all point cloud data here
                for (rowCount = 0; rowCount < masterPointCount - 1; rowCount++)
                {
                    if (rowCount % 256000 == 1)
                    {
                        EditorUtility.DisplayProgressBar(appName, "Processing points..", rowCount / (float)lines);
                        //progressCounter=0;
                    }

                    // process each line
                    rawLine = reader.ReadLine().Trim();

                    // trim duplicate spaces
                    rawLine = rawLine.Replace(",", "."); // for cgo/catia asc

                    rawLine = Regex.Replace(rawLine, "[^.0-9 ]+[^e\\-\\d]", "").Trim(); // cleanup non numeric
                    rawLine = rawLine.Replace("   ", " ").Replace("  ", " ").Trim();
                    lineSplitted = rawLine.Split(' ');

                    // have same amount of columns in data?
                    if (lineSplitted.Length == dataCount)
                    {
                        if (!double.TryParse(lineSplitted[0], out x)) skipRow = true;
                        if (!double.TryParse(lineSplitted[1], out y)) skipRow = true;
                        if (!double.TryParse(lineSplitted[2], out z)) skipRow = true;

                        if (readRGB == true)
                        {
                            r = System.Convert.ToInt32(lineSplitted[indexR]);
                            g = System.Convert.ToInt32(lineSplitted[indexG]);
                            b = System.Convert.ToInt32(lineSplitted[indexB]);

                            r /= 255f;
                            g /= 255f;
                            b /= 255f;
                        }

                        if (readIntensity == true)
                        {
                            // TODO: handle different intensity values
                            if (!float.TryParse(lineSplitted[indexI], out r)) skipRow = true;

                            // re-range PTS intensity
                            if (fileExtension == ".pts")
                            {
                                r = Remap(r, -2048, 2047, 0, 1);
                            }

                            // for now, set green and blue to intensity color also
                            g = r;
                            b = r;
                        }

                        if (readNormals == true)
                        {
                            if (!double.TryParse(lineSplitted[indexNX], out nx)) skipRow = true;
                            if (!double.TryParse(lineSplitted[indexNY], out ny)) skipRow = true;
                            if (!double.TryParse(lineSplitted[indexNZ], out nz)) skipRow = true;
                        }

                        // if flip
                        if (flipYZ == true)
                        {
                            tempVal = z;
                            z = y;
                            y = tempVal;

                            // flip normals?
                            if (readNormals == true)
                            {
                                tempVal = nz;
                                nz = ny;
                                ny = tempVal;
                            }

                        }

                        // scaling enabled
                        if (useUnitScale == true)
                        {
                            x *= unitScale;
                            y *= unitScale;
                            z *= unitScale;
                        }

                        // manual offset enabled
                        if (autoOffsetNearZero == true || enableManualOffset == true)
                        {
                            x -= manualOffset.x;
                            y -= manualOffset.y;
                            z -= manualOffset.z;
                        }

                        // get cloud corners
                        if (skipRow == false)
                        {
                            minCornerX = System.Math.Min(x, minCornerX);
                            minCornerY = System.Math.Min(y, minCornerY);
                            minCornerZ = System.Math.Min(z, minCornerZ);

                            maxCornerX = System.Math.Max(x, maxCornerX);
                            maxCornerY = System.Math.Max(y, maxCornerY);
                            maxCornerZ = System.Math.Max(z, maxCornerZ);

                            vertexArray[rowCount].Set((float)x, (float)y, (float)z);
                            indexArray[rowCount] = ((int)rowCount) % MaxVertexCountPerMesh; // TODO: FIXME, some problem if sorted?
                        }

                        if (readRGB == true || readIntensity == true)
                        {
                            colorArray[rowCount].r = r;
                            colorArray[rowCount].g = g;
                            colorArray[rowCount].b = b;
                            colorArray[rowCount].a = 1f;
                        }

                        if (readNormals == true)
                        {
                            normalArray[rowCount].Set((float)nx, (float)ny, (float)nz);
                        }


                    } else
                    { // if row length
                        skipRow = true;
                    }
                    //					} // line len


                    if (skipRow == true)
                    {
                        skippedRows++;
                        skipRow = false;
                    } else
                    {
                        //						rowCount++;
                    }

                    //					if (reader.EndOfStream)// || rowCount>=masterPointCount)
                    //					{
                    //Debug.Log(reader.EndOfStream);
                    //Debug.Log(rowCount>=masterPointCount);
                    //readMore = false;
                    //						Debug.LogError("Reached end of file too early ("+rowCount+"/"+masterPointCount+")");
                    //						break;
                    //					}

                } // while reading file
                EditorUtility.ClearProgressBar();

                if (skippedRows > 0) Debug.LogWarning("Skipped " + skippedRows.ToString() + " rows (out of " + masterPointCount + " rows) because of parsing errors");

            } // using reader


            // sort points, FIXME: something strange happens inside sort, points go missing or lose precision?
            if (optimizePoints == true)
            {
                EditorUtility.DisplayProgressBar(appName, "Optimizing points..", 0.25f);

                //                System.Array.Sort(vertexArray);
                //System.Array.Sort(vertexArray, (a,b) => (a.x>b.x)?1:-1); // works

                /*
                Debug.Log(indexArray[0]);
                Debug.Log(indexArray[1]);
                Debug.Log(indexArray[2]);
                Debug.Log(indexArray[3]);

                Debug.Log(indexArray[indexArray.Length - 1]);
                Debug.Log(indexArray[indexArray.Length - 2]);
                Debug.Log(indexArray[indexArray.Length - 3]);
                Debug.Log(indexArray[indexArray.Length - 4]);*/


                ArrayQuickSort(ref vertexArray, ref colorArray, ref indexArray, 0, vertexArray.Length - 1); // works

                // sort by val
                //                System.Array.Sort(pointArray, (a,b) => (a.index==b.index?0:(a.index>b.index)?1:-1));
                //System.Array.Sort(pointArray, (a,b) => (a.index>b.index)?1:-1);
                //System.Array.Sort(pointArray, (a,b) => (a.index).CompareTo(b.index));

                //System.Array.Sort(pointArray, (a,b) => (a.vertex.x>= b.vertex.x)?1:-1);

                // distance sort
                //System.Array.Sort<PointData>(pointArray, (a,b) => (minCorner-a.vertex).sqrMagnitude.CompareTo((minCorner-b.vertex).sqrMagnitude));

                // TODO: sorting loses precision??
                // sort by x
                //Debug.Log("Optimize Points function is currently disabled");

                //System.Array.Sort(pointArray, (a,b) => (a.vertex.x).CompareTo(b.vertex.x));
                //System.Array.Sort(pointArray, (a,b) => ((int)a.vertex.x).CompareTo((int)b.vertex.x));
                EditorUtility.ClearProgressBar();

                // TODO: split by slicing grid
            }

            // build mesh assets
            int indexCount = 0;

            Vector3[] vertices2 = new Vector3[MaxVertexCountPerMesh];
            Vector2[] uvs2 = new Vector2[MaxVertexCountPerMesh];
            int[] triangles2 = new int[MaxVertexCountPerMesh];
            Color[] colors2 = new Color[MaxVertexCountPerMesh];
            Vector3[] normals2 = new Vector3[MaxVertexCountPerMesh];

            EditorUtility.DisplayProgressBar(appName, "Creating " + ((int)(vertexArray.Length / MaxVertexCountPerMesh)) + " mesh arrays", 0.75f);

            // process all point data into meshes
            for (int i = 0, totalLen = vertexArray.Length; i < totalLen; i++)
            //for (int i = 0, totalLen = pointArray.Length; i < totalLen; i++)
            {

                vertices2[indexCount] = vertexArray[i];
                uvs2[indexCount].Set(vertexArray[i].x, vertexArray[i].y);
                triangles2[indexCount] = indexArray[i];
                if (readRGB || readIntensity) colors2[indexCount] = colorArray[i];
                if (readNormals) normals2[indexCount] = normalArray[i];

                /*
                vertices2[indexCount] = pointArray[i].vertex;
                //uvs2[indexCount] = pointArray[i].uv;
                //uvs2[indexCount].Set(pointArray[i].vertex.x, pointArray[i].vertex.y);
                triangles2[indexCount] = pointArray[i].index;
                if (readRGB || readIntensity) colors2[indexCount] = pointArray[i].color;
                if (readNormals) normals2[indexCount] = pointArray[i].normal;*/

                if (decimatePoints == true)
                {
                    if (i % removeEveryNth == 0) indexCount++;
                } else
                {
                    indexCount++;
                }

                //if (indexCount>=vertCount || i==pointArray.Length-1)
                if (indexCount >= MaxVertexCountPerMesh || i == vertexArray.Length - 1)
                {
                    var go = BuildMesh(vertices2, triangles2, colors2, normals2);
                    if (addMeshesToScene && go != null) if (createLODS) BuildLODS(go, vertices2, triangles2, colors2, normals2);

                    indexCount = 0;

                    // need to clear arrays, should use lists otherwise last mesh has too many verts (or slice last array)
                    System.Array.Clear(vertices2, 0, MaxVertexCountPerMesh);
                    System.Array.Clear(uvs2, 0, MaxVertexCountPerMesh);
                    System.Array.Clear(triangles2, 0, MaxVertexCountPerMesh);

                    if (readRGB || readIntensity) System.Array.Clear(colors2, 0, MaxVertexCountPerMesh);
                    if (readNormals) System.Array.Clear(normals2, 0, MaxVertexCountPerMesh);
                }
            }

            EditorUtility.ClearProgressBar();

            // save meshes

            EditorUtility.DisplayProgressBar(appName, "Saving " + (cloudList.Count) + " mesh assets", 0.95f);


            string pad = "";
            for (int i = 0; i < cloudList.Count; i++)
            {
                if (i < 1000) pad = "0";
                if (i < 100) pad = "00";
                if (i < 10) pad = "000";
                savePath = savePath.Replace(".asset", "");
                savePath = savePath.Replace(".ASSET", "");
                savePath = savePath.Replace(".Asset", "");
                AssetDatabase.CreateAsset(cloudList[i], savePath + "_" + pcCounter + "_" + pad + i + ".asset");
                AssetDatabase.SaveAssets(); // not needed?
            } // save meshes

            EditorUtility.ClearProgressBar();

            AssetDatabase.Refresh();

            Debug.Log("Total amount of points: " + masterPointCount);

        } // convert2meshOptimized2


        /// <summary>
        /// returns true if good array length
        /// </summary>
        bool CheckIfDataIsComments(int len)
        {
            //  XYZ         XYZI        XYZRGB      XYZIRGB     XYZNNNRGB   XYZNNNRGBA?
            return (len == 3 || len == 4 || len == 6 || len == 7 || len == 9 || len == 10);
        }




        void BrekelDataConvert(string fileToRead)
        {
            var reader = new BinaryReader(File.OpenRead(fileToRead));

            byte binaryVersion = reader.ReadByte();
            int numberOfFrames = reader.ReadInt32();
            //float frameRate=reader.ReadSingle(); // NOT YET USED
            reader.ReadSingle(); // skip framerate field
            bool containsRGB = reader.ReadBoolean();

            // TODO: if its 1, read our own binary
            if (binaryVersion != 2) Debug.LogWarning("BinaryVersion is not 2 - reading file most likely fails..");

            /*
			Debug.Log("binaryVersion:"+binaryVersion);
			Debug.Log("numberOfFrames:"+numberOfFrames);
			Debug.Log("frameRate:"+frameRate);
			Debug.Log("containsRGB:"+containsRGB);
			*/

            int pointCounter = 0; // array index
            Vector3[] vertices = new Vector3[MaxVertexCountPerMesh];
            Vector2[] uvs = new Vector2[MaxVertexCountPerMesh];
            int[] triangles = new int[MaxVertexCountPerMesh];
            Color[] colors = new Color[MaxVertexCountPerMesh];
            Vector3[] normals = new Vector3[MaxVertexCountPerMesh];

            int[] numberOfPointsPerFrame;
            numberOfPointsPerFrame = new int[numberOfFrames];

            for (int i = 0; i < numberOfFrames; i++)
            {
                numberOfPointsPerFrame[i] = reader.ReadInt32();//(int)System.BitConverter.ToInt32(data,byteIndex);
            }

            // Binary positions for each frame, not used
            for (int i = 0; i < numberOfFrames; i++)
            {
                reader.ReadInt64();
            }


            float x, y, z, r, g, b;

            for (int frame = 0; frame < numberOfFrames; frame++)
            {
                for (int i = 0; i < numberOfPointsPerFrame[frame]; i++)
                {
                    x = reader.ReadSingle();
                    y = reader.ReadSingle();
                    z = reader.ReadSingle();

                    if (containsRGB == true)
                    {
                        r = reader.ReadSingle();
                        g = reader.ReadSingle();
                        b = reader.ReadSingle();
                        colors[pointCounter] = new Color(r, g, b, 1f);
                    }

                    // scaling enabled
                    if (useUnitScale == true)
                    {
                        x *= unitScale;
                        y *= unitScale;
                        z *= unitScale;
                    }

                    // manual offset enabled
                    if (enableManualOffset == true)
                    {
                        x -= manualOffset.x;
                        y -= manualOffset.x;
                        z -= manualOffset.x;
                    }



                    // if flip
                    if (flipYZ == true)
                    {
                        vertices[pointCounter] = new Vector3(x, z, y);
                    } else
                    { // noflip
                        vertices[pointCounter] = new Vector3(x, y, z);
                    }

                    //uvs[pointCounter] = new Vector2(x, y);
                    triangles[pointCounter] = pointCounter;

                    pointCounter++;

                    // do we have enough for this mesh?
                    if (pointCounter >= MaxVertexCountPerMesh || i == numberOfPointsPerFrame[frame] - 1)
                    {
                        BuildMesh(vertices, triangles, colors, normals);
                        pointCounter = 0;
                    }
                } // points on each frame

            } // frames


            // save meshes
            string pad = "";
            for (int i = 0; i < cloudList.Count; i++)
            {
                if (cloudList.Count > 99)
                {
                    if (i < 100) pad = "0";
                    if (i < 10) pad = "00";
                } else
                {
                    if (i < 10) pad = "0";
                }
                savePath = savePath.Replace(".asset", "");
                savePath = savePath.Replace(".ASSET", "");
                savePath = savePath.Replace(".Asset", "");
                var newAssetPath = savePath + "_" + pcCounter + "_" + pad + i + ".asset";
                AssetDatabase.CreateAsset(cloudList[i], newAssetPath);
                // TODO: create gameobject, use the mesh, and assign material to the gameobject..or use the scene object
            } // save meshes

            AssetDatabase.Refresh();
            reader.Close();
        }


        // converting las files separately
        void LASConverter(string fileToRead)
        {
            Vector3[] vertexArray = null;
            Color[] colorArray = null;
            int[] indexArray = null;
            Vector3[] normalArray = null;

            Vector3 minCorner = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
            Vector3 maxCorner = new Vector3(Mathf.NegativeInfinity, Mathf.NegativeInfinity, Mathf.NegativeInfinity);

            double x = 0, y = 0, z = 0;
            int r = 0, g = 0, b = 0;

            // open file
            BinaryReader reader = new BinaryReader(File.OpenRead(fileToRead));

            string fileSignature = new string(reader.ReadChars(4));

            if (fileSignature != "LASF") Debug.LogError("LAS> FileSignature error: '" + fileSignature + "'");

            // NOTE: Currently most of this info is not used
            ushort fileSourceID = reader.ReadUInt16();
            ushort globalEncoding = reader.ReadUInt16();

            ulong projectID1 = reader.ReadUInt32(); // optional?
            ushort projectID2 = reader.ReadUInt16(); // optional?
            ushort projectID3 = reader.ReadUInt16(); // optional?
            string projectID4 = new string(reader.ReadChars(8)); // optional?

            byte versionMajor = reader.ReadByte();
            byte versionMinor = reader.ReadByte();

            string systemIdentifier = new string(reader.ReadChars(32));
            string generatingSoftware = new string(reader.ReadChars(32));

            ushort fileCreationDayOfYear = reader.ReadUInt16();
            ushort fileCreationYear = reader.ReadUInt16();
            ushort headerSize = reader.ReadUInt16();

            ulong offsetToPointData = reader.ReadUInt32();

            ulong numberOfVariableLengthRecords = reader.ReadUInt32();

            byte pointDataRecordFormat = reader.ReadByte();

            ushort PointDataRecordLength = reader.ReadUInt16();

            ulong legacyNumberOfPointRecords = reader.ReadUInt32();

            ulong[] legacyNumberOfPointsByReturn = new ulong[] { reader.ReadUInt32(), reader.ReadUInt32(), reader.ReadUInt32(), reader.ReadUInt32(), reader.ReadUInt32() };

            double xScaleFactor = reader.ReadDouble();
            double yScaleFactor = reader.ReadDouble();
            double zScaleFactor = reader.ReadDouble();

            double xOffset = reader.ReadDouble();
            double yOffset = reader.ReadDouble();
            double zOffset = reader.ReadDouble();
            double maxX = reader.ReadDouble();
            double minX = reader.ReadDouble();
            double MaxY = reader.ReadDouble();
            double minY = reader.ReadDouble();
            double maxZ = reader.ReadDouble();
            double minZ = reader.ReadDouble();

            // Only for 1.4
            if (versionMajor == 1 && versionMinor == 4)
            {
                ulong startOfFirstExtentedVariableLengthRecord = reader.ReadUInt64();
                ulong numberOfExtentedVariableLengthRecords = reader.ReadUInt32();

                ulong numberOfPointRecords = reader.ReadUInt64();
                ulong[] numberOfPointsByReturn = new ulong[] {reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),
                    reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),
                    reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64()};
            }

            //ulong numberOfPointRecords = reader.ReadUInt64();
            // VariableLengthRecords
            if (numberOfVariableLengthRecords > 0)
            {
                ushort vlrReserved = reader.ReadUInt16();
                string vlrUserID = new string(reader.ReadChars(16));
                ushort vlrRecordID = reader.ReadUInt16();
                ushort vlrRecordLengthAfterHeader = reader.ReadUInt16();
                string vlrDescription = new string(reader.ReadChars(32));
                /*
				Debug.Log("vlrReserved:"+vlrReserved);
				Debug.Log("vlrUserID:"+vlrUserID);
				Debug.Log("vlrRecordID:"+vlrRecordID);
				Debug.Log("vlrRecordLengthAfterHeader:"+vlrRecordLengthAfterHeader);
				Debug.Log("vlrDescription:"+vlrDescription);*/
            }

            // jump to points start pos
            reader.BaseStream.Seek((long)offsetToPointData, SeekOrigin.Begin);

            // format #2
            if (pointDataRecordFormat != 2 && pointDataRecordFormat != 3) Debug.LogWarning("LAS Import might fail - only pointDataRecordFormat #2 & #3 are supported (Your file is " + pointDataRecordFormat + ")");
            if (versionMinor != 2) Debug.LogWarning("LAS Import might fail - only version LAS 1.2 is supported. (Your file is " + versionMajor + "." + versionMinor + ")");

            masterPointCount = (int)legacyNumberOfPointRecords;

            // scaling enabled, scale manual offset
            if (useUnitScale) manualOffset *= unitScale;

            // progressbar
            float progress = 0;
            long progressCounter = 0;
            EditorUtility.ClearProgressBar();

            int rowCount = 0;
            bool haveMoreToRead = true;
            bool firstPointRead = false;

            //int pointCounter = 0; // array index
            //Vector3[] vertices = new Vector3[MaxVertexCountPerMesh];
            //Vector2[] uvs = new Vector2[vertCount];
            //int[] triangles = new int[MaxVertexCountPerMesh];
            //Color[] colors = new Color[MaxVertexCountPerMesh];
            //Vector3[] normals = new Vector3[MaxVertexCountPerMesh];

            Debug.Log("Reading " + masterPointCount + " points..");

            try
            {
                vertexArray = new Vector3[masterPointCount];
                indexArray = new int[masterPointCount];
                if (readRGB == true) colorArray = new Color[masterPointCount];
                if (readNormals == true) normalArray = new Vector3[masterPointCount];
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
                Debug.LogError("Cannot create point array - probably your file has too much data..(points: " + masterPointCount + ")");
                return;
            }

            // process all points
            while (haveMoreToRead == true)
            {
                if (progressCounter > 256000)
                {
                    if (EditorUtility.DisplayCancelableProgressBar(appName, "Counting lines..", progress / masterPointCount))
                    {
                        Debug.Log("Cancelled at: " + progress);
                        EditorUtility.ClearProgressBar();
                        return;
                    }
                    progressCounter = 0;
                }

                progressCounter++;
                progress++;

                long intX = reader.ReadInt32();
                long intY = reader.ReadInt32();
                long intZ = reader.ReadInt32();

                reader.ReadBytes(8); // unknown

                if (pointDataRecordFormat == 3) reader.ReadBytes(8); // GPS Time for format#3

                var colorR = reader.ReadBytes(2); // RED
                var colorG = reader.ReadBytes(2); // GREEN
                var colorB = reader.ReadBytes(2); // BLUE


                x = intX * xScaleFactor + xOffset;
                y = intY * yScaleFactor + yOffset;
                z = intZ * zScaleFactor + zOffset;

                // manual scaling enabled
                if (useUnitScale == true)
                {
                    x *= unitScale;
                    y *= unitScale;
                    z *= unitScale;
                }

                if (flipYZ == true)
                {
                    double yy = y;
                    y = z;
                    z = yy;
                }

                if (autoOffsetNearZero == true)
                {
                    if (!firstPointRead == true)
                    {
                        manualOffset = new Vector3((float)x, (float)y, (float)z);
                        firstPointRead = true;
                    }

                    x -= manualOffset.x;
                    y -= manualOffset.y;
                    z -= manualOffset.z;


                } else
                { // only 1 can be used autooffset or manual

                    if (enableManualOffset == true)
                    {
                        x -= manualOffset.x;
                        y -= manualOffset.y;
                        z -= manualOffset.z;
                    }
                }

                //vertices[pointCounter] = new Vector3((float)x, (float)y, (float)z);

                if (readRGB == true)
                {
                    r = System.BitConverter.ToUInt16(colorR, 0);
                    g = System.BitConverter.ToUInt16(colorG, 0);
                    b = System.BitConverter.ToUInt16(colorB, 0);

                    float rr = (float)r / 255f;
                    float gg = (float)g / 255f;
                    float bb = (float)b / 255f;

                    if (rr > 1f) rr /= 255f;
                    if (gg > 1f) gg /= 255f;
                    if (bb > 1f) bb /= 255f;

                    colorArray[rowCount] = new Color(rr, gg, bb, 1);

                    //if (rowCount < 200) Debug.Log("row:" + (rowCount + 1) + " xyz:" + x + "," + y + "," + z + " : " + rr + "," + gg + "," + bb + " pointcounter: " + rowCount);
                }

                // get cloud corners
                minCorner.x = Mathf.Min((float)x, minCorner.x);
                minCorner.y = Mathf.Min((float)y, minCorner.y);
                minCorner.z = Mathf.Min((float)z, minCorner.z);

                maxCorner.x = Mathf.Max((float)x, maxCorner.x);
                maxCorner.y = Mathf.Max((float)y, maxCorner.y);
                maxCorner.z = Mathf.Max((float)z, maxCorner.z);

                vertexArray[rowCount].Set((float)x, (float)y, (float)z);
                indexArray[rowCount] = rowCount % MaxVertexCountPerMesh;

                rowCount++;

                if (reader.BaseStream.Position >= reader.BaseStream.Length || rowCount >= masterPointCount)
                {
                    haveMoreToRead = false;
                }
            } // while loop reading file

            reader.Close();

            // build mesh asset files, arrays per mesh
            int indexCount = 0;
            Vector3[] vertices2 = new Vector3[MaxVertexCountPerMesh];
            //Vector2[] uvs2 = new Vector2[vertCount];
            int[] triangles2 = new int[MaxVertexCountPerMesh];
            Color[] colors2 = new Color[MaxVertexCountPerMesh];
            Vector3[] normals2 = new Vector3[MaxVertexCountPerMesh];
            //			Debug.Log("Total points: "+pointArray.Length);

            EditorUtility.DisplayProgressBar(appName, "Creating " + ((vertexArray.Length / MaxVertexCountPerMesh)) + " mesh arrays", 0.5f);

            // populate mesh vertex arrays, TODO: could use Array Copy, unless want to decimate
            for (int i = 0; i < vertexArray.Length; i++)
            {
                vertices2[indexCount] = vertexArray[i];
                triangles2[indexCount] = indexArray[i];
                if (readRGB == true) colors2[indexCount] = colorArray[i];

                if (decimatePoints == true)
                {
                    if (i % removeEveryNth == 0) indexCount++;
                } else
                {
                    indexCount++;
                }

                // reach max vertcount
                if (indexCount >= MaxVertexCountPerMesh || i == vertexArray.Length - 1)
                {
                    var go = BuildMesh(vertices2, triangles2, colors2, normals2);

                    if (addMeshesToScene == true && createLODS == true && go != null) BuildLODS(go, vertices2, triangles2, colors2, normals2);

                    // clear old data
                    System.Array.Clear(vertices2, 0, MaxVertexCountPerMesh);
                    System.Array.Clear(triangles2, 0, MaxVertexCountPerMesh);
                    if (readRGB == true || readIntensity == true) System.Array.Clear(colors2, 0, MaxVertexCountPerMesh);
                    if (readNormals == true) System.Array.Clear(normals2, 0, MaxVertexCountPerMesh);

                    indexCount = 0;
                }
            }

            EditorUtility.ClearProgressBar();

            // save meshes

            EditorUtility.DisplayProgressBar(appName, "Saving " + (cloudList.Count) + " mesh assets", 0.75f);


            string pad = "";
            for (int i = 0; i < cloudList.Count; i++)
            {
                if (i < 10000) pad = "0";
                if (i < 1000) pad = "00";
                if (i < 100) pad = "000";
                if (i < 10) pad = "0000";

                savePath = savePath.Replace(".asset", "");
                savePath = savePath.Replace(".ASSET", "");
                savePath = savePath.Replace(".Asset", "");

                AssetDatabase.CreateAsset(cloudList[i], savePath + "_" + pcCounter + "_" + pad + i + ".asset");
                AssetDatabase.SaveAssets(); // not needed?
            } // save meshes

            EditorUtility.ClearProgressBar();

            AssetDatabase.Refresh();


            EditorUtility.ClearProgressBar();
        }


        // helper functions
        GameObject BuildMesh(Vector3[] verts, int[] tris, Color[] colors, Vector3[] normals)
        {
            GameObject target = new GameObject();

            var mf = target.AddComponent<MeshFilter>();
            var mr = target.AddComponent<MeshRenderer>();

            Mesh mesh = new Mesh();
            target.isStatic = true;
            mf.mesh = mesh;
            target.transform.name = "PC_" + meshCounter;
            mr.sharedMaterial = meshMaterial;
            mr.receiveShadows = false;
            mr.shadowCastingMode = ShadowCastingMode.Off;
#if UNITY_5_6_OR_NEWER
            mr.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
#else
            mr.lightProbeUsage = LightProbeUsage.Off;
#endif
            mr.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;

            GameObject lodRoot = null;

            if (createLODS == true)
            {
                lodRoot = new GameObject();
                lodRoot.transform.parent = folder.transform;
                lodRoot.name = "lod_" + meshCounter;
                target.transform.parent = lodRoot.transform;
            } else // no lod
            {
                target.transform.parent = folder.transform;
            }

            mesh.vertices = verts;
            //mesh.uv = uvs;
            if (readRGB == true || readIntensity == true) mesh.colors = colors;
            if (readNormals == true) mesh.normals = normals;

            // TODO: use scanner centerpoint and calculate direction from that..not really accurate
            //if (forceRecalculateNormals) ...

            mesh.SetIndices(tris, MeshTopology.Points, 0);
            //mesh.RecalculateBounds();

            cloudList.Add(mesh);
            meshCounter++;

            // FIXME: temporary workaround to not add objects into scene..
            if (addMeshesToScene == false) DestroyImmediate(target);

            return target;
        }

        void BuildLODS(GameObject target, Vector3[] verts, int[] tris, Color[] colors, Vector3[] normals)
        {
            GameObject go;
            LODGroup group;

            group = target.transform.parent.gameObject.AddComponent<LODGroup>();
            LOD[] lods = new LOD[lodLevels];


            float lerpStep = 1 / (float)(lodLevels - 1);
            float lerpVal = 1;

            // make LODS
            for (int i = 0; i < lodLevels; i++)
            {

                if (i == 0) // main mesh
                {
                    go = target;

                } else
                { // make simplified meshes

                    go = new GameObject();
                    var mf = go.AddComponent<MeshFilter>();
                    var mr = go.AddComponent<MeshRenderer>();

                    Mesh mesh = new Mesh();
                    mf.mesh = mesh;
                    go.transform.name = "PC_" + meshCounter + "_" + i.ToString();
                    mr.sharedMaterial = meshMaterial;
                    mr.receiveShadows = false;
                    mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
#if UNITY_5_6_OR_NEWER
                    mr.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
#else
                    mr.lightProbeUsage = LightProbeUsage.Off;
#endif
                    mr.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;


                    lerpVal -= lerpStep;
                    int newVertCount = (int)Mathf.Lerp(minLodVertexCount, decimatePoints ? MaxVertexCountPerMesh / removeEveryNth : MaxVertexCountPerMesh, lerpVal);

                    var newVerts = new Vector3[newVertCount];
                    //var newUvs = new Vector2[newVertCount];
                    var newColors = new Color[newVertCount];
                    var newNormals = new Vector3[newVertCount];
                    var newTris = new int[newVertCount];

                    // get new verts
                    float oldIndex = 0;
                    float stepSize = MaxVertexCountPerMesh / (float)newVertCount;

                    // TODO: if rounds to same index, take next instead of same point?
                    float o = 0;

                    for (int newIndex = 0; newIndex < newVertCount; newIndex++)
                    {
                        newVerts[newIndex] = verts[Mathf.FloorToInt(o)];
                        newTris[newIndex] = newIndex; //tris[newIndex];

                        if (readRGB == true) newColors[newIndex] = colors[Mathf.FloorToInt(o)];

                        /*
						// for debugging LODS, different colors per lod
						switch(i)
						{
						case 1:
							newColors[newIndex] = Color.red;
							break;
						case 2:
							newColors[newIndex] = Color.green;
							break;
						case 3:
							newColors[newIndex] = Color.yellow;
							break;
						case 4:
							newColors[newIndex] = Color.cyan;
							break;
						default:
							newColors[newIndex] = Color.magenta;
							break;
						}*/

                        if (readNormals == true) newNormals[newIndex] = normals[Mathf.FloorToInt(o)];
                        o += stepSize;
                    }

                    mesh.vertices = newVerts;
                    //mesh.uv = newUvs;
                    if (readRGB == true || readIntensity == true) mesh.colors = newColors;
                    if (readNormals == true) mesh.normals = newNormals;
                    mesh.SetIndices(newTris, MeshTopology.Points, 0);
                    //mesh.RecalculateBounds();
                } // if main mesh


                go.transform.parent = target.transform.parent;
                Renderer[] renderers = new Renderer[1];
                renderers[0] = go.GetComponent<Renderer>();
                float LODVal = Mathf.Lerp(1f, 0.1f, (i + 1) / (float)lodLevels);
                lods[i] = new LOD(LODVal, renderers);
            }// for create lods

            group.SetLODs(lods);
            group.RecalculateBounds();
        } //BuildLODS



        string GetSelectedFileInfo()
        {
            string tempFilePath = AssetDatabase.GetAssetPath(sourceFile);

            if (Directory.Exists(tempFilePath))
            {
                return "[ Folder ]";
            } else
            {
                string tempFileName = Path.GetFileName(tempFilePath);
                return tempFileName + " (" + (new FileInfo(tempFilePath).Length / 1000000) + "MB)";
            }
        }

        bool ValidateSaveAndRead(string path, string fileToRead)
        {
            if (path.Length < 1) { Debug.Log(appName + "> Save cancelled.."); return false; }
            if (fileToRead.Length < 1) { Debug.LogError(appName + "> Cannot find file (" + fileToRead + ")"); return false; }
            if (!File.Exists(fileToRead)) { Debug.LogError(appName + "> Cannot find file (" + fileToRead + ")"); return false; }

            if (Path.GetExtension(fileToRead).ToLower() == ".bin") { Debug.LogError("Source file extension is .bin, binary file conversion is not supported"); return false; }

            return true;
        }

        float Remap(float source, float sourceFrom, float sourceTo, float targetFrom, float targetTo)
        {
            return targetFrom + (source - sourceFrom) * (targetTo - targetFrom) / (sourceTo - sourceFrom);
        }


        // http://anh.cs.luc.edu/170/notes/CSharpHtml/sorting.html
        public static void ArrayQuickSort(ref Vector3[] _vertexArray, ref Color[] _colorArray, ref int[] _indexArray, int l, int r)
        {
            int i, j;
            float x;

            i = l;
            j = r;

            x = _vertexArray[(l + r) / 2].x; /* find pivot item */

            while (true)
            {
                while (_vertexArray[i].x < x)
                    i++;
                while (x < _vertexArray[j].x)
                    j--;
                if (i <= j)
                {
                    var tempVector = _vertexArray[i];
                    _vertexArray[i] = _vertexArray[j];
                    _vertexArray[j] = tempVector;

                    /*
                    var tempInt = _indexArray[i]; // bugged!! its different size?
                    _indexArray[i] = _indexArray[j];
                    _indexArray[j] = tempInt;
                    */

                    // sort other arrays also, with same index
                    if (_colorArray != null)
                    {
                        var tempColor = _colorArray[i];
                        _colorArray[i] = _colorArray[j];
                        _colorArray[j] = tempColor;
                    }

                    // TODO: index, UV(not used), normal

                    i++;
                    j--;
                }
                if (i > j)
                    break;
            }
            if (l < j)
                ArrayQuickSort(ref _vertexArray, ref _colorArray, ref _indexArray, l, j);
            if (i < r)
                ArrayQuickSort(ref _vertexArray, ref _colorArray, ref _indexArray, i, r);
        }
    } // class
} // namespace

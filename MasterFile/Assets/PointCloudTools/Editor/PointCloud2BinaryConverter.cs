// Point Cloud to Binary Converter
// Saves pointcloud data into custom binary file, for faster viewing
// http://unitycoder.com

#pragma warning disable 0219 // disable unused var warnings

using UnityEditor;
using UnityEngine;
using System.Text.RegularExpressions;
using System.IO;
using unitycodercom_PointCloudHelpers;
using System.Text;

namespace unitycodercom_PointCloud2Binary
{

    public class PointCloud2BinaryConverter : EditorWindow
    {
        private static string appName = "PointCloud2Binary";
        private Object sourceFile;
        //                                             0      1         2      3      4            5              6      7      8
        private string[] fileFormats = new string[] { "XYZ", "XYZRGB", "CGO", "ASC", "CATIA ASC", "PLY (ASCII)", "LAS", "PTS", "PCD (ASCII)" };
        private string[] fileFormatInfo = new string[]{"sample: \"32.956900 5.632800 5.673400\"", // XYZ
			"sample: \"32.956900 5.632800 5.670000 128 190 232\"", // XYZRGB
			"sample: \"683,099976 880,200012 5544,700195\"", // CGO
			"sample: \" -1192.9 2643.6 5481.2\"", // ASC
			"sample: \"X 31022.1919 Y -3314.1098 Z 6152.5000\"", //  CATIA ASC
			"sample: \"-0.680891 -90.6809 0 204 204 204 255\"", // PLY (ASCII)
			"info: LAS 1.4", // LAS
			"info: 42.72464 -16.1426 -32.16625 -399 88 23 98", // PTS
            "info: 42.72464 -16.1426 -32.16625 -399 88 23 98" // PCD ASCII
		};
        private int fileFormat = 0;
        private bool readRGB = false;
        private bool readIntensity = false; // only for PTS currently
                                            //		private bool readNormals = false;
        private bool useUnitScale = true;
        private float unitScale = 0.001f;
        private bool flipYZ = true;
        private bool autoOffsetNearZero = true; // takes first point value as offset
        private bool enableManualOffset = false;
        private Vector3 manualOffset = Vector3.zero;
        private bool plyHasNormals = false;

        private long masterPointCount = 0;
        //		private bool compressed=false;

        private byte binaryVersion = 1;

        // create menu item and window
        [MenuItem("Window/PointCloudTools/Convert Point Cloud To Binary (DX11)", false, 1)]
        static void Init()
        {
            PointCloud2BinaryConverter window = (PointCloud2BinaryConverter)EditorWindow.GetWindow(typeof(PointCloud2BinaryConverter));
            window.titleContent = new GUIContent(appName);
            window.minSize = new Vector2(340, 380);
            window.maxSize = new Vector2(340, 384);
        }

        // main loop
        void OnGUI()
        {
            // source field
            GUILayout.Label("Point Cloud source file", EditorStyles.boldLabel);
            sourceFile = EditorGUILayout.ObjectField(sourceFile, typeof(Object), true);
            GUILayout.Label(sourceFile != null ? "file:" + GetSelectedFileInfo() : "", EditorStyles.miniLabel);
            EditorGUILayout.Space();

            // file format dropdown
            GUILayout.Label(new GUIContent("Input file format", "File extension can be anything, this selection decides the parsing method"));
            fileFormat = EditorGUILayout.Popup(fileFormat, fileFormats);
            GUILayout.Label(fileFormatInfo[fileFormat], EditorStyles.miniLabel);
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            // import RGB
            GUILayout.Label("Import settings", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal();
            if (fileFormat == 0) // XYZ format
            {
                GUI.enabled = false;
                readRGB = false;
                readRGB = EditorGUILayout.ToggleLeft(new GUIContent("Read RGB values", null, "Read R G B values"), readRGB);
                GUI.enabled = true;
            } else
            {
                readRGB = EditorGUILayout.ToggleLeft(new GUIContent("Read RGB values", null, "Read R G B values"), readRGB);
            }

            if (fileFormat == 7 || fileFormat == 8) // PTS, PCD
            {
                readIntensity = EditorGUILayout.ToggleLeft(new GUIContent("Read INT value", null, "Read INT values"), readIntensity);
                readRGB = readIntensity ? false : readRGB;
            }
            EditorGUILayout.EndHorizontal();

            /*
			if (fileFormats[fileFormat] == "PLY (ASCII)")
			{
				GUI.enabled = true;
			}else{
				GUI.enabled = false;
			}
			readNormals = EditorGUILayout.ToggleLeft(new GUIContent("Read Normal values",null,"Read NX NY NZ (float) values"), readNormals);
			GUI.enabled = true;*/
            EditorGUILayout.Space();

            // scaling
            useUnitScale = EditorGUILayout.BeginToggleGroup(new GUIContent("Scale values", null, "Enable scaling"), useUnitScale);
            unitScale = EditorGUILayout.FloatField(new GUIContent(" Scaling multiplier", null, "Multiply XYZ values with this multiplier"), unitScale);
            EditorGUILayout.EndToggleGroup();
            EditorGUILayout.Space();

            // flip y/z
            flipYZ = EditorGUILayout.ToggleLeft(new GUIContent("Flip Y & Z values", null, "Flip YZ values because Unity Y is up"), flipYZ);
            EditorGUILayout.Space();

            // offset
            autoOffsetNearZero = EditorGUILayout.ToggleLeft(new GUIContent("Auto-offset near 0,0,0", null, "Takes first line from xyz data as offset"), autoOffsetNearZero);
            enableManualOffset = EditorGUILayout.BeginToggleGroup(new GUIContent("Add Manual Offset", null, "Add this offset to XYZ values"), enableManualOffset);
            manualOffset = EditorGUILayout.Vector3Field(new GUIContent("Offset" + (flipYZ ? " (added before YZ values flip)" : ""), null, ""), manualOffset);
            EditorGUILayout.EndToggleGroup();
            EditorGUILayout.Space();
            GUI.enabled = sourceFile == null ? false : true;

            // extras
            //compressed = EditorGUILayout.ToggleLeft(new GUIContent("Compress colors",null,"Compresses RGB into single float"), compressed);

            // convert button
            if (GUILayout.Button(new GUIContent("Convert to Binary", "Convert source to binary"), GUILayout.Height(40)))
            {
                Convert2Binary();
            }
            GUI.enabled = true;
        } // OnGUI()


        bool IsNullOrEmptyLine(string line)
        {
            if (line.Length < 3 || line == null || line == string.Empty) { Debug.LogError("First line of the file is empty..quitting!"); return true; }
            return false;
        }

        // conversion function
        void Convert2Binary()
        {

            // TEMPORARY: Custom reader for LAS binary
            if (fileFormats[fileFormat] == "LAS")
            {
                LASConverter();
                return;
            }

            //var saveFilePath = EditorUtility.SaveFilePanelInProject("Save binary file","Assets/",sourceFile.name+".bin","bin");
            var saveFilePath = EditorUtility.SaveFilePanelInProject("Output binary file", sourceFile.name + ".bin", "bin", "");

            string fileToRead = AssetDatabase.GetAssetPath(sourceFile);
            if (!ValidateSaveAndRead(saveFilePath, fileToRead)) return;

            long lines = 0;

            // get initial data (so can check if data is ok)
            using (StreamReader streamReader = new StreamReader(File.OpenRead(fileToRead)))
            {
                double x = 0, y = 0, z = 0;
                float r = 0, g = 0, b = 0; //,nx=0,ny=0,nz=0;; // init vals
                string line = null;
                string[] row = null;

                PeekHeaderData headerCheck;
                headerCheck.x = 0; headerCheck.y = 0; headerCheck.z = 0;
                headerCheck.linesRead = 0;

                switch (fileFormats[fileFormat])
                {
                    case "ASC": // ASC (space at front)
                        {
                            headerCheck = PeekHeader.PeekHeaderASC(streamReader, readRGB);
                            if (!headerCheck.readSuccess) { streamReader.Close(); return; }
                            lines = headerCheck.linesRead;
                        }
                        break;

                    case "CGO": // CGO	(counter at first line and uses comma)
                        {
                            //                            headerCheck = PeekHeader.PeekHeaderCGO(txtReader, readRGB);
                            headerCheck = PeekHeader.PeekHeaderCGO(streamReader, readRGB, readIntensity, ref masterPointCount);
                            if (!headerCheck.readSuccess) { streamReader.Close(); return; }
                            lines = headerCheck.linesRead;
                        }
                        break;

                    case "CATIA ASC": // CATIA ASC (with header and Point Format           = 'X %f Y %f Z %f')
                        {
                            headerCheck = PeekHeader.PeekHeaderCATIA_ASC(streamReader, ref readRGB);
                            if (!headerCheck.readSuccess) { streamReader.Close(); return; }
                            lines = headerCheck.linesRead;
                        }
                        break;

                    case "XYZRGB":
                    case "XYZ": // XYZ RGB(INT)
                        {
                            headerCheck = PeekHeader.PeekHeaderXYZ(streamReader, ref readRGB);
                            if (!headerCheck.readSuccess) { streamReader.Close(); return; }
                            lines = headerCheck.linesRead;
                        }
                        break;

                    case "PTS": // PTS (INT) (RGB)
                        {
                            headerCheck = PeekHeader.PeekHeaderPTS(streamReader, readRGB, readIntensity, ref masterPointCount);
                            if (!headerCheck.readSuccess) { streamReader.Close(); return; }
                            lines = headerCheck.linesRead;
                        }
                        break;

                    case "PLY (ASCII)": // PLY (ASCII)
                        {
                            headerCheck = PeekHeader.PeekHeaderPLY(streamReader, readRGB, ref masterPointCount, ref plyHasNormals);
                            if (!headerCheck.readSuccess) { streamReader.Close(); return; }
                            //lines = headerCheck.linesRead;
                        }
                        break;

                    case "PCD (ASCII)": // PCD (ASCII)
                        {
                            headerCheck = PeekHeader.PeekHeaderPCD(streamReader, ref readRGB, ref masterPointCount);
                            if (headerCheck.readSuccess == false) { streamReader.Close(); return; }
                        }
                        break;

                    default:
                        Debug.LogError(appName + "> Unknown fileformat error (1) " + fileFormats[fileFormat]);
                        break;

                } // switch format


                if (autoOffsetNearZero == true)
                {
                    manualOffset = new Vector3((float)headerCheck.x, (float)headerCheck.y, (float)headerCheck.z);
                }

                // scaling enabled, scale offset too
                if (useUnitScale == true) manualOffset *= unitScale;

                // progressbar
                float progress = 0;
                long progressCounter = 0;

                // get total amount of points from formats that have it
                if (fileFormats[fileFormat] == "PLY (ASCII)" || fileFormats[fileFormat] == "PTS" || fileFormats[fileFormat] == "CGO" || fileFormats[fileFormat] == "PCD (ASCII)")
                {
                    lines = masterPointCount;

                    // reset back to start of file
                    streamReader.DiscardBufferedData();
                    streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
                    streamReader.BaseStream.Position = 0;

                    // get back to before first actual data line
                    for (int i = 0; i < headerCheck.linesRead - 1; i++)
                    {
                        streamReader.ReadLine();
                    }

                } else
                { // other formats need to be read completely

                    // reset back to start of file
                    streamReader.DiscardBufferedData();
                    streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
                    streamReader.BaseStream.Position = 0;

                    // get back to first actual data line
                    for (int i = 0; i < headerCheck.linesRead; i++)
                    {
                        streamReader.ReadLine();
                    }
                    lines = 0;

                    // calculate actual point data lines
                    int splitCount = 0;
                    while (!streamReader.EndOfStream)
                    {
                        line = streamReader.ReadLine();

                        if (progressCounter > 200000)
                        {
                            EditorUtility.DisplayProgressBar(appName, "Counting lines..", lines / 50000000.0f);
                            progressCounter = 0;
                        }

                        progressCounter++;

                        if (line.Length > 9)
                        {
                            splitCount = CharCount(line, ' ');
                            if (splitCount > 2 && splitCount < 16)
                            {
                                lines++;
                            }
                        }
                    }

                    Debug.Log("Founded lines:" + lines);

                    EditorUtility.ClearProgressBar();

                    // reset back to start of data
                    streamReader.DiscardBufferedData();
                    streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
                    streamReader.BaseStream.Position = 0;

                    // now skip header lines
                    for (int i = 0; i < headerCheck.linesRead; i++)
                    {
                        streamReader.ReadLine();
                    }

                    masterPointCount = lines;
                }

                // prepare to start saving binary file

                // write header
                var writer = new BinaryWriter(File.Open(saveFilePath, FileMode.Create));

                writer.Write(binaryVersion);
                writer.Write((System.Int32)masterPointCount);
                writer.Write(readRGB | readIntensity);

                progressCounter = 0;

                int skippedRows = 0;
                long rowCount = 0;
                bool haveMoreToRead = true;

                // for testing loading times
                //                System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
                //                stopwatch.Start();

                RegexOptions options = RegexOptions.None;
                Regex regex = new Regex("[ ]{2,}", options);


                // process all points
                while (haveMoreToRead == true)
                {
                    if (progressCounter > 256000)
                    {
                        EditorUtility.DisplayProgressBar(appName, "Converting point cloud to binary file", rowCount / (float)lines);
                        progressCounter = 0;
                    }

                    progressCounter++;

                    line = streamReader.ReadLine();

                    if (line != null && line.Length > 9)
                    {
                        // trim duplicate spaces
                        line = line.Replace("   ", " ").Replace("  ", " ").Trim();
                        row = line.Split(' ');

                        if (row.Length > 2)
                        {
                            switch (fileFormats[fileFormat])
                            {
                                case "ASC": // ASC
                                    if (IsFirstCharacter(line, '!') || IsFirstCharacter(line, '*'))
                                    {
                                        skippedRows++;
                                        continue;
                                    }
                                    x = double.Parse(row[0]);
                                    y = double.Parse(row[1]);
                                    z = double.Parse(row[2]);
                                    if (readRGB == true)
                                    {
                                        r = LUT255[int.Parse(row[3])];
                                        g = LUT255[int.Parse(row[4])];
                                        b = LUT255[int.Parse(row[5])];
                                    }
                                    break;

                                case "CGO": // CGO	(counter at first line and uses comma)
                                    if (IsFirstCharacter(line, '!') || IsFirstCharacter(line, '*'))
                                    {
                                        skippedRows++;
                                        continue;
                                    }
                                    x = double.Parse(row[0].Replace(",", "."));
                                    y = double.Parse(row[1].Replace(",", "."));
                                    z = double.Parse(row[2].Replace(",", "."));
                                    break;

                                case "CATIA ASC": // CATIA ASC (with header and Point Format           = 'X %f Y %f Z %f')
                                    if (IsFirstCharacter(line, '!') || IsFirstCharacter(line, '*'))
                                    {
                                        skippedRows++;
                                        continue;
                                    }
                                    x = double.Parse(row[1]);
                                    y = double.Parse(row[3]);
                                    z = double.Parse(row[5]);
                                    break;

                                case "XYZRGB":
                                case "XYZ": // XYZ RGB(INT)
                                    x = double.Parse(row[0]);
                                    y = double.Parse(row[1]);
                                    z = double.Parse(row[2]);
                                    if (readRGB == true)
                                    {
                                        r = LUT255[int.Parse(row[3])];
                                        g = LUT255[int.Parse(row[4])];
                                        b = LUT255[int.Parse(row[5])];
                                    }
                                    break;

                                case "PTS": // PTS (INT) (RGB)
                                    x = double.Parse(row[0]);
                                    y = double.Parse(row[1]);
                                    z = double.Parse(row[2]);

                                    if (readRGB == true)
                                    {
                                        if (row.Length == 7) // XYZIRGB
                                        {
                                            r = LUT255[int.Parse(row[4])];
                                            g = LUT255[int.Parse(row[5])];
                                            b = LUT255[int.Parse(row[6])];
                                        } else if (row.Length == 6) // XYZRGB
                                        {
                                            r = LUT255[int.Parse(row[3])];
                                            g = LUT255[int.Parse(row[4])];
                                            b = LUT255[int.Parse(row[5])];
                                        }
                                    } else if (readIntensity)
                                    {
                                        if (row.Length == 4 || row.Length == 7) // XYZI or XYZIRGB
                                        {
                                            // pts intensity -2048 to 2047 ?
                                            r = Remap(float.Parse(row[3]), -2048, 2047, 0, 1);
                                            g = r;
                                            b = r;
                                        }
                                    }
                                    break;

                                case "PLY (ASCII)": // PLY (ASCII)
                                    x = double.Parse(row[0]);
                                    y = double.Parse(row[1]);
                                    z = double.Parse(row[2]);

                                    /*
                                    // normals
                                    if (readNormals)
                                    {
                                        // Vertex normals are the normalized average of the normals of the faces that contain that vertex
                                        // TODO: need to fix normal values?
                                        nx = float.Parse(row[3]);
                                        ny = float.Parse(row[4]);
                                        nz = float.Parse(row[5]);

                                        // and rgb
                                        if (readRGB)
                                        {
                                            r = float.Parse(row[6])/255;
                                            g = float.Parse(row[7])/255;
                                            b = float.Parse(row[8])/255;
                                            //a = float.Parse(row[6])/255; // TODO: alpha not supported yet
                                        }

                                    }else{ // no normals, but maybe rgb
                                        */
                                    if (readRGB == true)
                                    {
                                        if (plyHasNormals == true)
                                        {
                                            r = float.Parse(row[6]) / 255f;
                                            g = float.Parse(row[7]) / 255f;
                                            b = float.Parse(row[8]) / 255f;
                                        } else
                                        { // no normals
                                            r = float.Parse(row[3]) / 255f;
                                            g = float.Parse(row[4]) / 255f;
                                            b = float.Parse(row[5]) / 255f;
                                        }
                                        //a = float.Parse(row[6])/255; // TODO: alpha not supported yet
                                    }
                                    /*
                                    }*/
                                    break;

                                case "PCD (ASCII)": // PCD (ASCII)
                                    x = double.Parse(row[0]);
                                    y = double.Parse(row[1]);
                                    z = double.Parse(row[2]);

                                    if (readRGB == true)
                                    {
                                        // TODO: need to check both rgb formats, this is for single value only
                                        if (row.Length == 4)
                                        {
                                            var rgb = (int)decimal.Parse(row[3], System.Globalization.NumberStyles.Float);
                                            r = (rgb >> 16) & 0x0000ff;
                                            g = (rgb >> 8) & 0x0000ff;
                                            b = (rgb) & 0x0000ff;
                                            r = LUT255[(int)r];
                                            g = LUT255[(int)g];
                                            b = LUT255[(int)b];
                                        } else if (row.Length == 6)
                                        {
                                            r = LUT255[int.Parse(row[3])];
                                            g = LUT255[int.Parse(row[4])];
                                            b = LUT255[int.Parse(row[5])];
                                        }
                                    } else if (readIntensity)
                                    {
                                        if (row.Length == 4) // XYZI only
                                        {
                                            r = float.Parse(row[3]);
                                            g = r;
                                            b = r;
                                        }
                                    }

                                    break;


                                default:
                                    Debug.LogError(appName + "> Error : Unknown format");
                                    break;

                            } // switch

                            // scaling enabled
                            if (useUnitScale == true)
                            {
                                x *= unitScale;
                                y *= unitScale;
                                z *= unitScale;
                            }

                            // manual offset enabled
                            if (autoOffsetNearZero == true || enableManualOffset == true)
                            {
                                x -= manualOffset.x;
                                y -= manualOffset.y;
                                z -= manualOffset.z;
                            }

                            if (flipYZ == true)
                            {
                                writer.Write((float)x);
                                writer.Write((float)z);
                                writer.Write((float)y);
                            } else
                            {
                                writer.Write((float)x);
                                writer.Write((float)y);
                                writer.Write((float)z);
                            }

                            // if have color data
                            if (readRGB == true || readIntensity == true)
                            {
                                writer.Write(r);
                                writer.Write(g);
                                writer.Write(b);
                            }
                            /*
                            // if have normals data, TODO: not possible yet
                            if (readNormals)
                            {
                                writer.Write(nx);
                                writer.Write(ny);
                                writer.Write(nz);
                            }
                            */

                            rowCount++;

                        } else
                        { // if row length
                          //Debug.Log(line);
                            skippedRows++;
                        }
                    }

                    // reached end or enough points
                    if (rowCount >= masterPointCount || streamReader.EndOfStream)
                    {

                        if (skippedRows > 0) Debug.LogWarning("Parser skipped " + skippedRows + " rows (probably bad data or comment lines in point cloud file)");
                        //Debug.Log(masterVertexCount);

                        if (rowCount < masterPointCount) // error, file ended too early, not enough points
                        {
                            Debug.LogWarning("File does not contain enough points, fixing point count to " + rowCount + " (expected : " + masterPointCount + ") - Fixing header point count.");

                            // fix header point count
                            writer.BaseStream.Seek(0, SeekOrigin.Begin);
                            writer.Write(binaryVersion);
                            writer.Write((System.Int32)rowCount);

                        }
                        haveMoreToRead = false;
                    }
                } // while loop reading file

                writer.Close();

                // for testing load timer
                //                stopwatch.Stop();
                //                Debug.Log("Timer: " + stopwatch.ElapsedMilliseconds + "ms"); // this one gives you the time in ms
                //                stopwatch.Reset();


                Debug.Log(appName + "> Binary file saved: " + saveFilePath + " (" + masterPointCount + " points)");
                EditorUtility.ClearProgressBar();
            } // using reader
        } // Convert2Binary



        void LASConverter()
        {
            var saveFilePath = EditorUtility.SaveFilePanel("Save binary file", "Assets/", sourceFile.name + ".bin", "bin");
            string fileToRead = AssetDatabase.GetAssetPath(sourceFile);
            if (!ValidateSaveAndRead(saveFilePath, fileToRead)) return;

            double x = 0f, y = 0f, z = 0f;
            float r = 0f, g = 0f, b = 0f;

            BinaryReader reader = new BinaryReader(File.OpenRead(fileToRead));
            string fileSignature = new string(reader.ReadChars(4));

            if (fileSignature != "LASF") Debug.LogError("LAS> FileSignature error: '" + fileSignature + "'");

            // NOTE: Currently most of this info is not used
            ushort fileSourceID = reader.ReadUInt16();
            ushort globalEncoding = reader.ReadUInt16();

            ulong projectID1 = reader.ReadUInt32(); // optional?
            ushort projectID2 = reader.ReadUInt16(); // optional?
            ushort projectID3 = reader.ReadUInt16(); // optional?
            string projectID4 = new string(reader.ReadChars(8)); // optional?

            byte versionMajor = reader.ReadByte();
            byte versionMinor = reader.ReadByte();

            string systemIdentifier = new string(reader.ReadChars(32));
            string generatingSoftware = new string(reader.ReadChars(32));

            ushort fileCreationDayOfYear = reader.ReadUInt16();
            ushort fileCreationYear = reader.ReadUInt16();
            ushort headerSize = reader.ReadUInt16();

            ulong offsetToPointData = reader.ReadUInt32();

            ulong numberOfVariableLengthRecords = reader.ReadUInt32();

            byte pointDataRecordFormat = reader.ReadByte();


            ushort PointDataRecordLength = reader.ReadUInt16();

            ulong legacyNumberOfPointRecords = reader.ReadUInt32();

            ulong[] legacyNumberOfPointsByReturn = new ulong[] { reader.ReadUInt32(), reader.ReadUInt32(), reader.ReadUInt32(), reader.ReadUInt32(), reader.ReadUInt32() };


            double xScaleFactor = reader.ReadDouble();
            double yScaleFactor = reader.ReadDouble();
            double zScaleFactor = reader.ReadDouble();

            double xOffset = reader.ReadDouble();
            double yOffset = reader.ReadDouble();
            double zOffset = reader.ReadDouble();
            double maxX = reader.ReadDouble();
            double minX = reader.ReadDouble();
            double MaxY = reader.ReadDouble();
            double minY = reader.ReadDouble();
            double maxZ = reader.ReadDouble();
            double minZ = reader.ReadDouble();

            // Only for 1.4
            if (versionMajor == 1 && versionMinor == 4)
            {
                ulong startOfFirstExtentedVariableLengthRecord = reader.ReadUInt64();
                ulong numberOfExtentedVariableLengthRecords = reader.ReadUInt32();

                ulong numberOfPointRecords = reader.ReadUInt64();
                ulong[] numberOfPointsByReturn = new ulong[] {reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),
                    reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),
                    reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64(),reader.ReadUInt64()};

                /*
				Debug.Log ("startOfFirstExtentedVariableLengthRecord:"+startOfFirstExtentedVariableLengthRecord); // FIXME
				Debug.Log ("numberOfExtentedVariableLengthRecords:"+numberOfExtentedVariableLengthRecords);
				Debug.Log ("numberOfPointRecords:"+numberOfPointRecords);
				Debug.Log ("numberOfPointsByReturn:"+numberOfPointsByReturn[0]); // *** // FIXME
				*/

            }


            /*
			Debug.Log ("fileSignature:"+fileSignature);
			Debug.Log ("fileSourceID:"+fileSourceID);
			Debug.Log ("globalEncoding:"+globalEncoding);
			Debug.Log ("ProjectID1:"+projectID1);
			Debug.Log ("ProjectID2:"+projectID2);
			Debug.Log ("ProjectID3:"+projectID3);
			Debug.Log ("ProjectID4:"+projectID4);
			
			Debug.Log ("versionMajor:"+versionMajor);
			Debug.Log ("versionMinor:"+versionMinor);
			Debug.Log ("systemIdentifier:"+systemIdentifier);
			Debug.Log ("generatingSoftware:"+generatingSoftware);
			Debug.Log ("fileCreationDayOfYear:"+fileCreationDayOfYear);
			Debug.Log ("fileCreationYear:"+fileCreationYear);
			Debug.Log ("headerSize:"+headerSize);
			Debug.Log ("offsetToPointData:"+offsetToPointData);
			Debug.Log ("numberOfVariableLengthRecords:"+numberOfVariableLengthRecords);
			Debug.Log ("pointDataRecordFormat:"+pointDataRecordFormat);
			Debug.Log ("PointDataRecordLength:"+PointDataRecordLength);
			Debug.Log ("legacyNumberOfPointRecords:"+legacyNumberOfPointRecords);
			Debug.Log ("legacyNumberOfPointsByReturn:"+legacyNumberOfPointsByReturn[0]); // ***
			Debug.Log ("xScaleFactor:"+xScaleFactor);
			Debug.Log ("yScaleFactor:"+yScaleFactor);
			Debug.Log ("zScaleFactor:"+zScaleFactor);
			Debug.Log ("xOffset:"+xOffset);
			Debug.Log ("yOffset:"+yOffset);
			Debug.Log ("zOffset:"+zOffset);
			Debug.Log ("maxX:"+maxX);
			Debug.Log ("minX:"+minX);
			Debug.Log ("MaxY:"+MaxY);
			Debug.Log ("minY:"+minY);
			Debug.Log ("maxZ:"+maxZ);
			Debug.Log ("minZ:"+minZ);
			*/


            //ulong numberOfPointRecords = reader.ReadUInt64();
            // VariableLengthRecords
            if (numberOfVariableLengthRecords > 0)
            {
                ushort vlrReserved = reader.ReadUInt16();
                string vlrUserID = new string(reader.ReadChars(16));
                ushort vlrRecordID = reader.ReadUInt16();
                ushort vlrRecordLengthAfterHeader = reader.ReadUInt16();
                string vlrDescription = new string(reader.ReadChars(32));
                /*
				Debug.Log("vlrReserved:"+vlrReserved);
				Debug.Log("vlrUserID:"+vlrUserID);
				Debug.Log("vlrRecordID:"+vlrRecordID);
				Debug.Log("vlrRecordLengthAfterHeader:"+vlrRecordLengthAfterHeader);
				Debug.Log("vlrDescription:"+vlrDescription);
				*/

            }

            // jump to points start pos
            reader.BaseStream.Seek((long)offsetToPointData, SeekOrigin.Begin);

            // format #2
            if (pointDataRecordFormat != 2 && pointDataRecordFormat != 3) Debug.LogWarning("LAS Import might fail - only pointDataRecordFormat #2 & #3 are supported (Your file is " + pointDataRecordFormat + ")");
            if (versionMinor != 2) Debug.LogWarning("LAS Import might fail - only version LAS 1.2 is supported. (Your file is " + versionMajor + "." + versionMinor + ")");

            masterPointCount = (int)legacyNumberOfPointRecords;

            // scaling enabled, scale manual offset
            if (useUnitScale) manualOffset *= unitScale;

            // progressbar
            long progress = 0;
            long progressCounter = 0;
            EditorUtility.ClearProgressBar();

            // saving, write header
            var writer = new BinaryWriter(File.Open(saveFilePath, FileMode.Create));
            writer.Write(binaryVersion);
            writer.Write((System.Int32)masterPointCount);
            writer.Write(readRGB);

            long rowCount = 0;
            bool haveMoreToRead = true;
            bool firstPointRead = false;

            // process all points
            while (haveMoreToRead == true)
            {
                if (progressCounter > 256000)
                {
                    if (EditorUtility.DisplayCancelableProgressBar(appName, "Converting point cloud to binary : " + progress + " / " + masterPointCount, progress / (float)masterPointCount))
                    {
                        Debug.Log("Cancelled at " + progress + " / " + masterPointCount);
                        haveMoreToRead = false;
                        break;
                    }
                    progressCounter = 0;
                }

                progressCounter++;
                progress++;

                long intX = reader.ReadInt32();
                long intY = reader.ReadInt32();
                long intZ = reader.ReadInt32();

                reader.ReadBytes(8); // unknown

                if (pointDataRecordFormat == 3) reader.ReadBytes(8); // GPS Time for format#3

                var colorR = reader.ReadBytes(2); // RED
                var colorG = reader.ReadBytes(2); // GREEN
                var colorB = reader.ReadBytes(2); // BLUE

                x = intX * xScaleFactor + xOffset;
                y = intY * yScaleFactor + yOffset;
                z = intZ * zScaleFactor + zOffset;

                // manual scaling enabled
                if (useUnitScale == true)
                {
                    x *= unitScale;
                    y *= unitScale;
                    z *= unitScale;
                }

                if (flipYZ == true)
                {
                    double yy = y;
                    y = z;
                    z = yy;
                }

                if (autoOffsetNearZero == true)
                {
                    if (firstPointRead == false)
                    {
                        manualOffset = new Vector3((float)x, (float)y, (float)z);
                        firstPointRead = true;
                    }

                    x -= manualOffset.x;
                    y -= manualOffset.y;
                    z -= manualOffset.z;
                } else // no auto offset
                {
                    if (enableManualOffset == true)
                    {
                        x -= manualOffset.x;
                        y -= manualOffset.y;
                        z -= manualOffset.z;
                    }
                }

                writer.Write((float)x);
                writer.Write((float)y);
                writer.Write((float)z);

                if (readRGB == true)
                {
                    r = (float)System.BitConverter.ToUInt16(colorR, 0);
                    g = (float)System.BitConverter.ToUInt16(colorG, 0);
                    b = (float)System.BitConverter.ToUInt16(colorB, 0);

                    //if (rowCount<100)	Debug.Log("row:"+(rowCount+1)+" xyz:"+x+","+y+","+z+" : "+r+","+g+","+b);

                    //LUT255[int.Parse(row[3])];

                    r = ((float)r) / 256f;
                    g = ((float)g) / 256f;
                    b = ((float)b) / 256f;

                    // fix for high values
                    if (r > 1) r /= 256f;
                    if (g > 1) g /= 256f;
                    if (b > 1) b /= 256f;

                    writer.Write(r);
                    writer.Write(g);
                    writer.Write(b);
                }

                rowCount++;

                if (reader.BaseStream.Position >= reader.BaseStream.Length || rowCount >= masterPointCount)
                {

                    if (rowCount < masterPointCount) // error, file ended too early, not enough points
                    {
                        Debug.LogWarning("LAS file does not contain enough points, fixing point count to " + rowCount);

                        // fix header point count
                        writer.BaseStream.Seek(0, SeekOrigin.Begin);
                        writer.Write(binaryVersion);
                        writer.Write((System.Int32)rowCount);
                    }
                    haveMoreToRead = false;
                }

            } // while loop reading file

            writer.Close();

            Debug.Log(appName + "> Binary file saved: " + saveFilePath + " (" + masterPointCount + " points)");
            EditorUtility.ClearProgressBar();
        }


        bool ValidateSaveAndRead(string path, string fileToRead)
        {
            if (path.Length < 1) { Debug.Log(appName + "> Save cancelled.."); return false; }
            if (fileToRead.Length < 1) { Debug.LogError(appName + "> Cannot find file (" + fileToRead + ")"); return false; }
            if (!File.Exists(fileToRead)) { Debug.LogError(appName + "> Cannot find file (" + fileToRead + ")"); return false; }
            if (Path.GetExtension(fileToRead).ToLower() == ".bin") { Debug.LogError("Source file extension is .bin, binary file conversion is not supported"); return false; }
            return true;
        }

        float Remap(float source, float sourceFrom, float sourceTo, float targetFrom, float targetTo)
        {
            return targetFrom + (source - sourceFrom) * (targetTo - targetFrom) / (sourceTo - sourceFrom);
        }

        string GetSelectedFileInfo()
        {
            string tempFilePath = AssetDatabase.GetAssetPath(sourceFile);

            if (Directory.Exists(tempFilePath))
            {
                return "[ Folders are NOT yet supported for binary conversion ]";
            } else
            {
                string tempFileName = Path.GetFileName(tempFilePath);
                return tempFileName + " (" + (new FileInfo(tempFilePath).Length / 1000000) + "MB)";
            }
        }



        int CharCount(string source, char separator)
        {
            int count = 0;
            for (int i = 0, length = source.Length; i < length; i++)
            {
                if (source[i] == separator) count++;
            }
            return count;
        }

        bool IsFirstCharacter(string source, char toFind)
        {
            if (source == null || source.Length == 0) return false;
            return source[0] == toFind;
        }

        // http://stackoverflow.com/a/16776096/5452781
        string FilterWhiteSpaces(string input)
        {
            if (input == null)
                return string.Empty;

            StringBuilder stringBuilder = new StringBuilder(input.Length);
            for (int i = 0; i < input.Length; i++)
            {
                char c = input[i];
                if (i == 0 || c != ' ' || (c == ' ' && input[i - 1] != ' '))
                    stringBuilder.Append(c);
            }
            return stringBuilder.ToString();
        }

        float[] LUT255 = new float[] { 0f, 0.00392156862745098f, 0.00784313725490196f, 0.011764705882352941f, 0.01568627450980392f, 0.0196078431372549f, 0.023529411764705882f, 0.027450980392156862f, 0.03137254901960784f, 0.03529411764705882f, 0.0392156862745098f, 0.043137254901960784f, 0.047058823529411764f, 0.050980392156862744f, 0.054901960784313725f, 0.058823529411764705f, 0.06274509803921569f, 0.06666666666666667f, 0.07058823529411765f, 0.07450980392156863f, 0.0784313725490196f, 0.08235294117647059f, 0.08627450980392157f, 0.09019607843137255f, 0.09411764705882353f, 0.09803921568627451f, 0.10196078431372549f, 0.10588235294117647f, 0.10980392156862745f, 0.11372549019607843f, 0.11764705882352941f, 0.12156862745098039f, 0.12549019607843137f, 0.12941176470588237f, 0.13333333333333333f, 0.13725490196078433f, 0.1411764705882353f, 0.1450980392156863f, 0.14901960784313725f, 0.15294117647058825f, 0.1568627450980392f, 0.1607843137254902f, 0.16470588235294117f, 0.16862745098039217f, 0.17254901960784313f, 0.17647058823529413f, 0.1803921568627451f, 0.1843137254901961f, 0.18823529411764706f, 0.19215686274509805f, 0.19607843137254902f, 0.2f, 0.20392156862745098f, 0.20784313725490197f, 0.21176470588235294f, 0.21568627450980393f, 0.2196078431372549f, 0.2235294117647059f, 0.22745098039215686f, 0.23137254901960785f, 0.23529411764705882f, 0.23921568627450981f, 0.24313725490196078f, 0.24705882352941178f, 0.25098039215686274f, 0.2549019607843137f, 0.25882352941176473f, 0.2627450980392157f, 0.26666666666666666f, 0.27058823529411763f, 0.27450980392156865f, 0.2784313725490196f, 0.2823529411764706f, 0.28627450980392155f, 0.2901960784313726f, 0.29411764705882354f, 0.2980392156862745f, 0.30196078431372547f, 0.3058823529411765f, 0.30980392156862746f, 0.3137254901960784f, 0.3176470588235294f, 0.3215686274509804f, 0.3254901960784314f, 0.32941176470588235f, 0.3333333333333333f, 0.33725490196078434f, 0.3411764705882353f, 0.34509803921568627f, 0.34901960784313724f, 0.35294117647058826f, 0.3568627450980392f, 0.3607843137254902f, 0.36470588235294116f, 0.3686274509803922f, 0.37254901960784315f, 0.3764705882352941f, 0.3803921568627451f, 0.3843137254901961f, 0.38823529411764707f, 0.39215686274509803f, 0.396078431372549f, 0.4f, 0.403921568627451f, 0.40784313725490196f, 0.4117647058823529f, 0.41568627450980394f, 0.4196078431372549f, 0.4235294117647059f, 0.42745098039215684f, 0.43137254901960786f, 0.43529411764705883f, 0.4392156862745098f, 0.44313725490196076f, 0.4470588235294118f, 0.45098039215686275f, 0.4549019607843137f, 0.4588235294117647f, 0.4627450980392157f, 0.4666666666666667f, 0.47058823529411764f, 0.4745098039215686f, 0.47843137254901963f, 0.4823529411764706f, 0.48627450980392156f, 0.49019607843137253f, 0.49411764705882355f, 0.4980392156862745f, 0.5019607843137255f, 0.5058823529411764f, 0.5098039215686274f, 0.5137254901960784f, 0.5176470588235295f, 0.5215686274509804f, 0.5254901960784314f, 0.5294117647058824f, 0.5333333333333333f, 0.5372549019607843f, 0.5411764705882353f, 0.5450980392156862f, 0.5490196078431373f, 0.5529411764705883f, 0.5568627450980392f, 0.5607843137254902f, 0.5647058823529412f, 0.5686274509803921f, 0.5725490196078431f, 0.5764705882352941f, 0.5803921568627451f, 0.5843137254901961f, 0.5882352941176471f, 0.592156862745098f, 0.596078431372549f, 0.6f, 0.6039215686274509f, 0.6078431372549019f, 0.611764705882353f, 0.615686274509804f, 0.6196078431372549f, 0.6235294117647059f, 0.6274509803921569f, 0.6313725490196078f, 0.6352941176470588f, 0.6392156862745098f, 0.6431372549019608f, 0.6470588235294118f, 0.6509803921568628f, 0.6549019607843137f, 0.6588235294117647f, 0.6627450980392157f, 0.6666666666666666f, 0.6705882352941176f, 0.6745098039215687f, 0.6784313725490196f, 0.6823529411764706f, 0.6862745098039216f, 0.6901960784313725f, 0.6941176470588235f, 0.6980392156862745f, 0.7019607843137254f, 0.7058823529411765f, 0.7098039215686275f, 0.7137254901960784f, 0.7176470588235294f, 0.7215686274509804f, 0.7254901960784313f, 0.7294117647058823f, 0.7333333333333333f, 0.7372549019607844f, 0.7411764705882353f, 0.7450980392156863f, 0.7490196078431373f, 0.7529411764705882f, 0.7568627450980392f, 0.7607843137254902f, 0.7647058823529411f, 0.7686274509803922f, 0.7725490196078432f, 0.7764705882352941f, 0.7803921568627451f, 0.7843137254901961f, 0.788235294117647f, 0.792156862745098f, 0.796078431372549f, 0.8f, 0.803921568627451f, 0.807843137254902f, 0.8117647058823529f, 0.8156862745098039f, 0.8196078431372549f, 0.8235294117647058f, 0.8274509803921568f, 0.8313725490196079f, 0.8352941176470589f, 0.8392156862745098f, 0.8431372549019608f, 0.8470588235294118f, 0.8509803921568627f, 0.8549019607843137f, 0.8588235294117647f, 0.8627450980392157f, 0.8666666666666667f, 0.8705882352941177f, 0.8745098039215686f, 0.8784313725490196f, 0.8823529411764706f, 0.8862745098039215f, 0.8901960784313725f, 0.8941176470588236f, 0.8980392156862745f, 0.9019607843137255f, 0.9058823529411765f, 0.9098039215686274f, 0.9137254901960784f, 0.9176470588235294f, 0.9215686274509803f, 0.9254901960784314f, 0.9294117647058824f, 0.9333333333333333f, 0.9372549019607843f, 0.9411764705882353f, 0.9450980392156862f, 0.9490196078431372f, 0.9529411764705882f, 0.9568627450980393f, 0.9607843137254902f, 0.9647058823529412f, 0.9686274509803922f, 0.9725490196078431f, 0.9764705882352941f, 0.9803921568627451f, 0.984313725490196f, 0.9882352941176471f, 0.9921568627450981f, 0.996078431372549f, 1f };

    } // class
} // namespace

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace PointCloudExtras
{
    // you can make your own measurement manager, by subscribing that BinaryViewerDX11.PointWasSelected event
    public class MeasurementManager : MonoBehaviour
    {
        [Header("References & Settings")]
        public Text distanceUIText;
        public Color lineColor = Color.yellow;

        protected bool isFirstPoint = true;
        protected bool haveFirstPoint = false;
        protected bool haveSecondPoint = false;
        protected Vector3 startPos;
        protected Vector3 endPos;
        protected Vector3 previousPoint = Vector3.zero;

        // octree sets this value when founded
        public static float closestDistance = 0;

        void Start()
        {
            closestDistance = 0;

            // subscribe to event listener
            //pointCloudViewer.GetComponent<unitycodercom_PointCloudBinaryViewer.BinaryViewerDX11>().PointWasSelected += PointSelected;
            unitycodercom_PointCloudBinaryViewer.BinaryViewerDX11.PointWasSelected -= PointSelected; // unsubscribe just in case
            unitycodercom_PointCloudBinaryViewer.BinaryViewerDX11.PointWasSelected += PointSelected;

        }
        // draw line between selected points
        public virtual void Update()
        {
            if (haveFirstPoint == true && haveSecondPoint == false)
            {
                //            endPos = cam.ScreenToWorldPoint(Input.mousePosition);
                //            GLDebug.DrawLine(startPos, endPos, lineColor, 0, true);
            } else if (haveFirstPoint == true && haveSecondPoint == true)
            {
                DrawLine();
            }
        }

        public virtual void DrawLine()
        {
            GLDebug.DrawLine(startPos, endPos, lineColor, 0, true);
        }

        // gets called when PointWasSelected event fires in BinaryViewerDX11
        void PointSelected(Vector3 pos)
        {
            // was this the first selection
            if (isFirstPoint == true)
            {
                startPos = pos;
                distanceUIText.text = "Measure: Select 2nd point";
                haveFirstPoint = true;
                haveSecondPoint = false;
            } else
            { // it was 2nd click
                endPos = pos;
                haveSecondPoint = true;

                var distance = Vector3.Distance(previousPoint, pos);
                distanceUIText.text = "Distance:" + distance.ToString();
                Debug.Log("Distance:" + distance);
            }

            previousPoint = pos;
            isFirstPoint = !isFirstPoint; // flip boolean
        }


        private void OnDestroy()
        {
            // unsubscribe
            unitycodercom_PointCloudBinaryViewer.BinaryViewerDX11.PointWasSelected -= PointSelected;
        }

    }
}
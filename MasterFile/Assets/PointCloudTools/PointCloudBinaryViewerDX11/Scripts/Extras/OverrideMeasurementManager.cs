﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PointCloudExtras
{

    public class OverrideMeasurementManager : MeasurementManager
    {
        public override void DrawLine()
        {
            // example just uses random color
            GLDebug.DrawLine(startPos, endPos, Random.ColorHSV(0, 1, 0, 1), 0, true);
        }
    }
}
﻿using UnityEngine;
using System.Collections;

// Multiple Cloud Spawner (Instantiates new DX11Viewer for each cloud)

namespace PointCloudExtras
{

    public class MultiCloudManager : MonoBehaviour
    {
        public KeyCode loadKey = KeyCode.Alpha1; // press this key to load next cloud

        public GameObject dx11ViewerPrefab;
        public Material pointCloudMaterial;

        public string[] cloudsToLoad;
        int cloudIndex = 0;

        // mainloop
        void Update()
        {
            // spawn new binaryviewers with keypress
            if (Input.GetKeyDown(loadKey))
            {
                if (cloudIndex < cloudsToLoad.Length)
                {
                    Debug.Log("Spawning new viewer for file and overriding prefab values:" + cloudsToLoad[cloudIndex], gameObject);
                    var go = Instantiate(dx11ViewerPrefab);
                    var pc = go.GetComponent<unitycodercom_PointCloudBinaryViewer.BinaryViewerDX11>();

                    // for demo, we move the cloud a bit, since its same..note not all shaders support transform offset
                    pc.transform.position += Vector3.up * 50 * cloudIndex;

                    // we override new cloudviewer parameters for the binaryviewer component (instead of using the prefab values)
                    pc.loadAtStart = false;
                    pc.fileName = cloudsToLoad[cloudIndex];

                    // set new material (REQUIRED to be unique material for each cloud)
                    pc.cloudMaterial = new Material(pointCloudMaterial);

                    // enable and load new cloud
                    pc.useThreading = true; // should use threading, so that it wont hang this main thread..

                    pc.enabled = true;
                    pc.CallReadPointCloudThreaded();

                    cloudIndex++;
                } else
                {
                    print("No more clouds to load..");
                }
            }
        }

    }
}
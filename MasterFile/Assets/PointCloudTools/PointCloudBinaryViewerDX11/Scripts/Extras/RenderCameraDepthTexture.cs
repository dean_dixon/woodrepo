﻿using UnityEngine;

namespace unitycodercom_pointcloud_extras
{
    public class RenderCameraDepthTexture : MonoBehaviour
    {
        void OnEnable()
        {
            var cam = GetComponent<Camera>();
            if (cam != null)
            {
                // enable camera depth texture
                cam.depthTextureMode = DepthTextureMode.Depth;
            }
            //          Debug.Log(Camera.main.depthTextureMode);
        }
    }
}

﻿// Point Cloud Binary Viewer DX11
// reads custom binary file and displays it with dx11 shader
// http://unitycoder.com

#if !UNITY_WEBPLAYER && !UNITY_SAMSUNGTV

using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.EventSystems;
using System.Threading;
using NitionUnityCoder;

namespace unitycodercom_PointCloudBinaryViewer
{
    class ThreadInfoOctTree
    {
        public Vector3 startPoint;
        public Vector3 direction;
    }

    class OctTreePoint
    {
        public int index;
    }

    class ThreadReaderInfo
    {
        public string fileName;
    }


    //[ExecuteInEditMode] // ** You can enable this, if you want to see DX11 cloud inside editor, without playmode **
    public class BinaryViewerDX11 : MonoBehaviour
    {
        [Header("Binary Source File")]
        public string baseFolder = "";
        public string fileName = "StreamingAssets/sample.bin";

        [Header("Settings")]
        public Material cloudMaterial;
        public bool loadAtStart = true;
        [Header("For Brekel Data Only")]
        public bool isAnimated = false; // Brekel binary cloud frames
        public float playbackDelay = 0.025F;

        private byte binaryVersion = 0;
        private int numberOfFrames = 0;
        //private float frameRate=30f; // Not used yet
        [HideInInspector]
        public bool containsRGB = false;

        // Brekel animated frames variables
        private int[] numberOfPointsPerFrame;
        private System.Int64[] frameBinaryPositionForEachFrame;
        private Vector3[] animatedPointArray;
        private Vector3[] animatedColorArray;
        private int totalNumberOfPoints; // total from each frame
        private int currentFrame = 0;
        private int[] animatedOffset;
        private float nextFrame = 0.0F;
        private float[] byteArrayToFloats;

        //private bool readNormals = false;
        [HideInInspector]
        public int totalPoints = 0;
        private ComputeBuffer bufferPoints;
        private ComputeBuffer bufferColors;
        private int instanceCount = 1;
        [HideInInspector]
        public Vector3[] points;
        [HideInInspector]
        public Vector3[] pointColors;
        private string fileToRead = "";
        private Vector3 dataColor;
        private float r, g, b;

        private bool isLoading = false;
        private bool haveError = false;

        [Header("Experimental")]

        // Threading (loading and parsing)
        public bool useThreading = false;
        public bool useOctTree = true;


        // Experimental point picking (brute force)
        public bool enablePicking = true;
        public delegate void PointSelected(Vector3 pointPos);
        public static event PointSelected PointWasSelected;
        // how many points are checked for measurement per frame (larger values will hang mainthread longer, too low values cause measuring to take very long time)
        int maxIterationsPerFrame = 1000000;
        bool isSearchingPoint = false;

        // Threading (measurements)
        int maxMeasureThreads = 1; // set max threads here, for now should be limited to 1 for measurements 
        static readonly object _measureCountLock = new object();
        static int _measureThreadCount = 0;
        static bool abortMeasureThread = false;

        int maxReaderThreads = 1; // for now use just 1 thread
        static readonly object _readerCountLock = new object();
        static int _readerThreadCount = 0;
        static bool abortReaderThread = false;

        private Camera cam;

        // octree
        PointOctree<OctTreePoint> pointTree;
        public float pointSearchRadius = 0.5f;
        public bool highliteNearestPoint = true;


        void Awake()
        {
            // reset static vars
            _measureThreadCount = 0;
            abortMeasureThread = false;
            _readerThreadCount = 0;
            abortReaderThread = false;
        }

        void Start()
        {
            cam = Camera.main;

            if (useThreading)
            {
                // check if MainThread script exists in scene, its required only for threading
                if (GameObject.Find("#MainThreadHelper") == null)
                {
                    var go = new GameObject();
                    go.name = "#MainThreadHelper";
                    go.AddComponent<UnityLibrary.MainThread>();
                }
            }

            if (loadAtStart == true)
            {
                if (useThreading == true)
                {
                    abortReaderThread = false;
                    CallReadPointCloudThreaded();
                } else
                {
                    ReadPointCloud();
                }
            }
        }

        // ====================================== mainloop ======================================
        void Update()
        {
            if (isLoading == true && haveError == true) return;

            if (isAnimated == true) // brekel animated cloud
            {
                if (Time.time > nextFrame)
                {
                    nextFrame = Time.time + playbackDelay;
                    System.Array.Copy(animatedPointArray, animatedOffset[currentFrame], points, 0, numberOfPointsPerFrame[currentFrame]);
                    bufferPoints.SetData(points);
                    if (bufferColors != null)
                    {
                        System.Array.Copy(animatedColorArray, animatedOffset[currentFrame], pointColors, 0, numberOfPointsPerFrame[currentFrame]);
                        bufferColors.SetData(pointColors);
                    }
                    currentFrame = (++currentFrame) % numberOfFrames;
                }
            }

            // experimentel point picking
            if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject()) return;

            if (highliteNearestPoint == true && useOctTree == true) HighlightClosestPoint();
            if (enablePicking == true && useThreading == true) SelectClosestPoint();
        }


        // binary point cloud reader *OLD, non-threaded
        public void ReadPointCloud()
        {
            // set full path to file
            fileToRead = Application.dataPath + "/" + baseFolder + fileName;
            Debug.Log("Reading pointcloud from: " + fileToRead);


            if (CheckIfFileExists(fileToRead) == false)
            {
                Debug.LogError("File not found:" + fileToRead);
                ShowMessage("File not found: " + fileToRead);
                haveError = true;
                return;
            }

            if (isAnimated == true)
            {
                ReadAnimatedPointCloud();
                return;
            }

            isLoading = true;

            // for testing loading times
            // System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            // stopwatch.Start();

            // new loader reads whole file at once
            byte[] data;

            try
            {
                data = File.ReadAllBytes(fileToRead);
            }
            catch
            {
                Debug.LogError(fileToRead + " cannot be opened with ReadAllBytes(), it might be too large >2gb");
                return;
            }

            System.Int32 byteIndex = 0;

            int binaryVersion = data[byteIndex];
            byteIndex += sizeof(System.Byte);

            // check format
            if (binaryVersion > 1)
            {
                Debug.LogError("File binaryVersion should have value (0) or (1). Loading cancelled... founded:" + binaryVersion + " (Is this animated cloud instead?)");
                return;
            }

            totalPoints = System.BitConverter.ToInt32(data, byteIndex);
            byteIndex += sizeof(System.Int32);

            containsRGB = System.BitConverter.ToBoolean(data, byteIndex);
            byteIndex += sizeof(System.Boolean);

            points = new Vector3[totalPoints];
            Debug.Log("Loading " + totalPoints + " points..");

            float x, y, z;

            if (containsRGB) pointColors = new Vector3[totalPoints];

            var byteSize = sizeof(System.Single);

            byteArrayToFloats = new float[(data.Length - byteIndex) / 4];
            System.Buffer.BlockCopy(data, byteIndex, byteArrayToFloats, 0, data.Length - byteIndex);

            int dataIndex = 0;
            for (int i = 0; i < totalPoints; i++)
            {
                x = byteArrayToFloats[dataIndex];
                dataIndex++;
                byteIndex += byteSize;
                y = byteArrayToFloats[dataIndex];
                dataIndex++;
                byteIndex += byteSize;
                z = byteArrayToFloats[dataIndex];
                dataIndex++;
                byteIndex += byteSize;

                points[i].Set(x, y, z);

                // need to move rgb after xyz
                if (containsRGB == true)
                {
                    r = byteArrayToFloats[dataIndex];
                    dataIndex++;
                    g = byteArrayToFloats[dataIndex];
                    dataIndex++;
                    b = byteArrayToFloats[dataIndex];
                    dataIndex++;
                    pointColors[i].Set(r, g, b);
                }
            }

            // for testing load timer
            // stopwatch.Stop();
            // Debug.Log("Timer: " + stopwatch.ElapsedMilliseconds + "ms");
            // stopwatch.Reset();

            InitDX11Buffers();
            isLoading = false;
        }


        public void CallReadPointCloudThreaded()
        {
            // load in another thread
            fileToRead = Application.dataPath + "/" + baseFolder + fileName;
            Debug.Log("Reading threaded pointcloud file: " + fileToRead, gameObject);

            if (CheckIfFileExists(fileToRead) == false)
            {
                Debug.LogError("File not found:" + fileToRead);
                ShowMessage("File not found: " + fileToRead);
                return;
            }

            // pass in filename
            ThreadReaderInfo threadReaderData = new ThreadReaderInfo();
            threadReaderData.fileName = fileToRead;

            ThreadPool.QueueUserWorkItem(new WaitCallback(ReadPointCloudThreaded), threadReaderData);

            // if using octtree
            // TODO: need to reset this if new data is loaded
            if (useOctTree == true)
            {
                pointTree = new PointOctree<OctTreePoint>(100, transform.position, 1);
            }
        }

        public void CallReadPointCloudThreaded(string fullPath)
        {
            fileToRead = fullPath;
            Debug.Log("Reading threaded pointcloud file: " + fileToRead, gameObject);

            if (CheckIfFileExists(fileToRead) == false)
            {
                Debug.LogError("File not found:" + fileToRead);
                ShowMessage("File not found: " + fileToRead);
                return;
            }

            // pass in filename
            ThreadReaderInfo threadReaderData = new ThreadReaderInfo();
            threadReaderData.fileName = fileToRead;

            ThreadPool.QueueUserWorkItem(new WaitCallback(ReadPointCloudThreaded), threadReaderData);

            // if using octtree
            // TODO: need to reset this if new data is loaded
            if (useOctTree == true)
            {
                pointTree = new PointOctree<OctTreePoint>(100, transform.position, 1);
            }
        }

        // binary point cloud reader (using separate thread)
        public void ReadPointCloudThreaded(System.Object a)
        {
            if (isAnimated == true)
            {
                Debug.LogError("Reading Animated Point Clouds in Separate thread is not supported");
                return;
            }

            // for testing loading times
            //System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            //stopwatch.Start();

            while (abortReaderThread == false)
            {
                lock (_readerCountLock)
                {
                    if (_readerThreadCount < maxReaderThreads && !abortReaderThread)
                    {
                        _readerThreadCount++;
                        break;
                    }

                    if (abortReaderThread == true)
                    {
                        _readerThreadCount--;
                        return;
                    }
                }
                Thread.Sleep(50);
            }

            if (abortReaderThread == true)
            {
                _readerThreadCount--;
                return;
            }

            // FIXME: issues with multiloader
            isLoading = true;

            byte[] data;

            try
            {
                data = File.ReadAllBytes(fileToRead);
            }
            catch
            {
                Debug.LogError(fileToRead + " cannot be opened with ReadAllBytes(), it might be too large >2gb");
                return;
            }

            System.Int32 byteIndex = 0;

            int binaryVersion = data[byteIndex];
            byteIndex += sizeof(System.Byte);

            // check format
            if (binaryVersion > 1)
            {
                Debug.LogError("File binaryVersion should have value (0) or (1). Loading cancelled.");
                return;
            }

            totalPoints = (int)System.BitConverter.ToInt32(data, byteIndex);
            byteIndex += sizeof(System.Int32);

            containsRGB = System.BitConverter.ToBoolean(data, byteIndex);
            byteIndex += sizeof(System.Boolean);

            points = new Vector3[totalPoints];
            Debug.Log("Loading " + totalPoints + " points..");

            float x, y, z;

            if (containsRGB == true) pointColors = new Vector3[totalPoints];

            var byteSize = sizeof(System.Single);

            byteArrayToFloats = new float[(data.Length - byteIndex) / 4];
            System.Buffer.BlockCopy(data, byteIndex, byteArrayToFloats, 0, data.Length - byteIndex);

            int dataIndex = 0;
            for (int i = 0; i < totalPoints; i++)
            {
                x = byteArrayToFloats[dataIndex];
                dataIndex++;
                byteIndex += byteSize;
                y = byteArrayToFloats[dataIndex];
                dataIndex++;
                byteIndex += byteSize;
                z = byteArrayToFloats[dataIndex];
                dataIndex++;
                byteIndex += byteSize;

                points[i].Set(x, y, z);

                if (containsRGB == true)
                {
                    r = byteArrayToFloats[dataIndex];
                    byteIndex += byteSize;
                    dataIndex++;
                    g = byteArrayToFloats[dataIndex];
                    byteIndex += byteSize;
                    dataIndex++;
                    b = byteArrayToFloats[dataIndex];
                    byteIndex += byteSize;
                    dataIndex++;

                    pointColors[i].Set(r, g, b);
                }

                if (abortReaderThread == true)
                {
                    _readerThreadCount--;
                    return;
                }
            }

            // for testing load timer
            //            stopwatch.Stop();
            //            Debug.Log("Timer: " + stopwatch.ElapsedMilliseconds + "ms");
            //            stopwatch.Reset();

            // refresh buffers
            UnityLibrary.MainThread.Call(InitDX11Buffers);
            UnityLibrary.MainThread.Call(UpdatePointData);
            if (containsRGB == true) UnityLibrary.MainThread.Call(UpdateColorData);

            isLoading = false;

            // 2nd pass, fill octree
            if (useOctTree == true)
            {
                Debug.Log("Building octree...");
                for (int i = 0; i < totalPoints; i++)
                {
                    var p = new OctTreePoint();
                    p.index = i;
                    pointTree.Add(p, points[i]);

                    if (abortReaderThread)
                    {
                        _readerThreadCount--;
                        break;
                    }
                }
                Debug.Log("Building octree: Done");
            }
            _readerThreadCount--;
        }


        // For Brekel animated binary data only
        void ReadAnimatedPointCloud()
        {
            if (isAnimated == false)
            {
                Debug.LogWarning("ReadAnimatedPointCloud() called, but isAnimated = false");
                return;
            }

            isLoading = true;

            // NOTE: Reads whole file into memory, could cause problems with huge files
            // TODO: Add option to stream from disk or read in smaller chunks
            var data = File.ReadAllBytes(fileToRead);

            if (data.Length < 1)
            {
                Debug.LogError("ReadAnimatedPointCloud() called, but isAnimated = false");
                return;
            }

            System.Int32 byteIndex = 0;
            binaryVersion = data[byteIndex];
            if (binaryVersion != 2) { Debug.LogError("For Animated point cloud, file binaryVersion should have value (2) or bigger. received=" + binaryVersion); isAnimated = false; return; }

            byteIndex += sizeof(System.Byte);
            numberOfFrames = (int)System.BitConverter.ToInt32(data, byteIndex);
            byteIndex += sizeof(System.Int32);
            //			frameRate = System.BitConverter.ToSingle(data,byteIndex); // not used yet
            byteIndex += sizeof(System.Single);
            containsRGB = System.BitConverter.ToBoolean(data, byteIndex);
            byteIndex += sizeof(System.Boolean);

            numberOfPointsPerFrame = new int[numberOfFrames];

            totalPoints = 0;
            for (int i = 0; i < numberOfFrames; i++)
            {
                numberOfPointsPerFrame[i] = (int)System.BitConverter.ToInt32(data, byteIndex);
                //Debug.Log(numberOfPointsPerFrame[i]);
                byteIndex += sizeof(System.Int32);
                if (numberOfPointsPerFrame[i] > totalPoints) totalPoints = numberOfPointsPerFrame[i]; // largest value will be used as a fixed size for point array
                totalNumberOfPoints += numberOfPointsPerFrame[i];
            }

            animatedPointArray = new Vector3[totalNumberOfPoints];
            animatedColorArray = new Vector3[totalNumberOfPoints];

            frameBinaryPositionForEachFrame = new System.Int64[numberOfFrames];
            for (int i = 0; i < numberOfFrames; i++)
            {
                frameBinaryPositionForEachFrame[i] = (System.Int64)System.BitConverter.ToInt64(data, byteIndex);
                byteIndex += sizeof(System.Int64);
            }


            Debug.Log("binaryVersion:" + binaryVersion);
            Debug.Log("numberOfFrames:" + numberOfFrames);
            //Debug.Log("frameRate:"+frameRate);
            Debug.Log("containsRGB:" + containsRGB);
            Debug.Log("numberOfPointsPerFrame[0]:" + numberOfPointsPerFrame[0]);
            Debug.Log("frameBinaryPositionForEachFrame[0]:" + frameBinaryPositionForEachFrame[0]);


            points = new Vector3[totalPoints];
            animatedOffset = new int[numberOfFrames];
            int totalCounter = 0;

            if (containsRGB) pointColors = new Vector3[totalPoints];
            for (int frame = 0; frame < numberOfFrames; frame++)
            {
                animatedOffset[frame] = totalCounter;
                for (int i = 0; i < numberOfPointsPerFrame[frame]; i++)
                {
                    // X Y Z R G B (float)

                    float x = System.BitConverter.ToSingle(data, byteIndex);
                    byteIndex += sizeof(System.Single);

                    float y = System.BitConverter.ToSingle(data, byteIndex);
                    byteIndex += sizeof(System.Single);
                    float z = System.BitConverter.ToSingle(data, byteIndex);
                    byteIndex += sizeof(System.Single);

                    float r = System.BitConverter.ToSingle(data, byteIndex);
                    byteIndex += sizeof(System.Single);
                    float g = System.BitConverter.ToSingle(data, byteIndex);
                    byteIndex += sizeof(System.Single);
                    float b = System.BitConverter.ToSingle(data, byteIndex);
                    byteIndex += sizeof(System.Single);

                    animatedPointArray[totalCounter] = new Vector3(x + transform.position.x, y + transform.position.y, z + transform.position.z);
                    if (containsRGB) animatedColorArray[totalCounter] = new Vector3(r, g, b);

                    totalCounter++;
                }
            }

            Debug.Log("Finished loading animated point cloud. total points = " + totalCounter);

            InitDX11Buffers();
            isLoading = false;
        }

        public void InitDX11Buffers()
        {
            // cannot init 0 size, so create dummy data if its 0
            if (totalPoints == 0)
            {
                totalPoints = 1;
                points = new Vector3[1];
                if (containsRGB == true)
                {
                    pointColors = new Vector3[1];
                }
            }

            // clear old buffers
            ReleaseDX11Buffers();

            if (bufferPoints != null) bufferPoints.Dispose();
            bufferPoints = new ComputeBuffer(totalPoints, 12);
            bufferPoints.SetData(points);
            cloudMaterial.SetBuffer("buf_Points", bufferPoints);

            if (containsRGB == true)
            {
                if (bufferColors != null) bufferColors.Dispose();
                bufferColors = new ComputeBuffer(totalPoints, 12);
                bufferColors.SetData(pointColors);
                cloudMaterial.SetBuffer("buf_Colors", bufferColors);
            }
        }

        public void ReleaseDX11Buffers()
        {
            if (bufferPoints != null) bufferPoints.Release();
            bufferPoints = null;
            if (bufferColors != null) bufferColors.Release();
            bufferColors = null;
        }

        // can use this to set new points data
        public void UpdatePointData()
        {
            if (points.Length == bufferPoints.count)
            {
                // same length as earlier
                bufferPoints.SetData(points);
                cloudMaterial.SetBuffer("buf_Points", bufferPoints);
            } else
            {
                // new data is different sized array, need to redo it
                totalPoints = points.Length;
                bufferPoints.Dispose();
                bufferPoints = new ComputeBuffer(totalPoints, 12);
                bufferPoints.SetData(points);
                cloudMaterial.SetBuffer("buf_Points", bufferPoints);
            }
        }

        // can use this to set new point colors data
        public void UpdateColorData()
        {
            if (pointColors.Length == bufferColors.count)
            {
                // same length as earlier
                bufferColors.SetData(pointColors);
                cloudMaterial.SetBuffer("buf_Colors", bufferColors);
            } else
            {
                // new data is different sized array, need to redo it
                totalPoints = pointColors.Length;
                bufferColors.Dispose();
                bufferColors = new ComputeBuffer(totalPoints, 12);
                bufferColors.SetData(pointColors);
                cloudMaterial.SetBuffer("buf_Colors", bufferColors);
            }
        }

        bool CheckIfFileExists(string fileToRead)
        {
            return File.Exists(fileToRead);
        }


        void OnDestroy()
        {
            ReleaseDX11Buffers();
            points = new Vector3[0];
            pointColors = new Vector3[0];

            abortMeasureThread = true;
            abortReaderThread = true;
        }

        // mainloop, for drawing the points
        //void OnPostRender() // < works also, BUT must have this script attached to Camera
        void OnRenderObject()
        {
            if (isLoading == true) return; // dont display while loading, it slows down with huge clouds

            cloudMaterial.SetPass(0);
            Graphics.DrawProcedural(MeshTopology.Points, totalPoints, instanceCount);

        }

        // highlights nearest point to cursor (by drawing GLDebug.DrawSquare)
        void HighlightClosestPoint()
        {
            var ray = cam.ScreenPointToRay(Input.mousePosition);
            if (pointTree == null) return;

            var r = pointTree.GetNearby(ray, maxDistance: 0.05f);
            if (r.Length > 0)
            {
                // sort results
                int closestIndex = -1;
                float closestDistance = Mathf.Infinity;
                for (int i = 0, len = r.Length > 128 ? 128 : r.Length; i < len; i++)
                {
                    var distance = DistanceToRay(ray, points[r[i].index]);
                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestIndex = i;
                    }
                }

                // founded point, draw square around it
                if (closestIndex > -1)
                {
                    var rot = Quaternion.LookRotation(cam.transform.up);
                    // FIXME: should probably draw always same size square (screenspace..)
                    GLDebug.DrawSquare(points[r[closestIndex].index], rot, Vector3.one * 0.05f, Color.green);
                }


            }

        }

        // bruteforce point picker
        void SelectClosestPoint()
        {
            // left click for measuring
            if (Input.GetMouseButtonDown(0) == true)
            {
                if (isSearchingPoint == false)
                {
                    if (useOctTree == true && useThreading == true)
                    {
                        ThreadInfoOctTree threadInfo = new ThreadInfoOctTree();
                        Camera cam = Camera.main;
                        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                        threadInfo.startPoint = ray.origin + ray.direction * cam.nearClipPlane; // start from near clip plane
                        threadInfo.direction = ray.direction;
                        ThreadPool.QueueUserWorkItem(new WaitCallback(FindClosestPointOctTreeThreaded), threadInfo);
                        return;
                    } else // non-threaded
                    {
                        StartCoroutine(FindClosestPointBrute(Input.mousePosition));
                    }

                }
            }
        }

        // finds closest point to mouse cursor from 3D cloud
        IEnumerator FindClosestPointBrute(Vector2 mousePos) // in screen pixel coordinates
        {
            isSearchingPoint = true;

            int? closestIndex = null;
            float closestDistance = Mathf.Infinity;
            Camera cam = Camera.main;

            var offsetPixels = new Vector2(0, 32); // search area in pixels
            var farPointUp = cam.ScreenPointToRay(mousePos + offsetPixels).GetPoint(999);
            var farPointDown = cam.ScreenPointToRay(mousePos - offsetPixels).GetPoint(999);

            offsetPixels = new Vector2(32, 0);  // search area in pixels
            var farPointLeft = cam.ScreenPointToRay(mousePos - offsetPixels).GetPoint(999);
            var farPointRight = cam.ScreenPointToRay(mousePos + offsetPixels).GetPoint(999);

            var screenPos = Vector2.zero;
            float distance = Mathf.Infinity;

            // build filtering planes
            Plane forwardPlane = new Plane(cam.transform.forward, cam.transform.position);
            Plane bottomLeft = new Plane(cam.transform.position, farPointDown, farPointLeft);
            Plane topLeft = new Plane(cam.transform.position, farPointLeft, farPointUp);
            Plane topRight = new Plane(cam.transform.position, farPointUp, farPointRight);
            Plane bottomRight = new Plane(cam.transform.position, farPointRight, farPointDown);

            /*
            // display search area
            Debug.DrawLine(farPointDown,farPointLeft, Color.magenta,20);
            Debug.DrawLine(farPointLeft,farPointUp, Color.magenta,20);
            Debug.DrawLine(farPointUp,farPointRight,Color.magenta,20);
            Debug.DrawLine(farPointRight,farPointDown,Color.magenta,20);
            */

            // check all points, until find close enough hit
            var pixelThreshold = 3; // if distance is this or less, just select it

            for (int i = 0, len = points.Length; i < len; i++)
            {
                if (i % maxIterationsPerFrame == 0)
                {
                    // Pause our work here, and continue finding on the next frame
                    yield return null;
                }

                if (!forwardPlane.GetSide(points[i])) continue;
                if (topRight.GetSide(points[i])) continue;
                if (bottomRight.GetSide(points[i])) continue;
                if (bottomLeft.GetSide(points[i])) continue;
                if (topLeft.GetSide(points[i])) continue;

                screenPos = cam.WorldToScreenPoint(points[i]);

                distance = Vector2.Distance(mousePos, screenPos);
                //distance = DistanceApprox(mousePos, screenPos);

                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestIndex = i;
                    if (distance <= pixelThreshold) break; // early exit on close enough hit
                }
            }

            if (closestIndex != null)
            {
                if (PointWasSelected != null) PointWasSelected(points[(int)closestIndex]); // fire event if have listeners
                Debug.Log("PointIndex:" + ((int)closestIndex) + " pos:" + points[(int)closestIndex]);
            } else
            {
                Debug.Log("No point selected..");
            }
            isSearchingPoint = false;
        }


        // octree point picking
        void FindClosestPointOctTreeThreaded(System.Object threadData)
        {
            while (abortMeasureThread == false)
            {
                lock (_measureCountLock)
                {
                    if (_measureThreadCount < maxMeasureThreads && !abortMeasureThread)
                    {
                        _measureThreadCount++;
                        break;
                    }
                }
                Thread.Sleep(50);
            }

            if (abortMeasureThread == true) return;

            isSearchingPoint = true;

            ThreadInfoOctTree data = threadData as ThreadInfoOctTree;
            var ray = new Ray(data.startPoint, data.direction);

            int? closestIndex = null;
            float closestDistance = Mathf.Infinity;

            var r = pointTree.GetNearby(ray, pointSearchRadius);

            // sort results
            for (int i = 0, len = r.Length; i < len; i++)
            {
                var distance = DistanceToRay(ray, points[r[i].index]);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestIndex = i;
                }
            }

            if (closestIndex != null)
            {
                UnityLibrary.MainThread.Call(PointCallBack, points[r[(int)closestIndex].index]);
                Debug.Log("Selected Point #:" + (r[(int)closestIndex].index) + " Position:" + points[r[(int)closestIndex].index]);
            } else
            {
                Debug.Log("No point selected..");
            }
            isSearchingPoint = false;
            _measureThreadCount--;
        }

        // this gets called after thread finds closest point
        void PointCallBack(System.Object a)
        {
            if (PointWasSelected != null) PointWasSelected((Vector3)a);
        }

        // TEST Distance approximation by using octagons approach https://gist.github.com/aurbano/4693462
        float DistanceApprox(Vector2 p1, Vector2 p2)
        {
            var x = p2.x - p1.x;
            var y = p2.y - p1.y;
            return (float)(1.426776695 * System.Math.Min(0.7071067812 * (System.Math.Abs(x) + System.Math.Abs(y)), System.Math.Max(System.Math.Abs(x), System.Math.Abs(y))));
        }

        // displays message on screen using old GUIText, FIXME, GUI layer is getting removed..
        void ShowMessage(string msg)
        {
            GameObject go = new GameObject("ShowMessage-TempGameObject" + msg);
            go.transform.position = new Vector3(0, 0.5f, 0);
            go.AddComponent<GUIText>();
            go.GetComponent<GUIText>().text = msg;
        }

        public static float DistanceToRay(Ray ray, Vector3 point)
        {
            return Vector3.Cross(ray.direction, point - ray.origin).sqrMagnitude;
        }


        // helper methods
        // returns current point count, or -1 if points array is null
        public int GetPointCount()
        {
            if (points == null) return -1;
            return points.Length;
        }

        // returns given point position from array
        public Vector3 GetPointPosition(int index)
        {
            if (points == null || index < 0 || index > points.Length - 1) return Vector3.zero;
            return points[index];
        }

        // returns currently assigned filepath (not necessarily same as currently loaded cloud, if you have modified the path variables)
        public string GetFilePath()
        {
            return Application.dataPath + "/" + baseFolder + fileName;
        }

    } // class
} // namespace

#endif
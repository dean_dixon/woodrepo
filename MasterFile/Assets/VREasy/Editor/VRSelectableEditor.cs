﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace VREasy
{
    [CustomEditor(typeof(VRSelectable))]
    public class VRSelectableEditor : Editor
    {
        private static int actionIndex = 0;
        private static Editor _editor;
        private static List<string> actions_assemblyNames = new List<string>();
        private static List<string> actions_names = new List<string>();

        bool handleRepaintErrors = false;
        public override void OnInspectorGUI()
        {
            // Hack to prevent ArgumentException: GUILayout: Mismatched LayoutGroup.Repaint errors
            // see more: https://forum.unity3d.com/threads/unexplained-guilayout-mismatched-issue-is-it-a-unity-bug-or-a-miss-understanding.158375/
            // and: https://forum.unity3d.com/threads/solved-adding-and-removing-gui-elements-at-runtime.57295/
            if (Event.current.type == EventType.Repaint && !handleRepaintErrors)
            {
                handleRepaintErrors = true;
                return;
            }
            VRSelectable _vrButton = (VRSelectable)target;

            DisplayCommon(ref _vrButton);
        }

        public static void DisplayCommon(ref VRSelectable _vrButton)
        {
            DisplayTooltip(ref _vrButton);

            DisplayTimingOptions(ref _vrButton);

            DisplayActionList(ref _vrButton);

            DisplayAudioOptions(ref _vrButton);
        }

        public static void DisplayTooltip(ref VRSelectable selectable)
        {
            EditorGUILayout.Separator();
            EditorGUI.BeginChangeCheck();
            string tooltip = EditorGUILayout.TextField("Tooltip", selectable.tooltip);
            if(EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(selectable, "tooltip changed");
                selectable.tooltip = tooltip;
            }
        }

        public static void DisplayTimingOptions(ref VRSelectable _vrButton)
        {
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Timers", EditorStyles.boldLabel);
            EditorGUI.BeginChangeCheck();
            float coolDownTime = EditorGUILayout.FloatField("Cooldown time", _vrButton.coolDownTime);
            float deactivateTime = EditorGUILayout.FloatField("Deactivation time", _vrButton.deactivationTime);
            if(EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(_vrButton, "timers");
                _vrButton.coolDownTime = coolDownTime;
                _vrButton.deactivationTime = deactivateTime;
            }
        }

        public static void DisplayAudioOptions(ref VRSelectable _vrButton)
        {
            // audio
            EditorGUILayout.Separator();
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.LabelField("Sounds", EditorStyles.boldLabel);
            AudioClip selectSound = (AudioClip)EditorGUILayout.ObjectField("Selection sound", _vrButton.selectSound, typeof(AudioClip), true);
            AudioClip activateSound = (AudioClip)EditorGUILayout.ObjectField("Activation sound", _vrButton.activateSound, typeof(AudioClip), true);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(_vrButton, "Changed VRButton sounds");
                _vrButton.selectSound = selectSound;
                _vrButton.activateSound = activateSound;
            }
        }

        public static void DisplayActionList(ref VRSelectable _vrButton)
        {
            // action triggered
            DisplayActionList(_vrButton.actionList);
        }

        public static void DisplayActionList(ActionList actions)
        {
            // action triggered
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Actions to trigger", EditorStyles.boldLabel);
            EditorGUI.BeginChangeCheck();
            int removeActionIndex = -1;
            VRAction addActionSpecific = null;
            bool addBlankAction = false;
            for (int ii = 0; ii < actions.list.Count; ii++)
            {
                EditorGUILayout.BeginHorizontal();
                actions.list[ii] = (VRAction)EditorGUILayout.ObjectField("Action " + ii, actions.list[ii], typeof(VRAction), true);
                Handles.BeginGUI();
                if (GUILayout.Button("-"))
                {
                    removeActionIndex = ii;
                }
                Handles.EndGUI();
                EditorGUILayout.EndHorizontal();
            }
            // add actions
            EditorGUILayout.BeginHorizontal();
            VREasy_utils.LoadClassesFromAssembly(typeof(VRAction), ref actions_assemblyNames, ref actions_names, "Action");

            actionIndex = EditorGUILayout.Popup("Action type", actionIndex, actions_names.ToArray());

            Handles.BeginGUI();
            if (GUILayout.Button("Add action"))
            {
                addActionSpecific = VREasy_utils.LoadAndSetClassFromAssembly<VRAction>(actions.gameObject, actions_assemblyNames[actionIndex]); //VRAction.getAction(actions.gameObject, actionIndex);
            }
            Handles.EndGUI();
            EditorGUILayout.EndHorizontal();
            // add unspecified action
            Handles.BeginGUI();
            if (GUILayout.Button("+"))
            {
                addBlankAction = true;
            }
            Handles.EndGUI();

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(actions, "Changed multiple action properties");
                if (removeActionIndex >= 0)
                {
                    VRAction reference = actions.list[removeActionIndex];
                    actions.list.RemoveAt(removeActionIndex);
                    DestroyImmediate(reference);
                    EditorGUIUtility.ExitGUI();
                }
                if (addActionSpecific)
                    actions.list.Add(addActionSpecific);
                if (addBlankAction)
                    actions.list.Add(null);
            }
            GUI.contentColor = Color.yellow;
            EditorGUILayout.LabelField("Actions are placed in gameobject: " + actions.gameObject.name);
            GUI.contentColor = Color.white;
        }

    }
}
﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace VREasy
{
    [CustomEditor(typeof(SteamControllerGrab))]
    public class SteamControllerGrabEditor : Editor
    {
        bool handleRepaintErrors = false;
        public override void OnInspectorGUI()
        {
            //SteamControllerGrab steamGrab = (SteamControllerGrab)target;
            // Hack to prevent ArgumentException: GUILayout: Mismatched LayoutGroup.Repaint errors
            // see more: https://forum.unity3d.com/threads/unexplained-guilayout-mismatched-issue-is-it-a-unity-bug-or-a-miss-understanding.158375/
            // and: https://forum.unity3d.com/threads/solved-adding-and-removing-gui-elements-at-runtime.57295/
            if (Event.current.type == EventType.Repaint && !handleRepaintErrors)
            {
                handleRepaintErrors = true;
                return;
            }

#if VREASY_STEAM_SDK
            base.OnInspectorGUI();
#else
            EditorStyles.label.wordWrap = true;
            GUI.contentColor = Color.yellow;
            EditorGUILayout.LabelField("Steam SDK not found or not activated. Please make sure the Steam SDK is imported and you have activated it via the VREasy/SDK Selector GUI");
            GUI.contentColor = Color.white;
#endif
        }
    }
}
﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace VREasy
{
    [CustomEditor(typeof(VRSimpleFPSLocomotion))]
    public class VRSimpleFPSLocomotionEditor : Editor
    {
        bool handleRepaintErrors = false;
        public override void OnInspectorGUI()
        {
            // Hack to prevent ArgumentException: GUILayout: Mismatched LayoutGroup.Repaint errors
            // see more: https://forum.unity3d.com/threads/unexplained-guilayout-mismatched-issue-is-it-a-unity-bug-or-a-miss-understanding.158375/
            // and: https://forum.unity3d.com/threads/solved-adding-and-removing-gui-elements-at-runtime.57295/
            if (Event.current.type == EventType.Repaint && !handleRepaintErrors)
            {
                handleRepaintErrors = true;
                return;
            }
            VRSimpleFPSLocomotion locomotion = (VRSimpleFPSLocomotion)target;

            EditorGUI.BeginChangeCheck();
            float speed = EditorGUILayout.FloatField("Speed", locomotion.speed);
            Transform head = (Transform)EditorGUILayout.ObjectField("Head", locomotion.head, typeof(Transform), true);
            bool fixedMovement = EditorGUILayout.Toggle("Fixed height", locomotion.fixedHeight);
            if(EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(locomotion,"Changed locomotion parameters");
                locomotion.head = head;
                locomotion.speed = speed;
                locomotion.fixedHeight = fixedMovement;
            }
            EditorGUILayout.Separator();

            locomotion.input = (VRLOCOMOTION_INPUT)EditorGUILayout.EnumPopup("Input type", locomotion.input);
            switch (locomotion.input)
            {
                case VRLOCOMOTION_INPUT.UNITY_INPUT:
                    EditorGUILayout.LabelField("Movement input based on Horizontal and Vertical axis from the Input System", EditorStyles.wordWrappedLabel);
                    break;
                case VRLOCOMOTION_INPUT.STEAM_CONTROLLER:
                    {
#if VREASY_STEAM_SDK
                        EditorGUILayout.LabelField("Movement input using Steam Controller's D-pad", EditorStyles.wordWrappedLabel);
                        locomotion.trackedObject = (SteamVR_TrackedObject)EditorGUILayout.ObjectField("Tracked controller", locomotion.trackedObject, typeof(SteamVR_TrackedObject), true);
#else
                        GUI.contentColor = Color.red;
                        EditorGUILayout.LabelField("Import Steam SDK and activate Steam SDK from the VREasy SDK helper window",EditorStyles.wordWrappedLabel);
                        GUI.contentColor = Color.white;
#endif
                    }
                    break;
                case VRLOCOMOTION_INPUT.MOBILE_TILT:
                    {
                        locomotion.forwardAngle = EditorGUILayout.FloatField("Start movement tilt angle", locomotion.forwardAngle);
                        EditorGUILayout.LabelField("Movement based on HMD horizontal tilt, look down to start moving, look up to stop", EditorStyles.wordWrappedLabel);
                    }
                    break;
                case VRLOCOMOTION_INPUT.TRIGGER:
                    GameObject obj = locomotion.gameObject;
                    VRGrabTrigger.DisplayGrabTriggerSelector(ref locomotion.trigger, ref obj);
                    break;
            }
        }
    }
}
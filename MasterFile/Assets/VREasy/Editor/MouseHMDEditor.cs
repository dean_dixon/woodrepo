﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace VREasy
{
    [CustomEditor(typeof(MouseHMD))]
    public class MouseHMDEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            if (PlayerSettings.virtualRealitySupported)
            {
                GUI.contentColor = new Color(1f,0.3f,0.2f);
                EditorGUILayout.LabelField("To be able to control the camera with the mouse, the Virtual Reality Supported option must be disabled in Player Settings", EditorStyles.wordWrappedLabel);
                GUI.contentColor = Color.white;
                
            }
            PlayerSettings.virtualRealitySupported = EditorGUILayout.Toggle("VR Supported", PlayerSettings.virtualRealitySupported);
            DrawDefaultInspector();
        }

    }
}
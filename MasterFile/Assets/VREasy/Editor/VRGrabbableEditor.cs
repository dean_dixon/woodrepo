﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace VREasy
{
    [CustomEditor(typeof(VRGrabbable))]
    public class VRGrabbableEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            VRGrabbable grabbable = (VRGrabbable)target;

            EditorGUILayout.Separator();
            EditorGUI.BeginChangeCheck();
            Color grabColour = EditorGUILayout.ColorField("Grab highlight", grabbable.grabColour);
            GRAB_TYPE type = (GRAB_TYPE)EditorGUILayout.EnumPopup("Grab type", grabbable.type);

            if(EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(grabbable, "Grab options");
                grabbable.type = type;
                grabbable.grabColour = grabColour;
            }

            EditorGUILayout.Separator();
            switch(grabbable.type)
            {
                case GRAB_TYPE.DRAG:
                    {
                        EditorGUI.BeginChangeCheck();
                        bool align = EditorGUILayout.Toggle("Aligned when grabbed", grabbable.alignWithPivot);
                        if (EditorGUI.EndChangeCheck())
                        {
                            Undo.RecordObject(grabbable, "grab align changed");
                            grabbable.alignWithPivot = align;
                        }
                    }
                        break;
                case GRAB_TYPE.SLIDE:
                    {
                        EditorGUILayout.LabelField("Moving axis", EditorStyles.boldLabel);
                        EditorGUI.BeginChangeCheck();
                        bool moveX = EditorGUILayout.Toggle("Along X", grabbable.moveXAxis);
                        bool moveY = EditorGUILayout.Toggle("Along Y", grabbable.moveYAxis);
                        bool moveZ = EditorGUILayout.Toggle("Along Z", grabbable.moveZAxis);
                        if(EditorGUI.EndChangeCheck()) {
                            Undo.RecordObject(grabbable, "Grab axis changed");
                            grabbable.moveXAxis = moveX;
                            grabbable.moveYAxis = moveY;
                            grabbable.moveZAxis = moveZ;
                        }

                    }
                    break;
            }
            EditorGUILayout.Separator();
            EditorGUI.BeginChangeCheck();
            bool snap = EditorGUILayout.Toggle("Snap to origin", grabbable.snapToOrigin);
            float radius = grabbable.snapRadius;
            if (snap)
            {
                radius = EditorGUILayout.FloatField("Snap radius", grabbable.snapRadius);
            }
            if(EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(grabbable, "Snap options");
                if (grabbable.snapToOrigin != snap)
                {
                    grabbable.snapToOrigin = snap;
                    SceneView.RepaintAll();
                }
                if (Mathf.Abs(radius - grabbable.snapRadius) > 0.001f)
                {
                    grabbable.snapRadius = radius;
                    SceneView.RepaintAll();
                }
            }
            if (!Application.isPlaying)
            {
                grabbable.SetOrigins();
            }

        }
    }
}
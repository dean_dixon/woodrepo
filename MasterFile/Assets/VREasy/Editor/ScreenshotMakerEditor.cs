﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace VREasy
{
    [CustomEditor(typeof(ScreenshotMaker))]
    public class ScreenshotMakerEditor : Editor
    {

        bool handleRepaintErrors = false;

        public override void OnInspectorGUI()
        {
            // Hack to prevent ArgumentException: GUILayout: Mismatched LayoutGroup.Repaint errors
            // see more: https://forum.unity3d.com/threads/unexplained-guilayout-mismatched-issue-is-it-a-unity-bug-or-a-miss-understanding.158375/
            // and: https://forum.unity3d.com/threads/solved-adding-and-removing-gui-elements-at-runtime.57295/
            if (Event.current.type == EventType.Repaint && !handleRepaintErrors)
            {
                handleRepaintErrors = true;
                return;
            }

            ScreenshotMaker screenshotmaker = (ScreenshotMaker)target;

            EditorGUI.BeginChangeCheck();

            string filename = EditorGUILayout.TextField("Filename", screenshotmaker.filename);

            GameObject gm = screenshotmaker.gameObject;
            VRGrabTrigger.DisplayGrabTriggerSelector(ref screenshotmaker.trigger, ref gm);

            EditorGUILayout.Separator();
            int superSize = EditorGUILayout.IntSlider("Resolution multiplier", screenshotmaker.screenshotMultiplier,1,10);

            if(EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(screenshotmaker, "screenshot maker options");
                screenshotmaker.filename = filename;
                screenshotmaker.screenshotMultiplier = superSize;
            }

        }
    }
}
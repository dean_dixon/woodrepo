﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

namespace VREasy
{
    [CustomEditor(typeof(PointerSelector))]
    public class PointerSelectorEditor : Editor
    {
        bool handleRepaintErrors = false;

        public override void OnInspectorGUI()
        {
            // Hack to prevent ArgumentException: GUILayout: Mismatched LayoutGroup.Repaint errors
            // see more: https://forum.unity3d.com/threads/unexplained-guilayout-mismatched-issue-is-it-a-unity-bug-or-a-miss-understanding.158375/
            // and: https://forum.unity3d.com/threads/solved-adding-and-removing-gui-elements-at-runtime.57295/
            if (Event.current.type == EventType.Repaint && !handleRepaintErrors)
            {
                handleRepaintErrors = true;
                return;
            }

            PointerSelector pointer = (PointerSelector)target;

            ConfigurePointerSelector(pointer);
        }

        public static void ConfigurePointerSelector(PointerSelector pointer)
        {
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Line options",EditorStyles.boldLabel);
            LineRenderer line = pointer.GetComponent<LineRenderer>();
            if(line.sharedMaterial == null)
            {
                EditorGUILayout.LabelField("Please assign a material to the Line Renderer before changing its properties");
            } else
            {
                EditorGUI.BeginChangeCheck();
                Color col = EditorGUILayout.ColorField("Line colour", line.sharedMaterial.color);
                float width = EditorGUILayout.Slider("Line width", pointer.LineWidth,0.001f,0.1f);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(pointer, "Pointer renderer options");
                    line.sharedMaterial.color = col;
                    pointer.LineWidth = width;
                }
            }
            
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Activation", EditorStyles.boldLabel);

            VRSelector sel = pointer;
            VRSelectorEditor.ConfigureSelector(ref sel);

            EditorGUI.BeginChangeCheck();
            float selectionDistance = EditorGUILayout.DelayedFloatField("Selection distance", pointer.selectionDistance);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(pointer, "Pointer selector settings");
                pointer.selectionDistance = selectionDistance;
            }

            GameObject obj = pointer.gameObject;
            VRGrabTrigger.DisplayGrabTriggerSelector(ref pointer.grabTrigger, ref obj);
        }
    }
}
﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace VREasy
{
    [CustomEditor(typeof(VRDisplayButton))]
    public class VRDisplayButtonEditor : Editor
    {
        bool handleRepaintErrors = false;
        public override void OnInspectorGUI()
        {
            // Hack to prevent ArgumentException: GUILayout: Mismatched LayoutGroup.Repaint errors
            // see more: https://forum.unity3d.com/threads/unexplained-guilayout-mismatched-issue-is-it-a-unity-bug-or-a-miss-understanding.158375/
            // and: https://forum.unity3d.com/threads/solved-adding-and-removing-gui-elements-at-runtime.57295/
            if (Event.current.type == EventType.Repaint && !handleRepaintErrors)
            {
                handleRepaintErrors = true;
                return;
            }
            VRDisplayButton displayButton = (VRDisplayButton)target;

            ConfigureDisplayButton(ref displayButton);
        }

        public static void ConfigureDisplayButton(ref VRDisplayButton displayButton)
        {
            VR2DButton vrButton = displayButton;

            VRSelectable selectable = displayButton;
            VRSelectableEditor.DisplayTooltip(ref selectable);

            VR2DButtonEditor.displayGraphicalRepresentation(ref vrButton,true,true,false);
            EditorGUILayout.Separator();

            VR2DButtonEditor.displayTypeAndFaceDirection(ref vrButton);
            EditorGUILayout.Separator();

            // display representations for each switch object
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Swappable - Icons", EditorStyles.boldLabel);
            EditorStyles.label.wordWrap = true;
            EditorGUILayout.LabelField("Each swappable object in the Switch action is represented with a different icon");

            EditorGUILayout.Separator();


            displayButton.SetRepresentationLength(displayButton.Action.swapObjects.Count);
            if (displayButton.Action.swapObjects.Count > 0)
            {
                EditorGUILayout.BeginHorizontal();
                if (displayButton.Action.Target == null)
                    EditorGUILayout.LabelField("Target: " + displayButton.Action.Target);
                else
                    EditorGUILayout.LabelField("Target: " + displayButton.Action.Target.name);
                EditorGUILayout.LabelField("Icon");
                EditorGUILayout.EndHorizontal();
            } else
            {
                GUI.contentColor = Color.yellow;
                EditorGUILayout.LabelField("Add swappable slots via the SwitchAction script");
                GUI.contentColor = Color.white;
            }
            for (int ii = 0; ii < displayButton.Action.swapObjects.Count; ii++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("" + (ii + 1));

                // Swappables
                if(displayButton.Action.swapObjects[ii] == null)
                {
                    EditorGUILayout.LabelField("Not assigned");
                } else
                {
                    EditorGUILayout.LabelField(displayButton.Action.swapObjects[ii].name);
                }

                // Representation
                displayButton.representations[ii] = (Sprite)EditorGUILayout.ObjectField(displayButton.representations[ii], typeof(Sprite), true);
                EditorGUILayout.EndHorizontal();
            }

            
            
        }
    }
}
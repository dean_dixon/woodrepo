﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace VREasy
{
    [CustomEditor(typeof(SightSelector))]
    public class SightSelectorEditor : Editor
    {
        bool handleRepaintErrors = false;
        public override void OnInspectorGUI()
        {
            // Hack to prevent ArgumentException: GUILayout: Mismatched LayoutGroup.Repaint errors
            // see more: https://forum.unity3d.com/threads/unexplained-guilayout-mismatched-issue-is-it-a-unity-bug-or-a-miss-understanding.158375/
            // and: https://forum.unity3d.com/threads/solved-adding-and-removing-gui-elements-at-runtime.57295/
            if (Event.current.type == EventType.Repaint && !handleRepaintErrors)
            {
                handleRepaintErrors = true;
                return;
            }
            SightSelector selector = (SightSelector)target;

            ConfigureSightSelector(selector);
        }

        public static void ConfigureSightSelector(SightSelector selector)
        {
            VRSelector sel = selector;
            VRSelectorEditor.ConfigureSelector(ref sel);

            EditorGUI.BeginChangeCheck();
            float selectionDistance = EditorGUILayout.DelayedFloatField("Selection distance", selector.selectionDistance);
            bool useCrosshair = EditorGUILayout.Toggle("Use crosshair", selector.useCrosshair);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(selector, "Changed sight selector properties distance");
                selector.selectionDistance = selectionDistance;
                selector.useCrosshair = useCrosshair;
            }
            selector.reconfigureCrosshair();
            if (selector.useCrosshair)
            {
                // display different options based on the crosshair type
                EditorGUI.BeginChangeCheck();
                CROSSHAIR_TYPE type = (CROSSHAIR_TYPE)EditorGUILayout.EnumPopup("Type", selector.crosshairType);
                if(EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(selector, "Crosshair type");
                    selector.crosshairType = type;
                }
                // common
                EditorGUI.BeginChangeCheck();
                Sprite crosshairSprite = (Sprite)EditorGUILayout.ObjectField("Active sprite", selector.CrosshairSprite, typeof(Sprite), true);
                float crosshairSize = selector.CrosshairSize;
                if(crosshairSprite) crosshairSize = EditorGUILayout.Slider("Crosshair size", selector.CrosshairSize, 0, 1);

                // specific
                switch (selector.crosshairType)
                {
                    case CROSSHAIR_TYPE.SINGLE_SPRITE:
                        {
                            Color crosshairActiveColour = selector.CrosshairActiveColour;
                            Color crosshairIdleColour = selector.CrosshairIdleColour;
                            if (crosshairSprite)
                            {
                                crosshairActiveColour = EditorGUILayout.ColorField("Active colour", selector.CrosshairActiveColour);
                                crosshairIdleColour = EditorGUILayout.ColorField("Idle colour", selector.CrosshairIdleColour);
                            }
                            if (EditorGUI.EndChangeCheck())
                            {
                                Undo.RecordObject(selector, "Changed crosshair properties");
                                selector.CrosshairSprite = crosshairSprite;
                                selector.CrosshairSize = crosshairSize;
                                selector.CrosshairActiveColour = crosshairActiveColour;
                                selector.CrosshairIdleColour = crosshairIdleColour;
                            }
                        }
                        break;
                    case CROSSHAIR_TYPE.DUAL_SPRITE:
                        {
                            Sprite idleSprite = (Sprite)EditorGUILayout.ObjectField("Idle sprite", selector.idleSprite, typeof(Sprite), true);

                            if (EditorGUI.EndChangeCheck())
                            {
                                Undo.RecordObject(selector, "Changed crosshair properties");
                                selector.idleSprite = idleSprite;
                                selector.CrosshairSprite = crosshairSprite;
                                selector.CrosshairSize = crosshairSize;
                            }
                        }
                        break;
                }
                
            }
        }
    }
}
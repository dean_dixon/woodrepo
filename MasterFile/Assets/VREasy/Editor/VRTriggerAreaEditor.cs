﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace VREasy
{
    [CustomEditor(typeof(VRTriggerArea))]
    public class VRTriggerAreaEditor : Editor
    {
        bool handleRepaintErrors = false;
        public override void OnInspectorGUI()
        {
            // Hack to prevent ArgumentException: GUILayout: Mismatched LayoutGroup.Repaint errors
            // see more: https://forum.unity3d.com/threads/unexplained-guilayout-mismatched-issue-is-it-a-unity-bug-or-a-miss-understanding.158375/
            // and: https://forum.unity3d.com/threads/solved-adding-and-removing-gui-elements-at-runtime.57295/
            if (Event.current.type == EventType.Repaint && !handleRepaintErrors)
            {
                handleRepaintErrors = true;
                return;
            }
            VRTriggerArea trigger = (VRTriggerArea)target;

            ConfigureAreaTrigger(ref trigger);

            EditorGUILayout.Separator();
            TouchSelector touch = FindObjectOfType<TouchSelector>();
            if(touch == null)
            {
                EditorStyles.label.wordWrap = true;
                GUI.contentColor = Color.yellow;
                EditorGUILayout.LabelField("Current scene does not contain an instance of TouchSelector. This object is needed to activate trigger areas");
                GUI.contentColor = Color.white;
            }
        }

        public static void ConfigureAreaTrigger(ref VRTriggerArea trigger)
        {
            EditorGUILayout.LabelField("[On enter]", EditorStyles.boldLabel);
            VRSelectable selectable = trigger;
            VRSelectableEditor.DisplayActionList(trigger.EntryActions);

            // display exit trigger actionlist
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("[On Exit]", EditorStyles.boldLabel);
            VRSelectableEditor.DisplayActionList(trigger.ExitActions);
            EditorGUILayout.Separator();

            VRSelectableEditor.DisplayAudioOptions(ref selectable);
        }
    }
}
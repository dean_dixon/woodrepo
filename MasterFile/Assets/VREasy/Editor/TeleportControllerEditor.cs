﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace VREasy
{
    [CustomEditor(typeof(TeleportController))]
    public class TeleportControllerEditor : Editor
    {
        bool handleRepaintErrors = false;
        public override void OnInspectorGUI()
        {
            // Hack to prevent ArgumentException: GUILayout: Mismatched LayoutGroup.Repaint errors
            // see more: https://forum.unity3d.com/threads/unexplained-guilayout-mismatched-issue-is-it-a-unity-bug-or-a-miss-understanding.158375/
            // and: https://forum.unity3d.com/threads/solved-adding-and-removing-gui-elements-at-runtime.57295/
            if (Event.current.type == EventType.Repaint && !handleRepaintErrors)
            {
                handleRepaintErrors = true;
                return;
            }

            TeleportController teleport = (TeleportController)target;

            TeleportActionEditor.ConfigureTeleportAction(teleport.Teleport);

            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Controller settings", EditorStyles.boldLabel);
            EditorGUI.BeginChangeCheck();
            LayerMask layers = EditorGUILayout.LayerField("Walkable layers", teleport.walkableLayers);
            //float fader = EditorGUILayout.FloatField("Fading time", teleport.fadeTimer);
            //Transform hmd = (Transform)EditorGUILayout.ObjectField("HMD parent object", teleport.HMD, typeof(Transform), true);
            GameObject obj = teleport.gameObject;
            VRGrabTrigger.DisplayGrabTriggerSelector(ref teleport.trigger, ref obj);
            EditorGUILayout.LabelField("Configure trigger in [" + teleport.name + "]'s inspector");

            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Line settings", EditorStyles.boldLabel);
            Color validColour = EditorGUILayout.ColorField("Valid dest colour", teleport.validMoveColour);
            Color invalidColour = EditorGUILayout.ColorField("Invalid dest colour", teleport.invalidMoveColour);
            float thickness = EditorGUILayout.FloatField("Line thickness", teleport.LineThickness);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(teleport, "Teleport settings changed");
                teleport.walkableLayers = layers;
                teleport.validMoveColour = validColour;
                teleport.invalidMoveColour = invalidColour;
                //teleport.fadeTimer = fader;
                teleport.LineThickness = thickness;
                //teleport.HMD = hmd;
            }
            
        }
    }
}
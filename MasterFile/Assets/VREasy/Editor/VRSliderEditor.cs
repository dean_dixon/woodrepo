﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Reflection;
using System.Collections.Generic;

namespace VREasy
{
    [CustomEditor(typeof(VRSlider))]
    public class VRSliderEditor : Editor
    {

        private static GameObject sliderTarget;

        private static List<Component> components_list = new List<Component>();
        private static List<string> componentNames_list = new List<string>();
        private static List<string> properties = new List<string>();

        private static int componentIndex = 0;
        private static int propertyIndex = 0;

        bool handleRepaintErrors = false;
        public override void OnInspectorGUI()
        {
            // Hack to prevent ArgumentException: GUILayout: Mismatched LayoutGroup.Repaint errors
            // see more: https://forum.unity3d.com/threads/unexplained-guilayout-mismatched-issue-is-it-a-unity-bug-or-a-miss-understanding.158375/
            // and: https://forum.unity3d.com/threads/solved-adding-and-removing-gui-elements-at-runtime.57295/
            if (Event.current.type == EventType.Repaint && !handleRepaintErrors)
            {
                handleRepaintErrors = true;
                return;
            }
            VRSlider slider = (VRSlider)target;

            ConfigureSlider(ref slider);
        }

        public static void ConfigureSlider(ref VRSlider slider)
        {
            float min = EditorGUILayout.FloatField("Min value", slider.min);
            float max = EditorGUILayout.FloatField("Max value", slider.max);
            bool valid = min <= max;
            slider.min = valid ? min : slider.min;
            slider.max = valid ? max : slider.max;

            float value = EditorGUILayout.Slider("Slider value (%)", slider.value, 0, 1);
            slider.SetValue(value);
            EditorGUILayout.LabelField("Actual value: " + slider.GetRealValue());

            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Broadcast target", EditorStyles.boldLabel);
            if (slider.targetComponent == null || string.IsNullOrEmpty(slider.targetProperty))
            {
                EditorGUILayout.LabelField("No broadcast target linked");
            }
            else
            {
                EditorGUILayout.LabelField("Target component: " + slider.targetComponent);
                EditorGUILayout.LabelField("Target property: " + slider.targetProperty);
            }
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Modify linked target");
            GameObject st = (GameObject)EditorGUILayout.ObjectField("Target object", sliderTarget, typeof(GameObject), true);

            if (st == null)
            {
                components_list.Clear();
                componentNames_list.Clear();
                properties.Clear();
                return;
            }

            if (sliderTarget != st)
            {
                sliderTarget = st;
                VREasy_utils.LoadComponents(sliderTarget, ref components_list, ref componentNames_list);
                componentIndex = -1;
            }

            int ci = EditorGUILayout.Popup("Component", componentIndex > 0 ? componentIndex : 0, componentNames_list.ToArray());
            if (ci != componentIndex)
            {
                componentIndex = ci;
                VREasy_utils.LoadPropertiesFromComponent(components_list[componentIndex], ref properties);
                propertyIndex = 0;
            }
            if (properties.Count > 0)
            {
                propertyIndex = EditorGUILayout.Popup("Target property", propertyIndex, properties.ToArray());
                Handles.BeginGUI();
                if (GUILayout.Button("Link target"))
                {
                    slider.targetComponent = components_list[componentIndex];
                    slider.targetProperty = properties[propertyIndex];
                    sliderTarget = null;
                }
                Handles.EndGUI();
            }
        }

        

    }
}
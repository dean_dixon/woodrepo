﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VREasy
{
    [RequireComponent(typeof(TeleportAction))]
    public class TeleportController : MonoBehaviour
    {
        public VRGrabTrigger trigger;
        public LayerMask walkableLayers;
        public Color validMoveColour = Color.blue;
        public Color invalidMoveColour = Color.red;

        private Transform teleportArea;
        
        
        public LineRenderer Parabola {
            get
            {
                if (parabola == null)
                {
                    GameObject l = new GameObject("[VREasy]TeleportControllerLine");
                    l.transform.parent = transform;
                    l.transform.localPosition = Vector3.zero;
                    l.transform.localRotation = Quaternion.identity;
                    parabola = l.AddComponent<LineRenderer>();
                    parabola.sharedMaterial = Resources.Load<Material>("TeleportPointer") as Material;
#if UNITY_5_4 || UNITY_5_5
                    parabola.SetWidth(0.05f, 0.05f);
#else
                    parabola.startWidth = 0.05f;
                    parabola.endWidth = 0.05f;
#endif
                    parabola.receiveShadows = false;
                    parabola.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                }
                return parabola;
            }
            set
            {
                parabola = value;
            }
        }
        public LineRenderer parabola;

        public float LineThickness
        {
            get
            {
                return _lineThickness;
            }
            set
            {
                _lineThickness = value;
#if UNITY_5_4 || UNITY_5_5
                Parabola.SetWidth(_lineThickness, _lineThickness);
#else
                Parabola.startWidth = _lineThickness;
                Parabola.endWidth = _lineThickness;
#endif

            }
        }
        public float _lineThickness = 0.2f;

        public TeleportAction Teleport
        {
            get
            {
                if(teleportAction == null)
                {
                    teleportAction = GetComponent<TeleportAction>();
                }
                if(teleportAction == null)
                {
                    teleportAction = gameObject.AddComponent<TeleportAction>(); // required for backwards compatibility (1.1 did not have TeleportAction as RequiredComponent)
                }
                return teleportAction;
            }
        }
        private TeleportAction teleportAction;

        private bool hasFuturePosition = false;
        private Vector3 futurePosition = Vector3.zero;
        private List<Vector3> positions = new List<Vector3>();
        private float maxLength = 100f;

        private Vector3 prevOrigin, prevForward;

        private float power = 10;

        private void Start()
        {
            teleportArea = transform.parent.Find("TeleportAreaHighlighter");
            prevForward = transform.forward;
        }

        void Update()
        {
            trigger.GetComponent<SteamControllerGrab>().checkUp = false;

            if(trigger != null && trigger.Triggered())
            {
                // Shoot parabole and detect future position
                hasFuturePosition = createAndDetectParabole();
                Parabola.material.color = hasFuturePosition ? validMoveColour : invalidMoveColour;
                parabola.material.mainTextureOffset = new Vector2(parabola.material.mainTextureOffset.x + -4*Time.deltaTime, 0);

                if (hasFuturePosition)
                {
                    parabola.SetColors(new Color(0,.33f,1), new Color(0, .33f, 1));
                }
                else
                {
                    parabola.SetColors(Color.red, Color.red);
                }

            } else if(hasFuturePosition)
            {
                // if future destination is set, teleport
                //StartCoroutine(teleport());
                teleport();
                parabola.material.mainTextureOffset = new Vector2(0, 0);
            } else
            {

#if UNITY_5_4 || UNITY_5_5
                Parabola.SetVertexCount(0);
#else
                Parabola.positionCount = 0;
#endif

            }

            //prevOrigin = Vectortransform.position;
            //Vector3 prevPoint = origin;
            prevForward = Vector3.Lerp(prevForward, transform.forward, .15f);
        }

        private Vector3 PlotTrajectoryAtTime(Vector3 start, Vector3 startVelocity, float time)
        {
            return start + startVelocity * time + Physics.gravity * time * time * 0.5f;
        }

        private void PlotTrajectory(Vector3 start, Vector3 startVelocity, float timestep, float maxTime)
        {
            Vector3 prev = start;
            for (int i = 1; ; i++)
            {
                float t = timestep * i;
                if (t > maxTime) break;
                Vector3 pos = PlotTrajectoryAtTime(start, startVelocity, t);
                RaycastHit hit;
                //if (Physics.Raycast(prev, pos - prev, out hit, Mathf.Abs(Vector3.Distance(pos, prev)) + .5f, 1 << walkableLayers))
                //{

                //}

                Debug.DrawLine(prev, pos, Color.red);
                prev = pos;
            }
        }

        private bool createAndDetectParabole()
        {
            if(Time.frameCount % 2 != 0) return hasFuturePosition;
            float angle = Mathf.Clamp(360 - transform.eulerAngles.x,-180f,89.999f);
            //return StartTrajectory(power, 360 - transform.eulerAngles.x);
            return StartTrajectory(angle/3.5f, angle);
            //return StartTrajectory(angle / 4.5f, angle);
        }

        // AIM HINT
        public bool StartTrajectory(float v, float angle) {
            hasFuturePosition = false;
            //float mass = 1f;
		    //float v = force / mass;
		    float g = Mathf.Abs(13f);
		    float a = Mathf.Deg2Rad* angle;
            //float maxDistance = v* Mathf.Cos(a)/g* (v* Mathf.Sin(a) + Mathf.Sqrt(Mathf.Pow(v* Mathf.Sin(a),2) + 2*g* transform.position.y));
		    
            Vector3 origin = transform.position;
		    Vector3 prevPoint = origin;
            Vector3 forward = prevForward;
            forward.y = 0;
            forward.Normalize();
		    float step = 1f;
            float count = 0f;
            float x, y;
            positions.Clear();

            float a_tan = Mathf.Tan(a);

            float pow = (2 * Mathf.Pow(v * Mathf.Cos(a), 2));

            do {
                Vector3 currentPoint = (origin + forward* count);
                x = count;
			    y = x* a_tan - g* Mathf.Pow(x,2) / pow;
			    currentPoint.y += y;
                // add currentPoint to LineRenderer
                
                // raycast from prevPoint to currentPoint and stop if found collider

                Vector3 dir = (currentPoint - prevPoint);
                dir.Normalize();

                RaycastHit hit;
                if (Physics.Raycast(prevPoint - dir, (currentPoint - prevPoint), out hit, Vector3.Distance(currentPoint,prevPoint) + dir.magnitude, 1 << walkableLayers))
                {
                    futurePosition = hit.point;
                    teleportArea.position = hit.point;
                    hasFuturePosition = true;

                    if(!teleportArea.gameObject.active)
                    {
                        teleportArea.gameObject.SetActive(true);
                    }

                    positions.Add(hit.point);

                    break;
                }
                else
                {
                    positions.Add(currentPoint);
                    if (teleportArea.gameObject.active)
                    {
                        teleportArea.gameObject.SetActive(false);
                    }
                }
                count +=step;
			    prevPoint = currentPoint;
		    } while(count <= maxLength);

#if UNITY_5_4 || UNITY_5_5
            Parabola.SetVertexCount(positions.Count);
#else
            Parabola.positionCount =positions.Count;
#endif

            Parabola.SetPositions(positions.ToArray());

            return hasFuturePosition;
		
		
	    }

        private void teleport()
        {
            // reset
            hasFuturePosition = false;
            
            Teleport.teleport(futurePosition);

            if (teleportArea.gameObject.active)
            {
                teleportArea.gameObject.SetActive(false);
            }

            /*if (HMD != null)
            {
                _loadSceneManager.FadeOut(fadeTimer);
                yield return new WaitForSeconds(fadeTimer*1.1f);
                futurePosition.y = HMD.position.y;
                HMD.position = futurePosition;
                _loadSceneManager.FadeIn(fadeTimer);
            }*/

        }

    }
}
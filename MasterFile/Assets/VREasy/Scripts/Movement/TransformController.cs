﻿using UnityEngine;
using System.Collections;

namespace VREasy
{
    public class TransformController : MonoBehaviour
    {

        public float X_Position
        {
            set
            {
                position = transform.position;
                position.x = value;
                transform.position = position;
            }
            get
            {
                return transform.position.x;
            }
        }
        public float Y_Position
        {
            set
            {
                position = transform.position;
                position.y = value;
                transform.position = position;
            }
            get
            {
                return transform.position.y;
            }
        }
        public float Z_Position
        {
            set
            {
                position = transform.position;
                position.z = value;
                transform.position = position;
            }
            get
            {
                return transform.position.z;
            }
        }

        public float X_Rotation
        {
            set
            {
                rotation = transform.eulerAngles;
                rotation.x = value;
                transform.eulerAngles = rotation;
            }
            get
            {
                return transform.eulerAngles.x;
            }
        }
        public float Y_Rotation
        {
            set
            {
                rotation = transform.eulerAngles;
                rotation.y = value;
                transform.eulerAngles = rotation;
            }
            get
            {
                return transform.eulerAngles.y;
            }
        }
        public float Z_Rotation
        {
            set
            {
                rotation = transform.eulerAngles;
                rotation.z = value;
                transform.eulerAngles = rotation;
            }
            get
            {
                return transform.eulerAngles.z;
            }
        }
        public float X_Scale
        {
            set
            {
                scale = transform.localScale;
                scale.x = value;
                transform.localScale = scale;
            }
            get
            {
                return transform.localScale.x;
            }
        }

        public float Added_Y_Rotation
        {
            set
            {
                sliderChange = value - sliderValue;
                sliderValue = value;
            }
            get
            {
                return sliderValue;
            }
        }


        private Vector3 position;
        private Vector3 rotation;
        private Vector3 scale;
        private bool isRotating = false;
        private float sliderChange;
        [HideInInspector]
        public float sliderValue;

        public GameObject myPrefab;

        public bool xAxis, yAxis, zAxis, isValve, isPump, isROVType;

        public float outlineWidth;

        public GameObject myValve;

        public AudioClip onSound, offSound;

		private void Start()
        {
            if(outlineWidth < .1f)
            {
                outlineWidth = .1f;
            }
        }

        private void Update()
        {
            if(xAxis)
            {
                float zRot = transform.eulerAngles.x + sliderChange;
                transform.eulerAngles = new Vector3(zRot, transform.eulerAngles.y, transform.eulerAngles.z);
            }
            else if(yAxis)
            {
                float zRot = transform.eulerAngles.y + sliderChange;
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, zRot, transform.eulerAngles.z);
            }
            else if(zAxis)
            {
                float zRot = transform.eulerAngles.z + sliderChange;
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, zRot);
            }

            sliderChange = 0;
        }
    }
}
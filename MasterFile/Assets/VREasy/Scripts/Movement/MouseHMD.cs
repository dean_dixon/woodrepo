﻿using UnityEngine;
using System.Collections;

namespace VREasy
{
    [ExecuteInEditMode]
    public class MouseHMD : MonoBehaviour
    {
        public float speed = 2.0f;
        public bool inverseY = false;

        // Update is called once per frame
        void Update()
        {
            transform.Rotate(Vector3.right, Input.GetAxis("Mouse Y") * (inverseY ? 1 : -1) * speed,Space.Self);
            transform.Rotate(Vector3.up, Input.GetAxis("Mouse X") * speed, Space.World);
        }

    }


}
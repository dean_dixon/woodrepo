﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace VREasy
{

    public enum CROSSHAIR_TYPE
    {
        SINGLE_SPRITE,
        DUAL_SPRITE
    }
    
    public enum VRELEMENT_TYPE
    {
        BUTTON_2D,
        BUTTON_3D,
        DISPLAY_BUTTON,
        TRIGGER_AREA,
        SLIDER,
        PANORAMA_VIEW
    }

    public enum VRELEMENT_FACE_DIRECTION
    {
        UP,
        FORWARD
    }

    public enum VRBUTTON_REFRESH_TYPE
    {
        NORMAL,
        STICKY,
        BILLBOARD
    }

    public enum SWITCH_TYPE
    {
        MESH,
        MATERIAL,
        TEXTURE,
        SPRITE,
        CUSTOM
    }

    public enum VRBUTTON_COLLIDER_TYPE
    {
        BOX,
        MESH,
        SPHERE,
        CAPSULE
    }

    public enum VROBJECTBUTTON_VALIDATE_ERROR
    {
        NONE,
        ALREADY_VROBJECTBUTTON,
        NEEDS_COLLIDER
    }

    public enum DISPLAY_IMAGE_TYPE
    {
        SPRITE,
        TEXTURE
    }

    public enum VRSELECTOR_TYPE
    {
        SIGHT,
        POINTER,
        TOUCH
    }

    public enum VRLOCOMOTION_INPUT
    {
        UNITY_INPUT,
        STEAM_CONTROLLER,
        MOBILE_TILT,
        TRIGGER
    }

    public enum PLAY_ACTION
    {
        PLAY,
        STOP,
        TOGGLE
    }

    public enum HOTSPOT_TYPE
    {
        LOAD_LOCATION,
        INFO
    }

    public enum GRAB_TYPE
    {
        SLIDE,
        DRAG
    }

    public class VREasy_utils {

        public static string STORE_OPTIONS_FILE = "VREasy_options.txt";

        public static string WEB_URL_LINK = "http://blog.avrworks.com";
        public static string LOGO_IMAGE = "VREasy_logo";

        public static void LoadClassesFromAssembly(Type parentType, ref List<string> assemblyNames, ref List<string> names, string substractString = "")
        {
            Assembly assembly = Assembly.GetAssembly(parentType);
            Type[] types = assembly.GetTypes();

            assemblyNames.Clear();
            names.Clear();
            IEnumerable<Type> subclasses = types.Where(t => !t.IsAbstract && t.IsSubclassOf(parentType));
            foreach (Type t in subclasses)
            {
                if (t.IsAbstract) continue; 
                string name = t.Name;
                if(!string.IsNullOrEmpty(substractString))
                {
                    int index1 = name.IndexOf(substractString);
                    if (index1 != -1)
                    {
                        name = name.Remove(index1);
                    }
                }
                
                names.Add(name);
                assemblyNames.Add(t.AssemblyQualifiedName);
            }

        }

        public static T LoadAndSetClassFromAssembly<T>(GameObject obj, string assemblyName) where T : Component
        {
            Type t = Type.GetType(assemblyName);
            return (T)obj.AddComponent(t);
        }

        public static void LoadComponents(GameObject target, ref List<Component> components_list, ref List<string> componentNames_list)
        {
            components_list.Clear();
            componentNames_list.Clear();
            if (target == null) return;
            Component[] components = target.GetComponents<Component>();

            foreach (Component p in components)
            {
                components_list.Add(p);
                componentNames_list.Add(p.GetType().Name);
            }
        }

        public static void LoadPropertiesFromComponent(Component c, ref List<string> properties)
        {
            properties.Clear();
            PropertyInfo[] props = c.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
            foreach (PropertyInfo propertyInfo in props)
            {
                if (propertyInfo.PropertyType == typeof(float) || propertyInfo.PropertyType == typeof(double))
                {
                    //Debug.Log("Obj: " + c.ToString() + ", Property: " + propertyInfo.Name + ", Type: " + propertyInfo.PropertyType);
                    properties.Add(propertyInfo.Name);
                }

            }

        }

        public static void LoadMethods(Type myType, ref List<MethodInfo> methods_list, ref List<string> methodNames_list)
        {
            // Get the public methods.
            MethodInfo[] myArrayMethodInfo = myType.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            //Debug.Log("The number of public methods is " + myArrayMethodInfo.Length);
            methods_list.Clear();
            methodNames_list.Clear();
            for (int i = 0; i < myArrayMethodInfo.Length; i++)
            {
                MethodInfo myMethodInfo = (MethodInfo)myArrayMethodInfo[i];

                //Debug.Log("The name of the method is " + myMethodInfo.Name);
                methods_list.Add(myMethodInfo);
                methodNames_list.Add(myMethodInfo.Name);
            }
        }

        public static void LoadParameters(MethodInfo myMethodInfo, ref List<Type> parameters_list)
        {
            parameters_list.Clear();
            ParameterInfo[] arguments = myMethodInfo.GetParameters();
            foreach (ParameterInfo ta in arguments)
            {
                parameters_list.Add(ta.ParameterType);
                //Debug.Log(ta.ParameterType.FullName);
            }
        }

        public static void SetAndConfigureSprite(Sprite sp, SpriteRenderer renderer, float width, float height)
        {
            if (renderer == null) return;
            renderer.sprite = sp;
            if (sp == null) return;

            
            Vector2 sprite_size = renderer.sprite.rect.size;
            Vector2 local_sprite_size = sprite_size / renderer.sprite.pixelsPerUnit;
            Dictionary<Transform, Vector3> scales = new Dictionary<Transform, Vector3>();
            Dictionary<Transform, Vector3> locations = new Dictionary<Transform, Vector3>();
            IEnumerable<Transform> children = renderer.GetComponentsInChildren<Transform>().Where(go => go.transform.parent == renderer.transform);
            foreach (Transform t in children)
            {
                scales.Add(t, t.localScale);
                locations.Add(t, t.position);
            }
            Vector3 prevScale = renderer.transform.localScale;
            Sprite sprite = renderer.sprite;
            Bounds bounds = sprite.bounds;
            float xSize = bounds.size.x;
            float ySize = bounds.size.y;
            renderer.transform.localScale = new Vector3(width / xSize, height / ySize, 1f);
            BoxCollider col = renderer.GetComponent<BoxCollider>();
            if (col != null)
            {
                renderer.GetComponent<BoxCollider>().size = new Vector3(local_sprite_size.x, local_sprite_size.y, 0.1f); //(1.0f/ sprite.pixelsPerUnit);
            }
            foreach (Transform t in scales.Keys)
            {
                t.localScale = new Vector3(scales[t].x / (renderer.transform.localScale.x / prevScale.x), scales[t].y / (renderer.transform.localScale.y / prevScale.y), scales[t].z);
                t.position = locations[t];
            }


        }




#if UNITY_EDITOR

        public static void SetGameObjectInScene(GameObject go, Camera referenceCamera = null)
        {
            if (SceneView.lastActiveSceneView == null)
            {
                go.transform.position = Vector3.zero;
            }
            else
            {
                if (referenceCamera == null)
                    referenceCamera = SceneView.lastActiveSceneView.camera;
                Vector3 pos = referenceCamera.transform.position + (referenceCamera.transform.forward) * 2;
                go.transform.position = pos;
                Vector3 lookAtPosition = referenceCamera.transform.position;
                lookAtPosition.y = go.transform.position.y;
                go.transform.LookAt(lookAtPosition);
                go.transform.Rotate(Vector3.up, 180.0f);
            }

        }

        private static string PREFS_SHOWABOUT = "PREFS_SHOWABOUT";

        public static void DrawHelperInfo()
        {
            EditorGUILayout.Separator();
            EditorGUILayout.Separator();
            EditorGUILayout.Separator();
            EditorGUILayout.Separator();

            bool showAbout = PlayerPrefs.GetInt(PREFS_SHOWABOUT, 1) == 1;
            showAbout = EditorGUILayout.Foldout(showAbout, "About");
            PlayerPrefs.SetInt(PREFS_SHOWABOUT, showAbout ? 1 : 0);
            if (!showAbout)
                return;
            
            Handles.BeginGUI();
            //GUIContent content = new GUIContent("logo");
            //GUIStyle style = GUIStyle.none;
            Texture logo = Resources.Load<Texture>(LOGO_IMAGE);
            //content.image = logo;
            Rect rect = GUILayoutUtility.GetRect(75, 75);//content, style);
            GUI.DrawTexture(rect, logo, ScaleMode.ScaleToFit);
            
            EditorGUILayout.Separator();
            EditorGUILayout.Separator();

            Handles.BeginGUI();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("For tutorials, manuals and more info");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            Rect labelRect = GUILayoutUtility.GetRect(7 * WEB_URL_LINK.Length, 25);
            GUI.Label(labelRect, WEB_URL_LINK);
            if (Event.current.type == EventType.MouseUp && labelRect.Contains(Event.current.mousePosition))
                Application.OpenURL(WEB_URL_LINK);
            GUI.color = new Color(0.2f,0.7f,1.0f);
            GUI.Label(labelRect, WEB_URL_LINK);
            GUI.color = Color.white;
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Copyright AVR Works 2016");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            Handles.EndGUI();
        }
#endif
        }
}
﻿using UnityEngine;
using System.Collections;
using System;

namespace VREasy
{
    public class TeleportAction : VRAction
    {
        public Transform targetPosition;
        public Transform HMD;
        private Transform Eyes;
        public float fadeTimer = 0.3f;

        private LoadSceneManager _loadSceneManager = null;


        void Awake()
        {
            _loadSceneManager = LoadSceneManager.instance;

            Eyes = HMD.Find("Camera (eye)");
        }

        public override void Trigger()
        {
            teleport(targetPosition.position);
        }

        public void teleport(Vector3 futurePosition)
        {
            if(HMD != null)
            {
                StartCoroutine(doTeleport(futurePosition));
            }
            
        }

        private IEnumerator doTeleport(Vector3 futurePosition)
        {
            if (HMD != null)
            {
                _loadSceneManager.FadeOut(fadeTimer);
                yield return new WaitForSeconds(fadeTimer * 1.1f);

                Vector3 offset = new Vector3(Eyes.position.x - HMD.position.x, 0, Eyes.position.z - HMD.position.z);

                HMD.position = futurePosition - offset;
                _loadSceneManager.FadeIn(fadeTimer);
            }
        }

    }
}
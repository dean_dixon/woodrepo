﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VREasy
{
    public class ActionList : MonoBehaviour
    {
        public List<VRAction> list = new List<VRAction>();

        public void Trigger()
        {
            foreach(VRAction a in list)
            {
                if(a != null)
                    a.Trigger();
            }
        }
    }
}
﻿using UnityEngine;
using System.Collections;
using System;

namespace VREasy
{
    public class ActivateVRElements : VRAction
    {
        public VRElement[] targets;
        public bool toggle = true;
        public bool activate = false;

        public override void Trigger()
        {
            if (targets.Length == 0)
                return;

            if (activate)
            {
                foreach (VRElement t in targets)
                    t.ReactivateElement();
            }
            else
            {
                foreach (VRElement t in targets)
                    t.DeactivateElement();
            }
            if (toggle) activate = !activate;
        }
    }

}
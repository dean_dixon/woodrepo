﻿using UnityEngine;
using System.Collections;

namespace VREasy
{
    public class ActivateObjectAction : VRAction
    {
        public GameObject[] targets;

        public override void Trigger()
        {
            foreach(GameObject target in targets) {
                target.SetActive(!target.activeInHierarchy);
            } 
        }
    }
}
﻿using UnityEngine;
using System.Collections;
using System;

namespace VREasy
{
    [RequireComponent(typeof(LineRenderer))]
    [RequireComponent(typeof(Rigidbody))]
    public class PointerSelector : LOSSelector
    {
        public VRGrabTrigger grabTrigger;
        public float LineWidth
        {
            get
            {
                return _lineWidth;
            } 
            set
            {
                _lineWidth = value;

#if UNITY_5_4 || UNITY_5_5
                Line.SetWidth(_lineWidth, _lineWidth);
#else
                Line.startWidth = _lineWidth;
                Line.endWidth = _lineWidth;
#endif
            }
        }
        private float _lineWidth;

        private bool isPointerActive = false;
        private bool selectableActive = false;
        private float distanceToSelectable = 0f;

        private VRGrabbable _grabbedObject = null;

        public LineRenderer Line
        {
            get
            {
                if(_line == null)
                {
                    _line = GetComponent<LineRenderer>();
                }
                return _line;
            } set
            {
                _line = value;
            }
        }
        public LineRenderer _line = null;

        protected override VRSelectable GetSelectable()
        {
            if(isPointerActive)
            {
                //Debug.Log("In pointer selector. Name is " + );
                VRSelectable obj = GetElement<VRSelectable>();
                if (obj != null && !obj.CanSelectWithSight())
                {
                    obj = null;
                }
                if (obj != null)
                {
                    distanceToSelectable = Vector3.Distance(obj.transform.position,transform.position);
                    selectableActive = true;
                } else
                {
                    selectableActive = false;
                }
                drawLine();
                return obj;

            } else
            {
                Line.enabled = false;
                return null;
            }
        }

        protected override VRGrabbable GetGrabbable()
        {
            if (isPointerActive)
            {
                if (_grabbedObject != null)
                {
                    Debug.Log(_grabbedObject.name);

                    if(_grabbedObject.name.Equals("ProxyCube"))
                    {
                        return _grabbedObject.transform.parent.GetComponent<VREasy.VRGrabbable>();
                    }
                    return _grabbedObject;
                }
                _grabbedObject = GetElement<VRGrabbable>();
                if (_grabbedObject != null)
                {
                    distanceToSelectable = Vector3.Distance(_grabbedObject.transform.position, transform.position);
                }
                else
                {
                    //grabbable = false;
                }
                drawLine();

                //Debug.Log(_grabbedObject.name);
                if (_grabbedObject != null)
                {
                    if (_grabbedObject.name.Equals("ProxyCube"))
                    {
                        return _grabbedObject.transform.parent.GetComponent<VREasy.VRGrabbable>();
                    }
                }

                return _grabbedObject;

            }
            else
            {
                _grabbedObject = null;
                Line.enabled = false;
                return null;
            }
        }

        protected override void ChildUpdate()
        {
            if(grabTrigger != null)
            {
                grabTrigger.GetComponent<SteamControllerGrab>().checkUp = true;
                isPointerActive = grabTrigger.Triggered();
            } else
            {
                isPointerActive = false;
            }
            if(!selectableActive && _grabbedObject == null)
            {
                distanceToSelectable = selectionDistance;
            }
        }

        public void ConfigureRigidbody()
        {
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        private void drawLine()
        {
            // use line renderer to draw select line?? https://docs.unity3d.com/Manual/class-LineRenderer.html
            Line.enabled = true;
            Line.SetPosition(0, transform.position);
            Line.SetPosition(1, transform.position + transform.forward * distanceToSelectable);
        }
    }
}
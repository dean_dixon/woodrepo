﻿using UnityEngine;
using System.Collections;

namespace VREasy
{
    public class SightSelector : LOSSelector
    {
        private const string CROSSHAIR_NAME = "[vreasy]crosshair";

        //public float selectionDistance = 10.0f;
        public bool useCrosshair = true;
        //public bool debugMode = true;

        public Sprite crosshairSprite;
        public float crosshairSize = 0.1f;
        public Color crosshairActiveColour = Color.red;
        public Color crosshairIdleColour = Color.white;
        public SpriteRenderer crosshair;
        public CROSSHAIR_TYPE crosshairType = CROSSHAIR_TYPE.SINGLE_SPRITE;
        public Sprite idleSprite;

        #region
        public Sprite CrosshairSprite
        {
            get
            {
                return crosshairSprite;
            }
            set
            {
                bool reconfigure = crosshairSprite != value;
                crosshairSprite = value;
                if (reconfigure) reconfigureCrosshair();
            }
        }

        public float CrosshairSize
        {
            get
            {
                return crosshairSize;
            }
            set
            {
                bool reconfigure = crosshairSize != value;
                crosshairSize = value;
                if (reconfigure) reconfigureCrosshair();
            }
        }
        public Color CrosshairActiveColour
        {
            get
            {
                return crosshairActiveColour;
            }
            set
            {
                bool reconfigure = crosshairActiveColour != value;
                crosshairActiveColour = value;
                if (reconfigure) SetCrosshairState(false);
            }
        }
        public Color CrosshairIdleColour
        {
            get
            {
                return crosshairIdleColour;
            }
            set
            {
                bool reconfigure = crosshairIdleColour != value;
                crosshairIdleColour = value;
                if (reconfigure) SetCrosshairState(false);
            }
        }
        public SpriteRenderer Crosshair
        {
            get
            {
                if(!crosshair)
                {
                    Transform t = transform.Find(CROSSHAIR_NAME);
                    if (t)
                        crosshair = t.GetComponent<SpriteRenderer>();
                    else
                    {
                        GameObject ob = new GameObject(CROSSHAIR_NAME);
                        ob.transform.parent = transform;
                        SpriteRenderer rend = ob.AddComponent<SpriteRenderer>();
                        rend.sharedMaterial = Resources.Load<Material>("Crosshair");
                        Crosshair = rend;
                    }
                }
                return crosshair;
            }
            set
            {
                crosshair = value;
            }
        }

        #endregion PROPERTIES

        protected override VRSelectable GetSelectable() {
            /*VRSelectable obj = null;
            if(debugMode) Debug.DrawLine(transform.position, transform.position + transform.forward * selectionDistance, Color.red);
            RaycastHit _hit;
            if (Physics.Raycast(transform.position, transform.forward, out _hit, selectionDistance)) {
                obj = _hit.collider.gameObject.GetComponent<VRSelectable>();
                if(obj != null && !obj.CanSelectWithSight())
                {
                    obj = null;
                }
            }*/
            VRSelectable obj = GetElement<VRSelectable>();
            if (obj != null && !obj.CanSelectWithSight())
            {
                obj = null;
            }

            SetCrosshairState(obj != null);
            
            return obj;
        }

        public void reconfigureCrosshair()
        {
            if (!useCrosshair)
            {
                removeCrosshair();
                return;
            }

            if(crosshairSprite != null)
            {
                Crosshair.transform.rotation = transform.rotation;
                if(Camera.main == null)
                {
                    Camera cam = GetComponent<Camera>();
                    if (cam == null)
                    {
                        Debug.LogWarning("SightSelector: Main camera has not been found. Impossible to dynamically situate crosshair. Setting it to default distance");
                        Crosshair.transform.localPosition = Vector3.forward * 0.31f;
                    } else
                    {
                        Crosshair.transform.localPosition = Vector3.forward * cam.nearClipPlane * 6.01f; // 1.01
                    }
                        
                } else
                {
                    Crosshair.transform.localPosition = Vector3.forward * Camera.main.nearClipPlane * 6.01f;
                }
                Crosshair.transform.localScale = Vector3.one * crosshairSize;
                Crosshair.sprite = crosshairSprite;
            } else
            {
                removeCrosshair();
            }
        }

        private void SetCrosshairState(bool isActive)
        {
            if (crosshair == null) return;

            switch(crosshairType)
            {
                case CROSSHAIR_TYPE.SINGLE_SPRITE:
                    {
                        if (isActive)
                        {
                            crosshair.sharedMaterial.SetColor("_Color",crosshairActiveColour);
                        }
                        else
                        {
                            crosshair.sharedMaterial.SetColor("_Color", crosshairIdleColour);
                        }
                    }
                    break;
                case CROSSHAIR_TYPE.DUAL_SPRITE:
                    {
                        if (isActive)
                        {
                            crosshair.sprite = CrosshairSprite;
                        }
                        else
                        {
                            crosshair.sprite = idleSprite;
                        }
                    }
                    break;
            }
            

            
        }

        private void removeCrosshair()
        {
            if(Crosshair)
            {
                DestroyImmediate(Crosshair.gameObject);
            }
        }
        
    }
}

﻿using UnityEngine;
using System.Collections;
using System;

namespace VREasy
{
    public class OculusControllerTrigger : VRGrabTrigger
    {
        // for button names mapping visit https://docs.unity3d.com/Manual/OculusControllers.html
#if VREASY_OCULUS_UTILITIES_SDK
        public OVRInput.Controller controller;
        public OVRInput.Button button;
#endif

        public override bool Triggered()
        {
#if VREASY_OCULUS_UTILITIES_SDK
            return OVRInput.Get(button, controller);
#else
            return false;
#endif
        }
    }
}
﻿using UnityEngine;
using System.Collections;
using System;

namespace VREasy
{
#if VREASY_STEAM_SDK
    [RequireComponent(typeof(SteamVR_TrackedObject))]
#endif
    public class SteamControllerGrab : VRGrabTrigger
    {

        [HideInInspector]
        public bool isMeasuring = false;

        public Valve.VR.EVRButtonId button = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
        private SteamVR_Controller.Device device;
        private SteamVR_TrackedObject trackedObject;
        public bool checkUp = true;

        void Start()
        {
            trackedObject = GetComponent<SteamVR_TrackedObject>();
        }

        public override bool Triggered()
        {
            if (trackedObject == null || isMeasuring) return false;

            device = SteamVR_Controller.Input((int)trackedObject.index);

            if(device != null && device.GetPress(button))
            {
                Vector2 touchpad = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
                //Debug.Log("X: " + touchpad.x + ". Y: " + touchpad.y);
                if (checkUp)
                {
                    if (Mathf.Abs(touchpad.x) < .7 && Mathf.Abs(touchpad.y) > .3)
                    {
                        if ((checkUp && touchpad.y > .3) || (!checkUp && touchpad.y < -.3))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }
                    else
                    {
                        if (Mathf.Abs(touchpad.x) < .7)
                        {
                            return true;
                        }
                        return false;
                    }
                }
                else
                {
                    return true;
                }
                
            }
            else
            {
                return false;
            }
            //return (device != null) ? device.GetPress(button) : false;
        }
    }
}
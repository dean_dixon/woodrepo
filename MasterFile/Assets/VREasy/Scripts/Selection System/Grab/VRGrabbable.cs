﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace VREasy
{
    [RequireComponent(typeof(Collider))]
    public class VRGrabbable : VRElement
    {
        [HideInInspector]
        public GameObject PlayerTablet;


        public GRAB_TYPE type = GRAB_TYPE.DRAG;

        public bool moveXAxis = true;
        public bool moveYAxis = true;
        public bool moveZAxis = true;

        public bool isValve;
        public bool isPump;

        public Color grabColour = Color.yellow;
        public bool alignWithPivot = false;
        public bool snapToOrigin = true;
        public float snapRadius = 0.25f;

        private Vector3 originalPosition;
        private Quaternion originalRotation;
        [HideInInspector]
        public bool isGrabbed = false;
        private Transform selectorTransform;
        private Vector3 selectorOffset;
        private bool wasKinematic;
        private List<Color> colourList = new List<Color>();

        private Rigidbody ownRb = null;
        private FixedJoint joint = null;
        private bool hadRb = false;
        private Collider _collider = null;
        private bool wasTrigger = false;

        public string[] tagStrings;
        private VPDatabase vpd;

        [HideInInspector]
        public float movementScale = 1f;

        [HideInInspector]
        public bool isResetting = false;

        [HideInInspector]
        public bool isHidden = false;

        private List<Renderer> myRends = new List<Renderer>();
        List<Material[]> rendMaterials = new List<Material[]>();

        public List<string> infoValues = new List<string>();

        public struct ManifoldStruct
        {
            public string name, type, depth, foundation, weight, dPressure, tempRating, headers, hubs, sensors;
        }

        [HideInInspector]
        public ManifoldStruct manifoldInfo = new ManifoldStruct();

        public struct PLEMStruct
        {
            public string name, type, depth, weight, dPressure, tempRating, hubs;
        }

        [HideInInspector]
        public PLEMStruct plemInfo = new PLEMStruct();

        public struct XTStruct
        {
            public string name, type, depth, configuration, weight, pressureRating, tempRating, materialClass, producitonBoreSize, annulusBoreSize, controlPressure, sensors;
        }

        [HideInInspector]
        public XTStruct xtInfo = new XTStruct();

        public struct JumperStruct
        {
            public string name, well, diameter, maxDepthRating, shape, span, connectorType, pressureRating, maxTemp, materialClass, insulation, sensors;
        }

        [HideInInspector]
        public JumperStruct jumperInfo = new JumperStruct();

        private float resetTimer = 0;

        private float moveTimer = 0, vplinkUpdateTimer = 0, vplinkTargetValue=0;

        private string vplinkTargetString = "";

        private GameObject myValve;

        [HideInInspector]
        public VR_Tablet_Script myTabletScript;

        void Awake()
        {
            loadDefaultColours();
            _collider = GetComponent<Collider>();
            wasTrigger = _collider.isTrigger;
            SetOrigins();
        }

        private void Start()
        { 

            PlayerTablet = GameObject.Find("Player/[CameraRig]/Controller (left)/TabletParent");
            if (PlayerTablet != null)
            {
                myTabletScript = PlayerTablet.transform.GetComponent<VR_Tablet_Script>();
            }

            if (transform.GetComponent<VREasy.TransformController>() != null)
            {
                isValve = transform.GetComponent<VREasy.TransformController>().isValve;
                isPump = transform.GetComponent<VREasy.TransformController>().isPump;
            }
            vpd = FindObjectOfType<VPDatabase>();
            if (vpd == null)
            {
                //Debug.LogError("Cannot find VP Link database in the VerMovement object");
            }

            myValve = transform.GetComponent<TransformController>().myValve;

            VPLinkEquipment vpe = transform.GetComponent<VPLinkEquipment>();
            if (vpe != null)
            {
                string tempStr = vpe.tagList;

                tagStrings = tempStr.Split(',');

                //Debug.Log("TagList=" + tempStr);
                SetInitialPosition();
            } else
            {
                //Debug.Log(transform.name + " needs a VPLinkEquipment");
            }

        }

        private void OnDrawGizmos()
        {
            Color oldColour = Gizmos.color;
            Gizmos.color = Color.red;
            if (snapToOrigin)
            {
                Gizmos.DrawWireSphere(originalPosition, snapRadius);
            }
            Gizmos.color = oldColour;
        }

        public virtual void StartGrab(VRSelector selector)
        {
            //Debug.Log("In Grab");
            if (PlayerTablet == null || myTabletScript == null)
            {
                PlayerTablet = GameObject.Find("Player/[CameraRig]/Controller (left)/TabletParent");
                myTabletScript = PlayerTablet.transform.GetComponent<VR_Tablet_Script>();
            }

            

            //CheckJumper();

            //edited by Dean Dixon
            //When an object is grabbed, tell the player's tablet what the object is etc.
            //only objects with a TransformController script attached can be manipulated
            if (!myTabletScript.hasHitButton && !myTabletScript.isMakingJumper && !myTabletScript.isMakingJumper2 && PlayerTablet != null && transform.GetComponent<TransformController>() != null && PlayerTablet.transform.GetComponent<VR_Tablet_Script>().selectedObject != transform)
            {
                moveTimer = 0;

                if (transform.GetComponent<Rigidbody>() != null)
                {
                    transform.GetComponent<Rigidbody>().isKinematic = true;
                }

                if (transform.tag.Equals("ROV"))
                {
                    transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
                }

                if (myTabletScript.selectedObject != null)
                {
                    //transform.GetComponent<Rigidbody>().constraints = myTabletScript.selectedObject.GetComponent<Rigidbody>().constraints;
                }
                else
                {
                    if (myTabletScript.movingVeritcal)
                    {
                        //transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
                    }
                    else
                    {
                        //transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
                    }
                }


                if (myTabletScript.selectedObject != null)
                {
                    myTabletScript.selectedObject.GetComponent<VREasy.VRGrabbable>().isGrabbed = false;

                    if (myTabletScript.selectedObject.GetComponent<LineRenderer>() != null)
                    {
                        myTabletScript.selectedObject.GetComponent<LineRenderer>().enabled = false;
                    }

                    Renderer[] renders = myTabletScript.selectedObject.transform.GetComponentsInChildren<Renderer>();
                    for (int ii = 0; ii < renders.Length; ii++)
                    {
                        List<Material> materialList = new List<Material>();
                        materialList.Add(renders[ii].materials[0]);
                        renders[ii].materials = materialList.ToArray();
                    }

                    myTabletScript.selectedObject.transform.GetComponent<VRGrabbable>().paintColours(true);
                }

                myTabletScript.selectedObject = transform;
                myTabletScript.selectedObjectOriginalPosition = originalPosition;
                myTabletScript.selectedObjectOriginalRotation = originalRotation.eulerAngles;

                myTabletScript.sliderScript.targetComponent = transform.GetComponent<TransformController>();

                myTabletScript.selectedObjectGrabbableController = transform.GetComponent<VREasy.VRGrabbable>();

                myTabletScript.VibrateRight();
                myTabletScript.transform.GetComponent<AudioSource>().clip = myTabletScript.selectSound;
                myTabletScript.transform.GetComponent<AudioSource>().Play();
                myTabletScript.InsertTabletInfo();
                //myTabletScript.UpdateAxisLineRends();
            
                paintColours(false);
                _collider.isTrigger = true;

                if (selector._previouslyGrabbedObject == null)
                {
                    //Debug.Log("Yea we made it");
                    selector._previouslyGrabbedObject = transform.GetComponent<VREasy.VRGrabbable>();
                }

                switch (type)
                {
                    case GRAB_TYPE.DRAG:
                        {
                            if (isGrabbed) return;
                            if (alignWithPivot)
                            {
                                transform.rotation = selector.transform.rotation;
                                transform.position = selector.transform.position;
                            }
                            ownRb = GetComponent<Rigidbody>();
                            hadRb = ownRb != null;
                            if (ownRb == null)
                            {
                                ownRb = gameObject.AddComponent<Rigidbody>();
                                ownRb.useGravity = false;
                            }
                            joint = gameObject.GetComponent<FixedJoint>();
                            if (joint == null)
                            {
                                joint = gameObject.AddComponent<FixedJoint>();
                            }
                            joint.connectedBody = selector.GetComponent<Rigidbody>();

                        }
                        break;
                    case GRAB_TYPE.SLIDE:
                        {
                            selectorTransform = selector.transform;
                            selectorOffset = selectorTransform.position;
                            if (isGrabbed) return;
                            Rigidbody rb = GetComponent<Rigidbody>();
                            if (rb != null)
                            {
                                wasKinematic = rb.isKinematic;
                                rb.isKinematic = true;
                            }
                        }
                        break;
                }

                isGrabbed = true;
                myTabletScript.hasHitButton = true;

            }
            else
            {
                
                if(transform.name.Equals("Handle"))
                {
                    paintColours(false);
                    _collider.isTrigger = true;

                    switch (type)
                    {
                        case GRAB_TYPE.DRAG:
                            {
                                if (isGrabbed) return;
                                if (alignWithPivot)
                                {
                                    transform.rotation = selector.transform.rotation;
                                    transform.position = selector.transform.position;
                                }
                                ownRb = GetComponent<Rigidbody>();
                                hadRb = ownRb != null;
                                if (ownRb == null)
                                {
                                    ownRb = gameObject.AddComponent<Rigidbody>();
                                    ownRb.useGravity = false;
                                }
                                joint = gameObject.GetComponent<FixedJoint>();
                                if (joint == null)
                                {
                                    joint = gameObject.AddComponent<FixedJoint>();
                                }
                                joint.connectedBody = selector.GetComponent<Rigidbody>();

                            }
                            break;
                        case GRAB_TYPE.SLIDE:
                            {
                                selectorTransform = selector.transform;
                                selectorOffset = selectorTransform.position;
                                if (isGrabbed) return;
                                Rigidbody rb = GetComponent<Rigidbody>();
                                if (rb != null)
                                {
                                    wasKinematic = rb.isKinematic;
                                    rb.isKinematic = true;
                                }
                            }
                            break;
                    }

                    isGrabbed = true;
                    myTabletScript.hasHitButton = true;

                    if(transform.parent.name.Equals("Animation_Scrollbar_VR"))
                    {
                        //Debug.Log("yO");
                        myTabletScript.hasHitButton = false;
                        myTabletScript.myMaster.GetComponent<MasterController>().PauseSceneAnimation();
                    }
                }
                else if(PlayerTablet.transform.GetComponent<VR_Tablet_Script>().selectedObject == transform)
                {
                    paintColours(false);
                    _collider.isTrigger = true;

                    switch (type)
                    {
                        case GRAB_TYPE.DRAG:
                            {
                                if (isGrabbed) return;
                                if (alignWithPivot)
                                {
                                    transform.rotation = selector.transform.rotation;
                                    transform.position = selector.transform.position;
                                }
                                ownRb = GetComponent<Rigidbody>();
                                hadRb = ownRb != null;
                                if (ownRb == null)
                                {
                                    ownRb = gameObject.AddComponent<Rigidbody>();
                                    ownRb.useGravity = false;
                                }
                                joint = gameObject.GetComponent<FixedJoint>();
                                if (joint == null)
                                {
                                    joint = gameObject.AddComponent<FixedJoint>();
                                }
                                joint.connectedBody = selector.GetComponent<Rigidbody>();

                            }
                            break;
                        case GRAB_TYPE.SLIDE:
                            {
                                selectorTransform = selector.transform;
                                selectorOffset = selectorTransform.position;
                                if (isGrabbed) return;
                                Rigidbody rb = GetComponent<Rigidbody>();
                                if (rb != null)
                                {
                                    wasKinematic = rb.isKinematic;
                                    rb.isKinematic = true;
                                }
                            }
                            break;
                    }

                    isGrabbed = true;
                    myTabletScript.hasHitButton = true;
                }
            }
        }

        public virtual void StopGrab(VRSelector selector)
        {
            
            myTabletScript.hasHitButton = false;
            
            
            if (transform.GetComponent<Rigidbody>() != null)
            {
                transform.GetComponent<Rigidbody>().isKinematic = true;
            }
            isGrabbed = false;
            //paintColours(true);
            _collider.isTrigger = wasTrigger;

            switch (type)
            {
                case GRAB_TYPE.DRAG:
                    {
                        Destroy(GetComponent<FixedJoint>());
                        if (!hadRb) Destroy(ownRb);
                    }
                    break;
                case GRAB_TYPE.SLIDE:
                    {
                        selectorTransform = null;
                        Rigidbody rb = GetComponent<Rigidbody>();
                        if (rb != null) rb.isKinematic = wasKinematic;
                    }
                    break;
            }
            // if snap to origin, place object in its original state (if close enough)
            if (snapToOrigin)
            {
                snapObjectToOrigin();
            }
            else
            {
                tossObject(selector);
            }
        }

        void Update()
        {
            moveTimer += Time.deltaTime;
            vplinkUpdateTimer += Time.deltaTime;

            if(moveTimer > .4f && isGrabbed)
            {
                if (transform.GetComponent<Rigidbody>() != null)
                {
                    transform.GetComponent<Rigidbody>().isKinematic = false;
                }
            }

            if (isResetting)
            {
                isResetting = false;
                transform.position = originalPosition;
                transform.rotation = originalRotation;
                resetTimer = 0;
            }

            if(isGrabbed)
            {
                if (tagStrings.Length >= 4)
                {
                    /*
                    myTabletScript.canvasInfoTexts[0].text = myTabletScript.infoTexts[0].text = vpd.PVEStr(tagStrings[0], "?? " + tagStrings[0] + " ??");
                    myTabletScript.canvasInfoTexts[1].text = myTabletScript.infoTexts[1].text = vpd.PVEStr(tagStrings[1], "?? " + tagStrings[1] + " ??");
                    myTabletScript.canvasInfoTexts[2].text = myTabletScript.infoTexts[2].text = vpd.PVEStr(tagStrings[2], "?? " + tagStrings[2] + " ??");
                    myTabletScript.canvasInfoTexts[3].text = myTabletScript.infoTexts[3].text = vpd.PVEStr(tagStrings[3], "?? " + tagStrings[3] + " ??");
                    */

                    /*
                    myTabletScript.infoTexts[0].text = myTabletScript.infoTexts[0].text = vpd.PVEStr(tagStrings[0], "?? " + tagStrings[0] + " ??");
                    myTabletScript.infoTexts[1].text = myTabletScript.infoTexts[1].text = vpd.PVEStr(tagStrings[1], "?? " + tagStrings[1] + " ??");
                    myTabletScript.infoTexts[2].text = myTabletScript.infoTexts[2].text = vpd.PVEStr(tagStrings[2], "?? " + tagStrings[2] + " ??");
                    myTabletScript.infoTexts[3].text = myTabletScript.infoTexts[3].text = vpd.PVEStr(tagStrings[3], "?? " + tagStrings[3] + " ??");
                    */

                    //Debug.Log("Tag name = " + tagStrings[3] + ". Value = " + myTabletScript.infoTexts[3].text);
                    
                }
            }

            if (vplinkUpdateTimer >= 1.0f)
            {
                if (myValve!=null && myValve.GetComponent<ValveLerper>().isLever)
                {
                    float vplinkCurrentValue = vpd.PVE(getManipulatedTag(), 0);

                    if(vplinkCurrentValue != vplinkTargetValue)
                    {
                        if(vplinkCurrentValue == 0)
                        {
                            CloseFully();
                        }
                        else
                        {
                            OpenFully();
                        }
                        vplinkTargetValue = vplinkCurrentValue;
                    }
                }
                else if(isPump)
                {
                    string vplinkCurrentString = vpd.PVEStr(getManipulatedTag(), "");

                    if (!vplinkCurrentString.Equals(vplinkTargetString))
                    {
                        if (vplinkCurrentString.Equals("Off"))
                        {
                            CloseFully();
                        }
                        else
                        {
                            OpenFully();
                        }
                        vplinkTargetString = vplinkCurrentString;
                    }
                }

                vplinkUpdateTimer = 0;
            }
        }

        void FixedUpdate()
        {         
            if (type == GRAB_TYPE.SLIDE && isGrabbed && selectorTransform != null && moveTimer > 1)
            {
                // POSITION
                Vector3 moved = (selectorTransform.position - selectorOffset);
                float moveX = moveXAxis ? Vector3.Dot(moved, transform.right) : 0;
                float moveY = moveYAxis ? Vector3.Dot(moved, transform.up) : 0;
                float moveZ = moveZAxis ? Vector3.Dot(moved, transform.forward) : 0;
                ApplyMovement(new Vector3(moveX, moveY, moveZ));
            }
            
        }

        protected virtual void ApplyMovement(Vector3 move)
        {
            //Debug.Log("Move Timer = " + moveTimer);
            //edited by Dean Dixon
            //apply the movement scale variabe if the grab type is glide (not drag)
            if (type.Equals(GRAB_TYPE.DRAG))
            {
                move = RestrictMovement(move);
                transform.Translate(move, Space.Self);
            }
            else
            {
                move = RestrictMovement(move);
                transform.Translate(move * movementScale, Space.Self);
            }
        }

        protected virtual Vector3 RestrictMovement(Vector3 move)
        {
            return move;
        }

        public void SetOrigins()
        {
            originalPosition = transform.position;
            originalRotation = transform.rotation;
        }

        private void tossObject(VRSelector selector)
        {
            
        }

        private void snapObjectToOrigin()
        {
            if(Vector3.Distance(transform.position,originalPosition) <= snapRadius)
            {
                transform.position = originalPosition;
                transform.rotation = originalRotation;
            }
        }

        private void loadDefaultColours()
        {
            colourList.Clear();
            Renderer[] rends = GetComponentsInChildren<Renderer>();
            foreach (Renderer r in rends)
            { 
                colourList.Add(r.material.color);
                rendMaterials.Add(r.materials);
            }
        }

        public void paintColours(bool defaultColour)
        {
            Renderer[] rends = GetComponentsInChildren<Renderer>();
            for (int ii=0; ii < rends.Length; ii++)
            {
                try
                {
                    if (defaultColour)
                    {
                        foreach(Material m in rendMaterials[ii])
                        {
                            m.color = Color.white;
                        }
                        rends[ii].materials = rendMaterials[ii];
                    }
                    else
                    {
                        foreach (Material m in rendMaterials[ii])
                        {
                            m.color = grabColour;
                        }
                    }
                    
#pragma warning disable 0168
                }
                catch (System.Exception e)
#pragma warning restore 0168
                {
                    // ignore ghost old out of range
                }
            }
        }

        public string getManipulatedTag()
        {
            return tagStrings[4];
        }

        public void SetInitialPosition()
        {
            if (isValve)
            {
                ValveLerper vl = myValve.GetComponent<ValveLerper>();
                if (vl != null && (vpd != null))
                {
                    vl.InitializeValvePosition(vpd.PV(getManipulatedTag(), 0));
                }
            }
            else
            {
                Debug.Log(vpd.PVEStr(getManipulatedTag(), "0"));
                if(vpd.PVEStr(getManipulatedTag(), "0").Equals("On"))
                {
                    transform.GetComponent<AudioSource>().loop = true;
                    transform.GetComponent<AudioSource>().clip = transform.GetComponent<TransformController>().onSound;
                    transform.GetComponent<AudioSource>().Play();
                }
            }
        }

        public void MyLerpingStatus(float atValue)
        {
            vpd.setPV(getManipulatedTag(), atValue);
        }

        public void MyNullLerpingStatus(float atValue)
        {

        }

        public void OpenFully()
        {
            if (!isPump)
            {
                if (myValve.GetComponent<ValveLerper>().isLever)
                {
                    vpd.setPV(getManipulatedTag(), 1);
                    myValve.GetComponent<ValveLerper>().StartLerp(vpd.PV(getManipulatedTag(), 0), MyNullLerpingStatus);
                    vplinkTargetValue = 1;

                    if (Mathf.Abs(myValve.GetComponent<ValveLerper>().angularRate) > 0)
                    {
                        transform.GetComponent<AudioSource>().loop = false;
                        transform.GetComponent<AudioSource>().clip = transform.GetComponent<TransformController>().onSound;
                        transform.GetComponent<AudioSource>().pitch = Random.Range(.8f, 1.1f);
                        transform.GetComponent<AudioSource>().Play();
                    }
                }
                else
                {
                    vpd.setPV(getManipulatedTag(), 100);
                    if (myValve != null)
                    {
                        myValve.GetComponent<ValveLerper>().StartLerp(vpd.PV(getManipulatedTag(), 0), MyLerpingStatus);
                    }

                    if (Mathf.Abs(myValve.GetComponent<ValveLerper>().angularRate) > 0)
                    {
                        transform.GetComponent<AudioSource>().loop = false;
                        transform.GetComponent<AudioSource>().clip = transform.GetComponent<TransformController>().offSound;
                        transform.GetComponent<AudioSource>().Play();
                    }
                }
            }
            else
            {
                vplinkTargetString = "On";
                vpd.setPV(getManipulatedTag(), 1);
                transform.GetComponent<AudioSource>().loop = true;
                transform.GetComponent<AudioSource>().clip = transform.GetComponent<TransformController>().onSound;
                transform.GetComponent<AudioSource>().Play();
            }
        }

        public void OpenInc()
        {
            vpd.setPV(getManipulatedTag(), vpd.PV(getManipulatedTag(), 0) + 2);
            if (myValve != null)
            {
                myValve.GetComponent<ValveLerper>().StartLerp(vpd.PV(getManipulatedTag(), 0), MyLerpingStatus);
            }

            if (Mathf.Abs(myValve.GetComponent<ValveLerper>().angularRate) > 0)
            {
                transform.GetComponent<AudioSource>().loop = false;
                transform.GetComponent<AudioSource>().clip = transform.GetComponent<TransformController>().offSound;
                transform.GetComponent<AudioSource>().Play();
            }
        }

        public void CloseFully()
        {
            if (!isPump)
            {
                if (myValve.GetComponent<ValveLerper>().isLever)
                {
                    vpd.setPV(getManipulatedTag(), 0);
                    myValve.GetComponent<ValveLerper>().StartLerp(100 * vpd.PV(getManipulatedTag(), 0), MyNullLerpingStatus);
                    vplinkTargetValue = 0;

                    if (Mathf.Abs(myValve.GetComponent<ValveLerper>().angularRate) > 0)
                    {
                        transform.GetComponent<AudioSource>().loop = false;
                        transform.GetComponent<AudioSource>().clip = transform.GetComponent<TransformController>().onSound;
                        transform.GetComponent<AudioSource>().pitch = Random.Range(.2f, .7f);
                        transform.GetComponent<AudioSource>().Play();
                    }
                }
                else
                {
                    vpd.setPV(getManipulatedTag(), 0);
                    if (myValve != null)
                    {
                        myValve.GetComponent<ValveLerper>().StartLerp(vpd.PV(getManipulatedTag(), 0), MyLerpingStatus);
                    }

                    if (Mathf.Abs(myValve.GetComponent<ValveLerper>().angularRate) > 0)
                    {
                        transform.GetComponent<AudioSource>().loop = false;
                        transform.GetComponent<AudioSource>().clip = transform.GetComponent<TransformController>().offSound;
                        transform.GetComponent<AudioSource>().Play();
                    }
                }
            }
            else
            {
                vplinkTargetString = "Off";
                vpd.setPV(getManipulatedTag(), 0);
                transform.GetComponent<AudioSource>().loop = false;
                transform.GetComponent<AudioSource>().clip = transform.GetComponent<TransformController>().offSound;
                transform.GetComponent<AudioSource>().Play();
            }
        }

        public void CloseDec()
        {
            vpd.setPV(getManipulatedTag(), vpd.PV(getManipulatedTag(), 0) - 2);
            if (myValve != null)
            {
                myValve.GetComponent<ValveLerper>().StartLerp(vpd.PV(getManipulatedTag(), 0), MyLerpingStatus);
            }

            if (Mathf.Abs(myValve.GetComponent<ValveLerper>().angularRate) > 0)
            {
                transform.GetComponent<AudioSource>().loop = false;
                transform.GetComponent<AudioSource>().clip = transform.GetComponent<TransformController>().offSound;
                transform.GetComponent<AudioSource>().Play();
            }
        }

        public void CloseAmount(float amount)
        {
            vpd.setPV(getManipulatedTag(), vpd.PV(getManipulatedTag(), 0) - amount);
            if (myValve != null)
            {
                myValve.GetComponent<ValveLerper>().StartLerp(vpd.PV(getManipulatedTag(), 0), MyLerpingStatus);
            }

            if (Mathf.Abs(myValve.GetComponent<ValveLerper>().angularRate) > 0)
            {
                transform.GetComponent<AudioSource>().loop = false;
                transform.GetComponent<AudioSource>().clip = transform.GetComponent<TransformController>().offSound;
                transform.GetComponent<AudioSource>().Play();
            }
        }

        public void OpenAmount(float amount)
        {
            vpd.setPV(getManipulatedTag(), vpd.PV(getManipulatedTag(), 0) + amount);
            if (myValve != null)
            {
                myValve.GetComponent<ValveLerper>().StartLerp(vpd.PV(getManipulatedTag(), 0), MyLerpingStatus);
            }

            if( Mathf.Abs(myValve.GetComponent<ValveLerper>().angularRate) > 0)
            {
                transform.GetComponent<AudioSource>().loop = false;
                transform.GetComponent<AudioSource>().clip = transform.GetComponent<TransformController>().offSound;
                transform.GetComponent<AudioSource>().Play();
            }

        }

        public void reloadVPLink()
        {
            VPLinkEquipment vpe = transform.GetComponent<VPLinkEquipment>();
            if (vpe != null)
            {
                string tempStr = vpe.tagList;

                tagStrings = tempStr.Split(',');

                //Debug.Log("TagList=" + tempStr);
                SetInitialPosition();
            } else
            {
                //Debug.Log(transform.name + " needs a VPLinkEquipment");
            }
        }

        public string ExtractNumber(string original)
        {
            return Regex.Split(original, @"\D+")[0];
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System;

namespace VREasy
{
    public abstract class LOSSelector : VRSelector
    {
        public float selectionDistance = 10.0f;

        protected T GetElement<T>() where T : VRElement
        {
            T obj = null;
            RaycastHit _hit;

            int lay = 13;
            int mask = ~(1 << lay);

            if (Physics.Raycast(transform.position, transform.forward, out _hit, selectionDistance, mask))
            {
                obj = _hit.collider.gameObject.GetComponent<T>();
            }

            return obj;
        }
    }
}
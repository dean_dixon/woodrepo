﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VREasy
{
    [RequireComponent(typeof(MeshRenderer))]
    [ExecuteInEditMode]
    public class VRPanoramaView : MonoBehaviour
    {
        public Texture2D Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
                Rend.sharedMaterial.mainTexture = _image;
            }
        }

        public Texture2D _image;

        public List<LoadSceneAction> Locations = new List<LoadSceneAction>();

        private MeshRenderer Rend
        {
            get
            {
                return GetComponent<MeshRenderer>();
            }
        }

        // Use this for initialization
        void Awake()
        {
            Image = _image;
        }

        void Update()
        {
            Image = _image;
        }
    }
}
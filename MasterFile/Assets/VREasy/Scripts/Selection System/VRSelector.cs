﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace VREasy
{
    public abstract class VRSelector : MonoBehaviour
    {

        public float activationTime = 1.5f;
        public bool hasTooltip = true;

        public GameObject canvasObject;

        #region
        public Text Tooltip
        {
            get
            {
                if(tooltip == null)
                {
                    tooltip = GetComponentInChildren<Text>();
                }
                return tooltip;
            }
            set
            {
                tooltip = value;
            }
        }
        public Text tooltip;
        #endregion PROPERTIES

        public Font tooltipFont;
        public int tooltipSize = 30;
        public Color tooltipColour = Color.yellow;
        public float tooltipDistance = 0.15f;

        protected float _selectedTime = 0.0f;
        private VRSelectable _previouslySelectedObject;
        public VRGrabbable _previouslyGrabbedObject;

        protected abstract VRSelectable GetSelectable(); // must be overriden by children classes (returns the selectable object when it is selected)
        protected virtual VRGrabbable GetGrabbable()
        {
            return null;
        }

        private void Start()
        {
            //Debug.Log(name);
        }

        void Update()
        {
            CheckSelectable();
            CheckActivate();
            CheckGrabbable();
            ChildUpdate();
        }

        protected virtual void ChildUpdate() { } // to be overriden by children if they need the Update function

        private void setTooltip()
        {
            if(hasTooltip)
                reconfigureTooltip(_previouslySelectedObject);
        }

        private void clearTooltip()
        {
            if(hasTooltip)
                reconfigureTooltip(null);
        }

        public void reconfigureTooltip(VRSelectable selectable)
        {
            if (tooltip != null)
            {
                if(tooltipFont != null) tooltip.font = tooltipFont;
                tooltip.fontSize = tooltipSize;
                tooltip.color = tooltipColour;
                
                if (selectable == null)
                {
                    tooltip.text = "";
                }
                else
                {
                    tooltip.text = selectable.tooltip;
                    // reposition
                    if (!string.IsNullOrEmpty(tooltip.text))
                    {
                        Vector3 direction = transform.position - selectable.transform.position;
                        canvasObject.transform.position = selectable.transform.position + direction * tooltipDistance;
                        canvasObject.transform.LookAt(selectable.transform);
                    }
                }
            }
        }

        private void CheckActivate()
        {
            if (_selectedTime > activationTime)
            {
                _selectedTime = 0.0f;
                ActivateSelectable(_previouslySelectedObject);
                
            }
        }

        private void CheckSelectable() {
            VRSelectable obj = filterVRelement<VRSelectable>(GetSelectable());
            if (obj == null || obj != _previouslySelectedObject)
            {
                StoppedInteraction(_previouslySelectedObject);
                _selectedTime = 0.0f;
            }
            if (obj != null && SelectSelectable(obj))
            {
                _selectedTime += Time.deltaTime;
                _previouslySelectedObject = obj;
                // show tooltip
                setTooltip();
            }
            else {
                if (_previouslySelectedObject != null)
                {
                    UnselectSelectable(_previouslySelectedObject);
                    // clear tooltip
                    clearTooltip();
                }
                _selectedTime = 0.0f;
                _previouslySelectedObject = null;
            }
            

        }

        private void CheckGrabbable()
        {
            VRGrabbable obj = filterVRelement<VRGrabbable>(GetGrabbable());
            if (obj == null) DeactivateGrabbable(_previouslyGrabbedObject);
            // do something with it
            ActivateGrabbable(obj);
            _previouslyGrabbedObject = obj;
        }

        private T filterVRelement<T>(VRElement element) where T : VRElement
        {
            //Debug.Log(element.transform.name);
            return element == null ? null : (element.active ? (T)element : null);
        }
        // Interact with selectable object //
        private void ActivateSelectable(VRSelectable obj)
        {
            if(obj != null)
                obj.activate();
        }

        private bool SelectSelectable(VRSelectable obj)
        {
            if (obj != null)
                return obj.select();
            else return false;
        }

        private void UnselectSelectable(VRSelectable obj)
        {
            if (obj != null)
                obj.unselect();
        }

        private void StoppedInteraction(VRSelectable obj)
        {
            if (obj != null)
                obj.stopInteraction();
        }

        public void ActivateGrabbable(VRGrabbable obj)
        {
            if (obj != null)
            {
                obj.StartGrab(this);
            }
        }

        private void DeactivateGrabbable(VRGrabbable obj)
        {
            if (obj != null)
                obj.StopGrab(this);
        }
    }
}
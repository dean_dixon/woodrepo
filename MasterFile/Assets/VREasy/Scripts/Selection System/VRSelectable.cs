﻿using UnityEngine;
using System.Collections;

namespace VREasy
{
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(ActionList))]
    public class VRSelectable : VRElement
    {
        public float coolDownTime = 2.0f;       // Time after activation in which the object cannot be selected again
        public float deactivationTime = 0.5f;   // Time after activation to return to idle state

        public AudioClip activateSound;
        public AudioClip selectSound;

        public string tooltip;

        private AudioSource _audio
        {
            get
            {
                return GetComponent<AudioSource>();
            }
        }
        public ActionList actionList
        {
            get
            {
                if (_actionList == null) _actionList = GetComponent<ActionList>();
                return _actionList;
            }
        }
        
        protected ActionList _actionList;

        private float _lastSelectionTime = 0.0f;

        protected bool isPressed = false;
        protected bool isSelected = false;

        void Awake()
        {
            Initialise();
        }

        protected virtual void Initialise()
        {

        }

        public virtual bool CanSelectWithSight()
        {
            return true;
        }

        public virtual bool CanSelectWithTouch()
        {
            return true;
        }

        // When a Selector initiates contact with IVRSelectable
        // returns whether the object is in fact selected (if it can be selected)
        public virtual bool select() {
            if (_lastSelectionTime + coolDownTime > Time.time)
            {
                return false;
            } else
            {
                // only play audio the first time
                if (!isPressed)
                    playSound(selectSound);
                Pressed();
                isPressed = true;
                return true;
            }
        }

        // When a Selector stops contact with IVRSelectable
        public virtual void unselect() {
            Unselected();
            isPressed = false;
        }

        public virtual void stopInteraction()
        {
            Unpressed();
            isPressed = false;
        }

        // method that will be called when a Selector activates a IVRSelectable
        public virtual void activate() {
            if (_lastSelectionTime + coolDownTime > Time.time)
                return;
            playSound(activateSound);
            _lastSelectionTime = Time.time;
            Selection();
            isSelected = true;
            Invoke("ToIdle", deactivationTime);
            Trigger();
        }

        private void ToIdle() {
            isSelected = false;
            Unselected();
        }

        private void playSound(AudioClip clip)
        {
            if (clip)
            {
                _audio.clip = clip;
                _audio.Play();
            }
        }

        // Methods to be overriden in children classes
        protected virtual void Selection()
        {
#if UNITY_EDITOR
            //Debug.Log("Selected!");
#endif
        }

        protected virtual void Pressed()
        {
#if UNITY_EDITOR
            //Debug.Log("Pressed!");
#endif
        }

        protected virtual void Unselected()
        {
#if UNITY_EDITOR
            //Debug.Log("Unselected!");
#endif
        }

        protected virtual void Trigger()
        {
            // TRIGGER ACTION
            actionList.Trigger();
        }

        protected virtual void Unpressed()
        {
#if UNITY_EDITOR
            //Debug.Log("Stopped interaction!");
#endif
        }

    }
}

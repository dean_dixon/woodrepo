﻿using UnityEngine;
using System.Collections;
using System.Reflection;

namespace VREasy
{
    [RequireComponent(typeof(BoxCollider))]
    public class VRSlider : VRGrabbable
    {
        public float value = 0.5f;
        public float min = 0.0f;
        public float max = 1.0f;

        public Component targetComponent;
        public string targetProperty;

        public float MIN = -0.3f;
        public float MAX = 0.3f;

        //private VR_Tablet_Script myTabletScript;

        void Start()
        {
            myTabletScript = GameObject.Find("TabletParent").GetComponent<VR_Tablet_Script>();
            moveXAxis = true;
            moveYAxis = false;
            moveZAxis = false;
            snapToOrigin = false;
            type = GRAB_TYPE.SLIDE;
            RecalculateBoundaries();
        }

        protected override Vector3 RestrictMovement(Vector3 move)
        {
            Vector3 dest = transform.TransformDirection(move) + transform.localPosition;
            if (dest.x > MAX) move.x = -0.001f;
            if (dest.x < MIN) move.x = 0.001f;
            return move;
        }

        protected override void ApplyMovement(Vector3 move)
        {
            fixPos();
            myTabletScript.hasHitButton = true;
            calculateValue();
            broadcast();
            base.ApplyMovement(move);
        }

        public void RecalculateBoundaries()
        {
            if (transform.parent.Find("Background"))
            {
                MIN = -(transform.parent.Find("Background").localScale.x * 2f + transform.parent.Find("Handle").localScale.x / 2f);
                MAX = Mathf.Abs(MIN);
            }
            else if (transform.parent.parent.Find("Background"))
            {
                MIN = -(transform.parent.parent.Find("Background").localScale.x * 2f + transform.parent.parent.Find("Handle").localScale.x / 2f);
                MAX = Mathf.Abs(MIN);
            }
            fixPos();
        }

        public void SetValue(float val)
        {
            fixPos();
            value = Mathf.Clamp01(val);
            transform.localPosition = new Vector3(value * (MAX - MIN) - Mathf.Abs(MIN),transform.localPosition.y,transform.localPosition.z);
            broadcast();
        }

        public float GetRealValue()
        {
            fixPos();
            return value * (max - min) - Mathf.Abs(min);
        }

        private void calculateValue()
        {
            value = (transform.localPosition.x + Mathf.Abs(MIN)) / (MAX - MIN);
            fixPos();
        }

        private void broadcast()
        {
            if(targetComponent != null && !string.IsNullOrEmpty(targetProperty))
            {
                
                try
                {
                    targetComponent.GetType().GetProperty(targetProperty).SetValue(targetComponent, GetRealValue(), null);
                }
                catch (System.Exception e)
                {
                    Debug.LogError("VRSlider error whilst accessing property via reflection. " + e.ToString());
                }

                fixPos();
            }

        }

        void fixPos()
        {
            if (transform.localPosition.x < MIN)
            {
                transform.localPosition = new Vector3(MIN, 0, 0);
            }
            else if (transform.localPosition.x > MAX)
            {
                transform.localPosition = new Vector3(MAX, 0, 0);
            }
            else
            {
                transform.localPosition = new Vector3(transform.localPosition.x, 0, 0);
            }
        }

    }
}
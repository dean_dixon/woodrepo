﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace VREasy
{
    public class VRSelectable_colour : VRSelectable
    {
        #region
        public Color IdleColour
        {
            get
            {
                return idleColour;
            }
            set
            {
                bool changed = idleColour != value;
                idleColour = value;
                if (changed) SetState();
            }
        }
        public Color SelectColour
        {
            get
            {
                return selectColour;
            }
            set
            {
                bool changed = selectColour != value;
                selectColour = value;
                if (changed) SetState();
            }
        }
        public Color ActivateColour
        {
            get
            {
                return activateColour;
            }
            set
            {
                bool changed = activateColour != value;
                activateColour = value;
                if (changed) SetState();
            }
        }

        #endregion PROPERTIES

        public Color idleColour = Color.white;
        public Color selectColour = Color.red;
        public Color activateColour = Color.blue;

        public bool useColourHighlights = true;

        public virtual void SetState()
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlayingOrWillChangePlaymode) return;
#endif
            // switch between states
            if (isSelected)
            {
                // SELECTED
                setColour(activateColour);
            }
            else if (isPressed)
            {
                // PRESSED BUT NOT SELECTED
                setColour(selectColour);
            }
            else
            {
                // IDLE
                setColour(idleColour);
            }
        }

        void Update()
        {
            SetState();
        }

        private void setColour(Color colour)
        {
            if (!useColourHighlights) return;
            Renderer[] rends = GetComponentsInChildren<Renderer>();
            foreach (Renderer rend in rends)
            {
                MeshRenderer mrend = rend as MeshRenderer;
                SpriteRenderer srend = rend as SpriteRenderer;
                if(mrend != null)
                {
                    foreach (Material m in rend.materials)
                    {
                        m.color = colour;
                    }
                    
                }
                if(srend != null)
                {
                    srend.color = colour;
                }
                
            }
        }
    }
}
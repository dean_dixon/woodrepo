﻿using UnityEngine;
using System.Collections;

namespace VREasy
{
    public class MoviePlayer : MonoBehaviour
    {
#if UNITY_STANDALONE
        private MovieTexture movie;
#endif
#pragma warning disable 0109
        private new AudioSource audio;
#pragma warning restore 0109
        void Start()
        {
#if UNITY_STANDALONE
            Renderer r = GetComponent<Renderer>();
            movie = (MovieTexture)r.material.mainTexture;
#endif
            audio = GetComponent<AudioSource>();
        }

        public void PlayMovie()
        {
#if UNITY_EDITOR
            Debug.Log("Play movie");
#endif
#if UNITY_STANDALONE
            if (movie) movie.Play();
#endif
            if (audio) audio.Play();
        }
        public void PauseMovie()
        {
#if UNITY_EDITOR
            Debug.Log("Pause movie");
#endif
#if UNITY_STANDALONE
            if (movie) movie.Pause();
#endif
            if (audio) audio.Pause();
        }

        public void StopMovie()
        {
#if UNITY_EDITOR
            Debug.Log("Stop movie");
#endif
#if UNITY_STANDALONE
            if (movie) movie.Stop();
#endif
            if (audio) audio.Stop();
        }
    }
}
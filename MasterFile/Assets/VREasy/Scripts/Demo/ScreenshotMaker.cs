﻿using UnityEngine;
using System.Collections;

namespace VREasy
{
    public class ScreenshotMaker : MonoBehaviour
    {
        public VRGrabTrigger trigger;
        public string filename;
        public int screenshotMultiplier = 1;

        [HideInInspector]
        public bool isTakingScreenshot = false;

        private float lastTake = -1f;
        private float COOLDOWN = 1f;



        // Update is called once per frame
        void Update()
        {
            if(trigger != null)
            {
                if (Time.time > lastTake + COOLDOWN && trigger.Triggered())
                {
                    ScreenCapture.CaptureScreenshot(filename + "_" + System.DateTime.Now.ToString("MM-dd-yy_hh-mm-ss") + ".png", screenshotMultiplier);
                    Invoke("flashAnimationDelay", 0.1f);
                }
            }
            else
            {
                if (Time.time > lastTake + COOLDOWN && isTakingScreenshot)
                {
                    ScreenCapture.CaptureScreenshot(filename + "_" + System.DateTime.Now.ToString("MM-dd-yy_hh-mm-ss") + ".png", screenshotMultiplier);
                    Invoke("flashAnimationDelay", 0.1f);
                    isTakingScreenshot = false;
                }
            }
            
        }

        private void flashAnimationDelay()
        {
            lastTake = Time.time;
        }

        private Shader shader;
        private Material m_Material;


        void Start()
        {
            shader = Shader.Find("VREasy/ColourImage");
            if(shader == null)
            {
                Debug.LogError("VREasy/ColourImage shader not found! Needed for screenshot effect");
                enabled = false;
                return;
            }
            // Disable if we don't support image effects
            if (!SystemInfo.supportsImageEffects)
            {
                enabled = false;
                return;
            }

            // Disable the image effect if the shader can't
            // run on the users graphics card
            if (!shader || !shader.isSupported)
                enabled = false;
        }


        private Material material
        {
            get
            {
                if (m_Material == null)
                {
                    m_Material = new Material(shader);
                    m_Material.hideFlags = HideFlags.HideAndDontSave;
                }
                return m_Material;
            }
        }


        void OnDisable()
        {
            if (m_Material)
            {
                DestroyImmediate(m_Material);
            }
        }

        // Called by camera to apply image effect
        void OnRenderImage(RenderTexture source, RenderTexture destination)
        {

            material.SetFloat("_Value", Mathf.Clamp01(COOLDOWN - (Time.time - lastTake)));
            Graphics.Blit(source, destination, material);
        }
    }
}